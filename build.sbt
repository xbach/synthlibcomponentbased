//enablePlugins(PackPlugin)

name := "OMTRepair"

version := "1.0"

scalaVersion := "2.12.6"

//javacOptions ++= Seq("-source", "1.8", "-target", "1.8")

//scalacOptions += "-target:jvm-1.8"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
//resolvers += "Maven repo1" at "http://repo1.maven.org/"

//libraryDependencies += "com.typesafe.akka" % "akka-actor" % "2.0.2"

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-library" % "2.12.6",
  "log4j" % "log4j" % "1.2.17"
  //"org.apache.maven.shared" % "maven-invoker" % "3.0.0"
)

// for debugging sbt problems
logLevel := Level.Debug

//scalacOptions += "-deprecation"

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

//packMain := Map("hello" -> "driver.Main")

exportJars := true

//mainClass in (Compile, packageBin) := Some("driver.Main")

mainClass in assembly := Some("metadriver.MainDriver")