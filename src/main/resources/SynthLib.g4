grammar SynthLib;

program: setLogicCmd (cmd)+ EOF
       | (cmd)+ EOF
       ;

setLogicCmd: OPENBRK SETLOGICKW Symbol CLOSEBRK ;

cmd: //SortDefCmd
     varDeclCmd
    //| funDeclCmd
    //| FunDefCmd
    | synthFunCmd
    | constraintCmd
    | checkSynthCmd
    | origCmd
    ;

//Symbol: JavaLetter JavaLetterOrDigit*;

//fragment
//JavaLetter
//    :   [a-zA-Z$_-] // these are the "java letters" below 0x7F
//    |   // covers all characters above 0x7F which are not a surrogate
//        ~[\u0000-\u007F\uD800-\uDBFF]
//    |   // covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
//        [\uD800-\uDBFF] [\uDC00-\uDFFF]
//    ;

//fragment
//JavaLetterOrDigit
//    :   [a-zA-Z0-9$_-] // these are the "java letters or digits" below 0x7F
//    |   // covers all characters above 0x7F which are not a surrogate
//        ~[\u0000-\u007F\uD800-\uDBFF]
//    |   // covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
//        [\uD800-\uDBFF] [\uDC00-\uDFFF]
//    ;


//QuotedLiteral: '"'([a-z]| [A-Z]| [0-9]| SpecialChar)+'"'
//        ;

varDeclCmd: OPENBRK DECVARKW Symbol sortExpr CLOSEBRK;

//funDeclCmd: OPENBRK DECFUNKW Symbol (SortExpr)* SortExpr CLOSEBRK ;

synthFunCmd: OPENBRK SYNTHFUN Symbol OPENBRK pairs CLOSEBRK sortExpr OPENBRK nTDef+ CLOSEBRK CLOSEBRK ;

origCmd: OPENBRK ORIGKW Symbol OPENBRK pairs CLOSEBRK sortExpr  term  CLOSEBRK ;

nTDef: OPENBRK Symbol sortExpr OPENBRK gTerm+ CLOSEBRK CLOSEBRK;

pairs: (OPENBRK Symbol sortExpr CLOSEBRK)* ;

constraintCmd: OPENBRK CONSTRAINTKW term CLOSEBRK ;

checkSynthCmd: OPENBRK CHECKSYNTHKW CLOSEBRK;

gTerm:  fGTerm
        | literalGTerm
        | symbolGTerm
        ;
fGTerm: OPENBRK Symbol gTerm* CLOSEBRK ;
literalGTerm: literal ;
symbolGTerm: Symbol ;

term: fterm
        | literalTerm
        | symbolTerm
        ;
fterm: OPENBRK Symbol term* CLOSEBRK ;
literalTerm: literal ;
symbolTerm: Symbol ;

sortExpr: intSortExp | boolSortExp ;
intSortExp: IntSort;
boolSortExp: BoolSort;
IntSort: 'Int';
BoolSort: 'Bool';

literal: intConstExp
        //| RealConst
       | boolConstExp
        //| BV Const
        //| EnumConst
        ;
intConstExp: IntConst ;
boolConstExp: BoolConst ;

IntConst: [0-9]+ | '-'[0-9]+
        ;
BoolConst: 'true' | 'false' ;

CONSTRAINTKW: 'constraint';
ORIGKW: 'original-expr';
DECVARKW: 'declare-var';
DECFUNKW: 'declare-fun';
CHECKSYNTHKW: 'check-synth';
SETLOGICKW: 'set-logic';
OPENBRK: '(';
CLOSEBRK: ')';
SYNTHFUN: 'synth-fun';

Symbol: ([a-z]|[A-Z]|SpecialChar|'_'|'-')
        | ([a-z]|[A-Z]|[0-9]|SpecialChar|'_'|'-')+
        ;

SpecialChar: '_'|'+'|'−'|'∗'|'&'|'|'|'!'|'∼'|'<'|'>'|'='|'/'|'%'|'?'|'.'|'$'|'ˆ' ;

WS : [ \t\r\n\u000C]+ -> skip; // Define whitespace rule, toss it out

LINE_COMMENT
    :   ';' ~[\r\n]* -> channel(HIDDEN)
;