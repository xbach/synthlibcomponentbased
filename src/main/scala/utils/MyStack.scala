package utils
import scala.collection.immutable
/**
  * Created by dxble on 3/28/17.
  */
class MyStack[T] {
  private var stack = immutable.List[T]()

  def apply(i: Int) = {
    stack(i)
  }

  def push(t: T): Unit = {
    stack = t :: stack
  }

  def pop(): T = {
    val h = stack.head
    stack = stack.tail
    h
  }

  def size(): Int = {
    stack.length
  }

  def isEmpty(): Boolean = {
    stack.isEmpty
  }

  def top(): T = {
    stack.head
  }
}
