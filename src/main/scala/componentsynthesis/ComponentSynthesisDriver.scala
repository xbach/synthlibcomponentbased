package componentsynthesis

import java.io.{File, IOException}

import common.astutils.{CASTUtils, ToZ3ASTConverter, UnrolledGrammar}
import componentsynthesis.core.{Circuit, Component, Port}
import common.coreast._
import componentsynthesis.ComponentSynthesisDriver.{ExampleMultiFuncs, InputVariationMap, Variable2ValueMap}
import componentsynthesis.satsolver._
import config.ConfigOptions
import main.scala.synthlib2parser.scopemanager._
import org.apache.log4j.{BasicConfigurator, Logger}
import synthlib2parser.SynthLibCompiler

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 3/25/17.
  */
class ComponentSynthesisDriver(symbolTable: SymbolTable) extends AbstractSolver(symbolTable){
  val synthFunMap = new mutable.HashMap[String, SynthFunCmd]()
  val declaredVar2PortMap = new mutable.HashMap[String, Port]()
  val specInputPorts = new mutable.ArrayBuffer[Port]()
  val circuitCache = new mutable.HashMap[CAST, Circuit]()
  val circuitComponentCopies = new mutable.HashMap[FunctionSignature, mutable.HashMap[String, Int]]() // circuit name -> map (component -> copies)
  val synthUnrolledGrammar = new mutable.HashMap[String, UnrolledGrammar]()

  override def visitSynthFunCmd(cmd: SynthFunCmd): Unit = {
    synthFunMap += cmd.name -> cmd
  }

  override def visitVarDeclCmd(cmd: VarDeclCmd): Unit = {
    val name = String.format("__DECVAR_%s", cmd.name)
    val breed = String.format("__SPEC_%s", cmd.name)
    val port = Port(name, breed, cmd.sort)
    declaredVar2PortMap += cmd.name -> port
    specInputPorts.append(port)
  }

  def processOriginalExp(): Unit = {
    originalExprs.foreach(circuit => {
      val (circuitName, origExp) = (circuit._1, circuit._2)
      if(!synthUnrolledGrammar.contains(circuitName.funName)) {
        val countCache = new mutable.HashMap[String, Int]()
        val unrolledGrammar = new UnrolledGrammar
        val synthCmd = synthFunMap.getOrElse(circuitName.funName, throw new RuntimeException("Not found synthFun: " + circuitName))
        synthCmd.accept(unrolledGrammar)
        CASTUtils.funcComponentCount(origExp, countCache, unrolledGrammar)
        circuitComponentCopies += circuitName -> countCache
        synthUnrolledGrammar += circuitName.funName -> unrolledGrammar
      }
    })
  }

  override def visitOriginalExpCmd(cmd: OriginalExpCmd): Unit = {
    super.visitOriginalExpCmd(cmd)
  }

  private def lookup(expi: GTerm, r: NTDef, scope: SymbolTableScope) : SortExp ={
    if (expi.isInstanceOf[SymbolGTerm]) {
      val symGterm = expi.asInstanceOf[SymbolGTerm]
      //This is in case of recursive type definition
      if (symGterm.symbol == r.symbol)
        r.sort
      else {
        //Bachle: resolve in current scope first, then go to global scope
        //Current scope
        val resolvedSort = scope.lookup(SymbolTableCommon.mangleSortName(symGterm.symbol)).getSort()
        if (resolvedSort != null)
          resolvedSort
        else//Global scope
          symGterm.getTermSort(symbolTable)
      }
    } else {//FunGTerm or LiteralGTerm
      symbolTable.push(scope)
      val s = expi.getTermSort(symbolTable)
      symbolTable.pop()
      s
    }
  }

  // k is number of added copies
  private def generateCircuit(cmd: SynthFunCmd, k: Int): Circuit = {
    val circuitInputPorts = cmd.argList.args.foldLeft(List[Port]()){
      (res, arg) => {
        res :+
          Port(String.format("__CIRCUIT_INP_%s", arg.name),
               String.format("__CIRCUIT_%s_%s", cmd.name, arg.name),
               arg.sort)
      }
    }

    val circuitOutputPort = Port(String.format("__CIRCUIT_OUT_%s", cmd.name),
                                 String.format("__CIRCUIT_%s_%s", cmd.name, "Start"), cmd.sort)

    val componentBag = new ArrayBuffer[Component]()
    cmd.grammarRules.foreach(rule => rule.expansions.foreach(production => {
      production match {
        case e:LiteralGTerm => {
          //logger.info("GTerm: "+e)
            val outputPort = Port(String.format("%s_c%s_o_%d", rule.symbol, e.litGTerm.litStr, new Integer(0)),
                                  String.format("__CIRCUIT_%s_%s", cmd.name, rule.symbol), e.litGTerm.sort)
            val spec = Equal(outputPort.portNameCAST, CASTUtils.getValFromType(e.litGTerm.litStr, e.litGTerm.sort))
            val comp = Component(Array[Port](), outputPort, spec,
              String.format("%s_c%s",rule.symbol, e.litGTerm.litStr), e)
            comp.setIsConstComp()
            componentBag.append(comp)
        }

        case e:SymbolGTerm => {
          //logger.info("SymbolGTerm: "+e)
          val asArg = cmd.argList.args.find(arg => arg.name.compareTo(e.symbol) == 0).getOrElse(null)
          if(asArg != null){
            val sort = asArg.sort
            val inputPort = Port(String.format("%s_LIT%s_i_%s", rule.symbol, e.symbol, new Integer(0)),
                                 String.format("__CIRCUIT_%s_%s", cmd.name, e.symbol), sort)
            val outputPort = Port(String.format("%s_LIT%s_o_%s", rule.symbol, e.symbol, new Integer(0)),
                                  String.format("__CIRCUIT_%s_%s", cmd.name, rule.symbol), sort)
            val spec = Equal(outputPort.portNameCAST, inputPort.portNameCAST)
            val comp = Component(Array[Port](inputPort), outputPort, spec,
                                 String.format("%s_LIT%s", rule.symbol, e.symbol),e)
            comp.setIsLitComp()
            componentBag.append(comp)
          } else {
            throw new RuntimeException("Not yet handled!")
          }
        }
        case e: FunGTerm => {
          val funSig = FunctionSignature(cmd.name, argList2InternalTermList(cmd.argList))
          //val test = if(e.funName.compareTo("<=") == 0) 1 else if(e.funName.compareTo("or") == 0) -1 else 0
          val copies = circuitComponentCopies.getOrElse(funSig, new mutable.HashMap[String, Int]()).getOrElse(rule.symbol+"_"+e.funName, 1) + k
          var copyNum = 0
          while (copyNum < copies){
            var i = 1
            val inputPorts = e.args.foldLeft(Array[Port]()){
              (res, arg) => {
                val sort = lookup(arg, rule, cmd.scope)
                val inputPort = Port(String.format("%s_%s_i%d_%d", rule.symbol, e.funName, new Integer(i), new Integer(copyNum)),
                                     String.format("__CIRCUIT_%s_%s", cmd.name, arg.getGTermName()),
                                     sort)
                i += 1
                res :+ inputPort
              }
            }
            val outputPortSort = lookup(e, rule, cmd.scope)
            val outputPort = Port(String.format("%s_%s_o_%d", rule.symbol, e.funName, new Integer(copyNum)),
                                   String.format("__CIRCUIT_%s_%s", cmd.name, rule.symbol), outputPortSort)
            val inputPortVars = inputPorts.foldLeft(new ArrayBuffer[CAST]()){
              (res, p) => {
                res.append(p.portNameCAST)
                res
              }
            }

            val theoryFuncOnInputs = CASTUtils.theory2CAST(e.funName, inputPortVars)
            val spec = Equal(outputPort.portNameCAST, theoryFuncOnInputs)
            val comp = Component(inputPorts, outputPort, spec,
                                 String.format("%s_%s", rule.symbol, e.funName), e)
            comp.setIsFuncComp()
            componentBag.append(comp)
            copyNum += 1
          }
        }
      }
    }))
    val originalExp = originalExprs.find(p => p._1.funName.compareTo(cmd.name) == 0).getOrElse(null)._2
    val unrolledGrammar = synthUnrolledGrammar.getOrElse(cmd.name, null)
    return Circuit(cmd.name, componentBag.toArray, circuitInputPorts, circuitOutputPort, cmd, originalExp, unrolledGrammar)
  }

  // k is the number of copies. This function is called when visit checkSynth
  private def generateAll(constraints: CAST, k: Int, localVarConstraintsMap: mutable.HashMap[String, CAST]):
                                        (ArrayBuffer[CAST], ArrayBuffer[CAST], ArrayBuffer[Circuit]) ={
    val argConstraints = new ArrayBuffer[CAST]()
    val specConnList = new ArrayBuffer[CAST]()
    val circuitList = new ArrayBuffer[Circuit]()
    val opSym = constraints.topLevelString()

    def matchVar(name: String) = {
      if(localVarConstraintsMap.contains(name))
        argConstraints.append(localVarConstraintsMap.getOrElse(name, null))
      else if(declaredVar2PortMap.contains(name))
        argConstraints.append(declaredVar2PortMap.getOrElse(name, null).portNameCAST)

    }

    def matchFun(e: CAST) = {
      val (funcConstraint, specConns, circuits) = generateAll(e, k, localVarConstraintsMap)
      argConstraints.appendAll(funcConstraint)
      specConnList.appendAll(specConns)
      circuitList.appendAll(circuits)
    }

    constraints match {
      case e: InternalTerm => {
        e match {
          case e1: IntVar => {
            matchVar(e1.name)
            return (argConstraints, specConnList, circuitList)
          }
          case e1: BoolVar => {
            matchVar(e1.name)
            return (argConstraints, specConnList, circuitList)
          }
          case e1: IntConst => {
            argConstraints.append(IntConst(e1.value))
            return (argConstraints, specConnList, circuitList)
          }
          case e1: BoolConst => {
            argConstraints.append(BoolConst(e1.value))
            return (argConstraints, specConnList, circuitList)
          }
          case e1: FunctionSignature => {
            e1.args.foreach(arg => matchVar(arg.topLevelString()))
          }
        }
      }
      case e: UnOp => {
        matchFun(e.getOperand())
      }
      case e: BinOp => {
        matchFun(e.getLeft())
        matchFun(e.getRight())
      }
    }

    if(synthFunMap.contains(opSym)){
      if(localVarConstraintsMap.isEmpty){
        val circuit = circuitCache.getOrElse(constraints, null)
        if(circuit != null){
          val ret = new ArrayBuffer[CAST]()
          ret.append(circuit.outputPort.portNameCAST)
          return (ret, new ArrayBuffer[CAST](), new ArrayBuffer[Circuit]())
        }
      }

      val circuit = generateCircuit(synthFunMap.getOrElse(opSym, null), k)
      if(localVarConstraintsMap.isEmpty){
        circuitCache += constraints -> circuit
      }
      circuitList.append(circuit)
      if(circuit.inputPorts.length != argConstraints.length)
        throw new RuntimeException("Improper function call at: "+constraints)
      var i = 0
      circuit.inputPorts.foreach(port => {
        specConnList.append(Equal(port.portNameCAST, argConstraints(i)))
        i += 1
      })

      val ret = new ArrayBuffer[CAST]()
      ret.append(circuit.outputPort.portNameCAST)
      return (ret, specConnList, circuitList)
    }else{
      val ret = new ArrayBuffer[CAST]()
      ret.append(CASTUtils.theory2CAST(constraints.topLevelString(), argConstraints))
      return (ret, specConnList, circuitList)
    }
  }

  private def inputVariations(examples: mutable.HashSet[ExampleMultiFuncs]): InputVariationMap = {
    val res = new InputVariationMap
    var iter = 1
    examples.foreach((example: ExampleMultiFuncs) => {
      example.foreach{case (circuitName: String, valueMap: Variable2ValueMap) =>{
        val variationMap = res.getOrElse(circuitName, new mutable.HashMap[VarTerm, mutable.ArrayBuffer[(Int, ConstTerm)]]())
        valueMap.foreach {
          case (term, value) => {
            val values = variationMap.getOrElse(term, new mutable.ArrayBuffer[(Int, ConstTerm)]())
            values.append((iter, value))
            variationMap.update(term, values)
          }
        }
        res.update(circuitName, variationMap)
      }}
      iter += 1
    })
    res
  }

  /**
    * Synthesis task is started from here. This serves as an activation function.
    * @param cmd
    */
  override def visitCheckSynthCmd(cmd: CheckSynthCmd): Unit = {
    processOriginalExp()
    var additionalCopyNum = 0
    val constraints = CASTUtils.joinConstraint(allConstraints)
    //lazy val solver = new Z3ScalaWeightedSolver(MaxSatAlgorithm.StandardWeighted)
    //lazy val solver = new Z3JavaWeightedSolver(MaxSatAlgorithm.STANDARDWEIGHTED)
    lazy val solver = new Z3WeightedSatSolver(ConfigOptions.maxSatAlgorithm)
    //lazy val solver = new OptiMathSatSolver(ConfigOptions.maxSatAlgorithm)
    val examples = extractExamplesMultiFuncs()
    val variations = inputVariations(examples)
    while (additionalCopyNum < 3) {
      //solver.reset()
      logger.info("Synthesis copy: "+additionalCopyNum)
      solver.push()
      val (spec, specConnList, circuits) = generateAll(constraints, additionalCopyNum, new mutable.HashMap[String, CAST]())
      logger.debug("Circuits: "+ circuits)
      val synthRes = solver.synthesize(spec(0), specInputPorts, CASTUtils.joinConstraint(specConnList), circuits, examples, variations)
      if(synthRes)
        return
      else{
        circuitCache.clear()
      }
      solver.pop()
      additionalCopyNum += 1
    }
    solver.delete()
  }

  private def extractExamplesMultiFuncs(): mutable.HashSet[ExampleMultiFuncs] = {
    assert(synthFunMap.size > 0, "The number of functions being synthesized should be greater than zero!")
    val res = new mutable.HashSet[ExampleMultiFuncs]()
    val example = new ExampleMultiFuncs
    def matchImplies(constraint: CAST): Unit = {
      constraint match {
        case e: Implies => {
          val m = new Variable2ValueMap
          def recMatchVariable2Value(rm: CAST): Unit = {
            rm match {
              case a: And => {
                recMatchVariable2Value(a.opr1)
                recMatchVariable2Value(a.opr2)
              }
              case eq: Equal => {
                m += CASTUtils.transformName(eq.opr1.asInstanceOf[VarTerm], specInputPorts) -> eq.opr2.asInstanceOf[ConstTerm]
              }
            }
          }
          recMatchVariable2Value(e.opr1)

          val funcName = e.opr2.asInstanceOf[Equal].getLeft().asInstanceOf[FunctionSignature].funName
          // We intended to keep expected output in this map as well.
          val funcSort = synthFunMap.getOrElse(funcName, null).sort
          val expectedValue = e.opr2.asInstanceOf[Equal].getRight().asInstanceOf[ConstTerm]
          m += CASTUtils.makeVariable(funcName, funcSort) -> expectedValue

          example += funcName -> m

          if(example.size == synthFunMap.size){
            // Copy the example, add to result, and clear the example.
            res.add(example.foldLeft(new ExampleMultiFuncs){(res, e) => {res += e; res}})
            example.clear()
          }
        }
        case or: Or => {// or of angelic paths
          matchImplies(or.opr1)
          matchImplies(or.opr2)
        }
        case and: And => {// and of instance ids in a path, each instance id is then and of implies
          matchImplies(and.opr1)
          matchImplies(and.opr2)
        }
        case _ => {logger.error("!!!Non-example constraints are encountered: "+constraint)}
      }
    }
    allConstraints.foreach{
      constraint => {
        matchImplies(constraint)
      }
    }
    res
  }

  /*private def extractExamples(): mutable.HashSet[mutable.HashMap[VarTerm, ConstTerm]] = {
    def matchImplies(constraint: CAST, res: mutable.HashSet[mutable.HashMap[VarTerm, ConstTerm]]): Unit = {
      constraint match {
        case e: Implies => {
          val m = new mutable.HashMap[VarTerm, ConstTerm]()
          def recMatch(rm: CAST): Unit = {
            rm match {
              case a: And => {
                recMatch(a.opr1)
                recMatch(a.opr2)
              }
              case eq: Equal => {
                m += transformName(eq.opr1) -> eq.opr2.asInstanceOf[ConstTerm]
              }
            }
          }
          recMatch(e.opr1)
          res.add(m)
        }
        case or: Or => {
          matchImplies(or.opr1, res)
          matchImplies(or.opr2, res)
        }
        case and: And => {
          matchImplies(and.opr1, res)
          matchImplies(and.opr2, res)
        }
        case _ => {logger.warn("!!!Non-example constraints are encountered: "+constraint)}
      }
    }
    val ret = allConstraints.foldLeft(new mutable.HashSet[mutable.HashMap[VarTerm, ConstTerm]]()){
      (res, constraint) => {
        matchImplies(constraint, res)
        res
      }
    }
    ret
  }*/
}
object ComponentSynthesisDriver {
  // Map from variable name to its value
  type Variable2ValueMap = mutable.HashMap[VarTerm, ConstTerm]
  // Map from function name (circuit name) to variables' values in its circuit
  type ExampleMultiFuncs = mutable.HashMap[String, Variable2ValueMap]
  // Map from circuit name to the variation of input variables
  type InputVariationMap = mutable.HashMap[String, mutable.HashMap[VarTerm, mutable.ArrayBuffer[(Int, ConstTerm)]]]

  val logger = Logger.getLogger(this.getClass)
  val testDistance = "/home/dxble/workspace/synthesis/sygus-comp14/solvers/symbolic/tests/distance.sl"
  val testLongif = "/home/dxble/workspace/synthesis/sygus-comp14/solvers/symbolic/tests/longif.sl"

  // t3 only works with box opt, t1 works best when only coverage
  val prefix = "/home/dxble/Desktop/initRes/FSE2017-S3-SyntaxSemanticRepairData2/prioritizeS3/"
  val prefix1= "/home/dxble/Desktop/initRes/resBef26"
  val test1 = prefix+"/t3/synthesized.patch.sygus" //t1, t3 now ok, check t5, t17
  // t5 requires adding 1 more <= component.
  // we: t1, t3, t4, t7, t9, t12 [works only with output coverage], t13
  // Note: t9: (and (or (>= yearInt 0) (<= yearInt 0)) (< yearInt 9999))) size bound 4, size bound 3 then correct. Pareto.
  // common: t2, t6
  // s3: t5, t8, t10, t11, t14 [sensitive to structural box vs pareto]

  @throws[IOException]
  def solve(): Unit = {
    val parser = new SynthLibCompiler()
    //BasicConfigurator.configure()
    //System.setProperty("log4j.configuration", new File("resources", "Log4j.properties").toURL().toString)
    logger.info("Parsing...")
    val start = System.currentTimeMillis()
    val sygusFile = ConfigOptions.sygusFile
    val ast = parser.parse(sygusFile)
    val end = System.currentTimeMillis()
    logger.info("Parsing time: "+ (end - start)/1000.0)
    logger.info("Parsed...")
    logger.debug(ast)
    val synthesizer = new ComponentSynthesisDriver(parser.theSymbolTable)
    ast.accept(synthesizer)
    val end2 = System.currentTimeMillis()
    logger.info("Running time: "+ (end2 - start)/1000.0)
  }
}
