package componentsynthesis.core

import common.astutils.CASTUtils
import common.coreast._
import main.scala.synthlib2parser.scopemanager.{GTerm, IntSortExp, SortExp}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 3/25/17.
  */
case class Port(nameWithoutID: String, breed: String, sort: SortExp){
  val id: Int = Port.numPorts
  lazy val name: String = nameWithoutID+"_"+id
  lazy val portNameCAST: CAST = CASTUtils.makeVariable(name, sort)

  def init(){
    Port.numPorts += 1
  }

  init()

  def genVarCopyForIteration(iteration: Int) = {
    val varCopyName = name + "_iter" + iteration
    CASTUtils.makeVariable(varCopyName, sort)
  }

  def getBreed(): String = breed

  override def toString: String = {
    "Port_"+name+"_"+breed
  }

  def equalPortName(that: Port): BoolOperatorsCAST = {
    if(!that.sort.equals(sort))
      throw new RuntimeException("Not equal sorts! "+sort +" vs "+that.sort+ "Ports: "+this.portNameCAST+" "+that.portNameCAST)

    Equal(portNameCAST, that.portNameCAST)
  }
}
object Port{
  var numPorts = 0
}
case class Component(inputPorts: Array[Port], outputPort: Port, spec: BoolOperatorsCAST, nameOrig: String, production: GTerm){
  val name: String = nameOrig + "_" + Component.numComponents
  var inputLabels: Array[String] = null
  val inputLabelMap: mutable.HashMap[String, String] = new mutable.HashMap()
  var outputLabel: String = null
  lazy val topLevelFuncString = if(isFuncComp()) nameOrig.split("_")(1) else throw new RuntimeException("!!! This is not a function component: " + this)

  var pos: Int = -1
  private var isConst: Boolean = false
  private var isLit: Boolean = false
  private var isFunc: Boolean = false

  def setIsConstComp() = isConst = true
  def isConstComp() = isConst
  def setIsLitComp() = isLit = true
  def isLitComp() = isLit
  def isFuncComp() = isFunc
  def setIsFuncComp() = isFunc = true

  def init(): Unit = {
    Component.numComponents += 1
  }

  init()

  def setInputLabels(labels: Array[String]): Unit = {
    if(inputPorts.length != labels.length)
      throw new RuntimeException("Length not equals here...")
    inputLabels = labels
    inputLabels.zipWithIndex.foreach{case (l,i) => inputLabelMap += inputPorts(i).name -> l}
  }

  def distinctInputConstraints(): BoolOperatorsCAST = {
    if(inputLabels.length == 2)
      Not(Equal(IntVar(inputLabels(0)), IntVar(inputLabels(1)))) // Bachle: added
    else null
  }

  def setOutputLabel(label: String): Unit = {
    outputLabel = label
  }

  def setPosition(pos: Int): Unit = {
    this.pos = pos
  }

  override def toString: String = {
    var retVal = String.format("<Component %s:", name)
    inputPorts.foreach(port => {
      retVal += String.format("\n\t%s", port.toString)
    })
    retVal += String.format("\n\t->")
    retVal += String.format("\n\t%s", outputPort.toString)
    retVal += String.format("\n\tSpec\n\t%s\n", if(spec != null) spec.toString else "null lala")
    retVal += '>'

    return retVal
  }

  def print(inputNames: ArrayBuffer[String], outputName: String): String = {
    var retVal = outputName + ":=" + name+"("
    var i = 0
    while(i < inputNames.length) {
      retVal += inputNames(i)
      if (i != inputNames.length - 1)
        retVal += ", "
      i += 1
    }
    retVal += ")"
    return retVal
  }
}

object Component {
  var numComponents = 0

  // These functions are not yet used, but may be of value later to organize code
  def compFuncOrigName(scope: String, funcName: String) = {
    String.format("%s_%s", scope, funcName)
  }

  def compConstOrigName(scope: String, value: String) = {
    String.format("%s_c%s", scope, value)
  }

  def test() = {
    val p =  Port("x","---", IntSortExp())
    println(p)
    val p2 = Port("y","---", IntSortExp())
    println(p2)
  }

  def main(args: Array[String]): Unit = {
    test()
  }
}

