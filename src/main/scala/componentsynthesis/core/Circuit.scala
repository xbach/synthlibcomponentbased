package componentsynthesis.core


import common.astutils.{CASTUtils, UnrolledGrammar}
import common.coreast._
import componentsynthesis.satsolver.SolverUtils
import main.scala.synthlib2parser.scopemanager.{FunGTerm, LiteralGTerm, SymbolGTerm, SynthFunCmd}
import org.apache.log4j.Logger
import z3.scala.{Z3AST, Z3Context, Z3Model}

import scala.collection.{immutable, mutable}
import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 3/26/17.
  */
case class Circuit(funcName: String, components: Array[Component], inputPorts: List[Port],
                   outputPort: Port, synthFun: SynthFunCmd, originalExp: CAST, unrolledGrammar: UnrolledGrammar) {

  val logger = Logger.getLogger(this.getClass)

  // Function name of circuit is composed by f_lineNumber
  lazy val lineNumer = funcName.split("_")(1)

  val id = Circuit.numCircuits - 1
  val labels: mutable.HashMap[Int, IntVar] = new mutable.HashMap()
  //lazy val labelSet: Set[IntVar] = labels.values.toSet
  val pn2lnMap: mutable.HashMap[String, IntVar] = new mutable.HashMap() // compPortNameToLabelNameMap
  val ln2pMap: mutable.HashMap[IntVar, Port] = new mutable.HashMap() // labelNameToCompPortMap

  lazy val (consComps, litFuncComps) = components.partition(_.isConstComp())
  lazy val (funcComps, litComps) = litFuncComps.partition(comp => comp.isFuncComp())

  private lazy val numInputPorts = inputPorts.length
  private lazy val numOfVarConstTerms = {
    /*var i = 0
    val seen = new mutable.HashSet[CAST]()
    def calculate(exp: CAST): Unit = {
      exp match {
        case _: InternalTerm => {
          if(!seen.contains(exp)){
            i += 1
            seen.add(exp)
          }
        }
        case bin: BinOp => {calculate(bin.getLeft()); calculate(bin.getRight())}
        case un: UnOp => calculate(un.getOperand())
      }
    }
    calculate(originalExp)
    i*/
    consComps.length + litComps.length
  }

  lazy val origExpHasLitVarTerm = {
    def helper(exp: CAST): Boolean = {
      exp match {
        case _: VarTerm => true
        case bin: BinOp => if(helper(bin.getLeft())) true else helper(bin.getRight())
        case un: UnOp => helper(un.getOperand())
        case _ => false
      }
    }
    helper(originalExp)
  }

  private lazy val numFuncsOrigExp: Int = calculateNumFuncs(originalExp)
  private lazy val outputLocOrigExp: Int = {
     numFuncsOrigExp + numOfVarConstTerms + numInputPorts - 1 + (funcComps.length - numFuncsOrigExp)
  }

  def init(): Unit = {
    Circuit.numCircuits += 1
    generateLabels()
  }

  init()

  override def toString: String = {
    "Circuit("+funcName+","+components.foldLeft("\n"){(res, i) => res + i.toString}+","+inputPorts+","+outputPort+")"
  }

  private def generateLabels(): Unit = {
    var numLabels = 0

    // Generate labels for input ports. Note that each component can have multiple input ports
    components.foreach { comp => {
      val inputLabels = comp.inputPorts.zipWithIndex.foldLeft(Array[String]()) { (res, pi) => {
        val (inpPort, index) = pi
        val labelName = String.format("L_%1$s_i_%2$02d", comp.name, new Integer(index))
        val labelNameFormula = IntVar(labelName)
        labels += numLabels -> labelNameFormula
        numLabels += 1
        pn2lnMap += inpPort.name -> labelNameFormula
        ln2pMap += labelNameFormula -> inpPort
        res :+ labelName
      }
      }
      comp.setInputLabels(inputLabels)
    }
    }

    // Generate labels for output ports. Note that each component only has 1 output port
    components.foreach { comp => {
      val labelName = String.format("L_%s_o", comp.name)
      val labelNameFormula = IntVar(labelName)
      labels += numLabels -> labelNameFormula
      numLabels += 1
      pn2lnMap += comp.outputPort.name -> labelNameFormula
      ln2pMap += labelNameFormula -> comp.outputPort
      comp.setOutputLabel(labelName)
    }
    }
  }

  def generateLabelRangeConstraints(): BoolOperatorsCAST = {
    //val numInputPorts = inputPorts.length
    val numComponents = components.length

    val inputLabelLo = IntConst(0.toString)
    val inputLabelHi = IntConst((numInputPorts + numComponents)toString)
    val outputLabelLo = IntConst(numInputPorts.toString)
    val outputLabelHi = IntConst((numInputPorts + numComponents).toString)

    val initConstraint: BoolOperatorsCAST = BoolConst("true")
    val constraints = components.foldLeft(initConstraint) {
      (res, comp) => {
        val resi = comp.inputPorts.foldLeft(res)((r, p) => {
          val pLabel = pn2lnMap.getOrElse(p.name, null)
          And(And(r, GTE(pLabel, inputLabelLo)), LessThan(pLabel, inputLabelHi))
        })

        val pOutputLabel = pn2lnMap.getOrElse(comp.outputPort.name, null)
        And(And(resi, GTE(pOutputLabel, outputLabelLo)), LessThan(pOutputLabel, outputLabelHi))
      }
    }

    constraints
  }

  def generateDistinctOutputLabelConstraints(): BoolOperatorsCAST = {
    var i = 0
    val len = components.length
    var constraints: BoolOperatorsCAST = BoolConst("true")
    while (i < len) {
      var j = i + 1
      while (j < len) {
        val iLabel = pn2lnMap.getOrElse(components(i).outputPort.name, null)
        val jLabel = pn2lnMap.getOrElse(components(j).outputPort.name, null)
        val eq = Equal(iLabel, jLabel)
        constraints = And(constraints, Not(eq))
        j += 1
      }
      i += 1
    }
    constraints
  }

  def generateAcycConstraints(): BoolOperatorsCAST = {
    var constraints: BoolOperatorsCAST = BoolConst("true")
    components.foreach {
      comp => {
        val outputLabel = pn2lnMap.getOrElse(comp.outputPort.name, null)
        comp.inputPorts.foreach {
          inpPort => {
            val inputLabel = pn2lnMap.getOrElse(inpPort.name, null)
            constraints = And(constraints, LessThan(inputLabel, outputLabel))
          }
        }
      }
    }
    constraints
  }

  /**
    * Each function component has distinct inputs, e.g., (+ x x) is prohibited
    *
    * @return
    */
  def generateDistinctInputs(): BoolOperatorsCAST = {
    var constraints: BoolOperatorsCAST = BoolConst("true")
    components.foreach {
      comp => {
        val constraint = comp.distinctInputConstraints()
        if(constraint != null)
          constraints = And(constraints, constraint)
      }
    }
    constraints
  }

  /**
    * Bachle added
    *
    * @return
    */
  def generateNoBothConstantInputs(): BoolOperatorsCAST = {
    //val (consComps, litFuncComps) = components.partition(_.isConstComp())
    //val funcComps = litFuncComps.filter(comp => comp.isFuncComp() && comp.inputPorts.length == 2)
    def orConstraint(comps: Array[Component], port: Port) = {
      val inputPort = pn2lnMap.getOrElse(port.name, null)
      var constraint: BoolOperatorsCAST = null
      comps.foreach(comp => {
        val eq = Equal(inputPort, IntVar(comp.outputLabel))
        if(constraint == null)
          constraint = eq
        else constraint = Or(constraint, eq)
      })
      constraint
    }

    val constraints = new ArrayBuffer[CAST]()
    funcComps.filter(_.inputPorts.size == 2).foreach{comp => {
      val leftPort = comp.inputPorts(0)
      val rightPort = comp.inputPorts(1)

      // filter constant components with compatible types
      val consCompsLeft = consComps.filter(comp => comp.outputPort.sort.equals(leftPort.sort))
      val consCompsRight = consComps.filter(comp => comp.outputPort.sort.equals(rightPort.sort))
      if(consCompsLeft.length > 0 && consCompsRight.length > 0) {
        val constraint = Not(And(orConstraint(consCompsLeft, leftPort), orConstraint(consCompsRight, rightPort)))
        //logger.info("--- "+constraint)
        constraints.append(constraint)
      }
    }}
    //logger.info(constraints)
    val x  = CASTUtils.joinConstraint(constraints)
    //logger.info("X "+x)
    x
    //BoolConst("true")
  }

  def generateSoftFewerIdenticalSubTree(): ArrayBuffer[(BoolOperatorsCAST, Int)] = {
    val res = new ArrayBuffer[(BoolOperatorsCAST, Int)]()
    var i = 0
    val max = funcComps.length
    while(i < max){
      var j = i + 1
      while (j < max){
        val comp1 = funcComps(i)
        val comp2 = funcComps(j)
        if(comp1.topLevelFuncString.compareTo(comp2.topLevelFuncString) == 0){
          if(comp1.inputPorts.length == comp2.inputPorts.length){
            var constraint: BoolOperatorsCAST = BoolConst("true")
            var index = 0
            val maxLength = comp1.inputPorts.length
            val collected = new ArrayBuffer[CAST]()
            while (index < maxLength){
              collected.append(Equal(IntVar(comp1.inputLabels(index)), IntVar(comp2.inputLabels(index))))
              index += 1
            }

            constraint = CASTUtils.joinConstraint(collected)
            if (comp1.topLevelFuncString.compareTo("=") == 0){
              val additionalSymmetry1 = Equal(IntVar(comp1.inputLabels(0)), IntVar(comp2.inputLabels(1)))
              val additionalSymmetry2 = Equal(IntVar(comp1.inputLabels(1)), IntVar(comp2.inputLabels(0)))
              constraint = Or(constraint, And(additionalSymmetry1, additionalSymmetry2))
            }

            res.append((Not(constraint), 1))
          }
        }
        j += 1
      }
      i += 1
    }
    res
  }

  def generateVariableWScore(variableScore: mutable.HashMap[String, Int]): ArrayBuffer[(BoolOperatorsCAST, Int)] = {
    val res = new ArrayBuffer[(BoolOperatorsCAST, Int)]()
    variableScore.foreach{case(vName, score) => {
      val comps = litComps.filter(comp => comp.nameOrig.contains("_LIT"+vName))
      assert(comps.length > 0)
      var vConstraints: BoolOperatorsCAST = null
      comps.foreach(vComp => {
        var compConstraint: BoolOperatorsCAST = null
        funcComps.foreach(fComp => {
          var eq: BoolOperatorsCAST = null
          fComp.inputPorts.foreach(ip => {
            if(ip.sort.equals(vComp.outputPort.sort)){
              val il = pn2lnMap.getOrElse(ip.name, throw new RuntimeException("Not found label for: "+ip.name))
              val temp = Equal(il, IntVar(vComp.outputLabel))
              if(eq == null)
                eq = temp
              else eq = Or(eq, temp)
            }
          })
          if(eq != null) {
            if (compConstraint == null)
              compConstraint = eq
            else compConstraint = Or(compConstraint, eq)
          }
        })
        if(compConstraint != null){
          if(vConstraints == null){
            vConstraints = compConstraint
          }else {
            vConstraints = Or(vConstraints, compConstraint)
          }
        }
      })
      if(vConstraints != null)
        res.append((vConstraints, score))
    }}
    res
  }

  def generateFewerConstants(map: mutable.HashMap[String, String]): ArrayBuffer[(BoolOperatorsCAST, Int)] = {
    val res = new ArrayBuffer[(BoolOperatorsCAST, Int)]()
    funcComps.foreach(funcComp => {
      consComps.foreach(consComp => {
        funcComp.inputLabels.foreach(il => {
          val temp = map.getOrElse(il, null)
          if (temp == null || temp.compareTo(consComp.outputLabel) != 0)
            res.append((Not(Equal(IntVar(il), IntVar(consComp.outputLabel))), 1))
        })
      })
    })
    res
  }

  // By value, need also by location
  def generateInputVariationConstraints(variations: mutable.HashMap[VarTerm, mutable.ArrayBuffer[(Int, ConstTerm)]]): ArrayBuffer[(BoolOperatorsCAST, Int)] = {
    val res = new mutable.HashSet[(BoolOperatorsCAST, Int)]()
    val trueConst: BoolOperatorsCAST = BoolConst("true")
    //assert(!variations.contains(IntVar(funcName)), "Make sure the variation map only contains variables and their values")
    assert(variations.exists(_._1.getName.compareTo(funcName) == 0), "Make sure the variation map also contains expected output value of the circuit")

    funcComps.foreach(funcComp => {
      funcComp.inputPorts.foreach(ip => {
        variations.foreach{case (term, values) => {
          if(term.getName.compareTo(funcName) != 0) {// This is to check if the term is not the circuit name
            val consts = values.map { case (_, t) => t }.toSet
            // Size > 1, so it must not be constant component.
            if (consts.size > 1 && ip.portNameCAST.getSort().equals(term.getSort())) {
              //logger.info("Processing: "+term+" "+values+ " "+ip)
              var constraints: BoolOperatorsCAST = null
              val collected = new mutable.HashSet[CAST]()
              values.foreach { case (iter, c) => collected.add(Equal(ip.genVarCopyForIteration(iter), c)) }
              constraints = CASTUtils.joinConstraint(collected.toBuffer)
              //val label = pn2lnMap.getOrElse(ip.name, null)
              // Not constant
              // consComps.foreach(c => constraints = And(constraints, Not(Equal(label, IntVar(c.outputLabel)))))
              res.add(Implies(trueConst, constraints), consts.size)
           }
          }
        }}
      })
    })
    res.foldLeft(new mutable.ArrayBuffer[(BoolOperatorsCAST, Int)]()){_ :+ _}
  }

  def generateSoftStructuralConstraints(): (ArrayBuffer[(BoolOperatorsCAST, Int)], mutable.HashMap[String, String]) = {
    val res = new ArrayBuffer[(BoolOperatorsCAST, Int)]()
    val cache = new mutable.HashMap[String, String]()
    val exp2Comp = new mutable.HashMap[CAST, Component]()

    def findCompsOfExp(exp: CAST, comps: mutable.Buffer[Component]): Array[Component] = {
      val comp = exp2Comp.getOrElse(exp, null)
      if(comp != null)
        return Array[Component](comp)

      val scopeList = unrolledGrammar.findScopesOfExp(exp)
      def getKey(scope: String): String = {
        exp match {
          case _: BinOp => scope + "_" + exp.topLevelString()
          case _: UnOp => scope + "_" + exp.topLevelString()
          case _: ConstTerm => scope + "_c" + exp.topLevelString()
          case _: VarTerm => scope + "_LIT" + exp.topLevelString()
          case _ => ""
        }
      }
      scopeList.scopesList.foldLeft(Array[Component]()){(res, scopeName) => {
        val key = getKey(scopeName.scope)
        res ++ comps.filter(_.nameOrig.compareTo(key) == 0)
      }}
    }

    var comps = components.toBuffer
    val usedOutput = new mutable.HashSet[String]()

    def filterComp(comp: Component) = comps.filter(c => if(c.isFuncComp()) c.name.compareTo(comp.name) != 0 else true)

    def helper(exp: CAST): Unit = {
      val expComp = findCompsOfExp(exp, comps)(0)
      exp2Comp += exp -> expComp
      exp match {
        case bin: BinOp => {
          comps = filterComp(expComp)
          val leftComp = findCompsOfExp(bin.getLeft(), comps).filterNot(comp => usedOutput.contains(comp.outputLabel))(0)
          exp2Comp += bin.getLeft() -> leftComp
          if(leftComp.isFuncComp())
            usedOutput += leftComp.outputLabel
          comps = filterComp(leftComp)
          val rightComp = findCompsOfExp(bin.getRight(), comps).filterNot(comp => usedOutput.contains(comp.outputLabel))(0)
          exp2Comp += bin.getRight() -> rightComp
          if(rightComp.isFuncComp())
            usedOutput += rightComp.outputLabel
          comps.append(expComp)
          comps.append(leftComp)
          val temp1 = Equal(IntVar(expComp.inputLabels(0)), IntVar(leftComp.outputLabel))
          val temp2 =  Equal(IntVar(expComp.inputLabels(1)), IntVar(rightComp.outputLabel))
          cache += expComp.inputLabels(0) -> leftComp.outputLabel
          cache += expComp.inputLabels(1) -> rightComp.outputLabel
          res.append((temp1, bin.getLeft().size())) // formula and its weight score
          res.append((temp2, bin.getRight().size()))
          helper(bin.getLeft())
          helper(bin.getRight())
        }
        case un: UnOp => {
          comps = filterComp(expComp)
          val operandComp = findCompsOfExp(un.getOperand(), comps).filterNot(comp => usedOutput.contains(comp.outputLabel))(0)
          exp2Comp += un.getOperand() -> operandComp
          if(operandComp.isFuncComp())
            usedOutput += operandComp.outputLabel
          comps.append(expComp)
          val tempConstraints = Equal(IntVar(expComp.inputLabels(0)), IntVar(operandComp.outputLabel))
          res.append((tempConstraints, un.getOperand().size()))
          helper(un.getOperand())
        }
        case _ => {}
      }
    }
    helper(originalExp)
    //val fewerConsts = generateFewerConstants(cache)
    //fewerConsts.foreach(res.append(_))
    (res, cache)
  }

  private def calculateNumFuncs(exp: CAST): Int ={
    exp match {
      case bin: BinOp => {
        val numLeft = calculateNumFuncs(bin.getLeft())
        val numRight = calculateNumFuncs(bin.getRight())
        numLeft + numRight + 1
      }
      case un: UnOp => {
        val numFunc = calculateNumFuncs(un.getOperand())
        numFunc + 1
      }
      case _: InternalTerm => 0
    }
  }

  private def usedFuncInOrigExp(): Array[String] = {
    def helper(exp: CAST): Array[String] = {
      exp match {
        case bin: BinOp => {
          (bin.topLevelString() +: helper(bin.getLeft())) ++ helper(bin.getRight())
        }
        case un: UnOp => {
          un.topLevelString() +: helper(un.getOperand())
        }
        case _ => Array[String]()
      }
    }
    helper(originalExp)
  }

  def generateSoftLocationConstraints(): ArrayBuffer[(BoolOperatorsCAST, Int)] = {
    var funcCompsX = funcComps.foldLeft(Array[Component]()){_ :+ _}

    val res = new ArrayBuffer[(BoolOperatorsCAST, Int)]()

    var numOfDefinedFuncs = 0
    val funcCompsTopLevelString = funcComps.foldLeft(Array[String]()){_ :+ _.topLevelFuncString}
    val usedFuncs = usedFuncInOrigExp()
    val numOfUnusedFuncs = funcCompsTopLevelString.length - usedFuncs.length
    val fixedSize = numOfUnusedFuncs + numOfVarConstTerms + numInputPorts
    var numOfTraversedFunc = 0

    def funcHelper(exp: CAST) = {
      val scopeList = unrolledGrammar.findScopesOfExp(exp)
      var constraints: BoolOperatorsCAST = null
      //val numFuncs = calculateNumFuncs(exp)
      //val additionalSize = if(numFuncs == 1) numOfDefinedFuncs else 0
      val origLoc = (usedFuncs.length - numOfTraversedFunc) + fixedSize - 1

      scopeList.scopesList.foreach(scopeName => {
        val key = scopeName.scope + "_" + exp.topLevelString()
        val comp = funcCompsX.find(_.nameOrig.compareTo(key) == 0).getOrElse(null)
        constraints = if(constraints != null) {
          Or(constraints, Equal(IntVar(comp.outputLabel), IntConst(origLoc.toString)))
        } else {
          Equal(IntVar(comp.outputLabel), IntConst(origLoc.toString))
        }
        funcCompsX = funcCompsX.filterNot(_.equals(comp))
      })

      //if(numFuncs == 1)
      //  numOfDefinedFuncs += 1

      numOfTraversedFunc += 1
      res.append((constraints, exp.size()))
    }

    /*var numOfDefinedTerm = 0
    def termHelper(exp: InternalTerm) = {
      val scopeList = unrolledGrammar.findScopesOfExp(exp)
      var constraints: BoolOperatorsCAST = BoolConst("false")
      scopeList.scopesList.foreach(scopeName => {
        exp match {
          case _:VarTerm => {
            val key = scopeName.scope + "_LIT" + exp.topLevelString()
            val comp = litComps.find(_.nameOrig.compareTo(key) == 0).getOrElse(null)
            val origLoc = numInputPorts + numOfDefinedFunc
            constraints = Or(constraints, Equal(IntVar(comp.outputLabel), IntConst(origLoc.toString)))
          }
          case _:ConstTerm => {
            val key = scopeName.scope + "_c" + exp.topLevelString()
            val comp = consComps.find(_.nameOrig.compareTo(key) == 0).getOrElse(null)
            val origLoc = numInputPorts + numOfDefinedFunc
            constraints = Or(constraints, Equal(IntVar(comp.outputLabel), IntConst(origLoc.toString)))
          }
        }
      })
      numOfDefinedTerm += 1
      res.append((constraints, 1))
    }*/

    def helper(exp: CAST): ArrayBuffer[(BoolOperatorsCAST, Int)] = {
      exp match {
        case bin: BinOp => {
          funcHelper(exp)
          helper(bin.getLeft())
          helper(bin.getRight())
        }
        case un: UnOp => {
          funcHelper(exp)
          helper(un.getOperand())
        }
        case term: InternalTerm => {/*termHelper(term)*/}
        case _ => {}
      }
      res
    }
    helper(originalExp)
  }

  private def isOutputLabel(l: IntVar): Boolean = {
    l.name.endsWith("_o")
  }

  val temp = new ArrayBuffer[BoolOperatorsCAST]()

  def generateSizeBound(sizeBound: Int): mutable.ArrayBuffer[(BoolOperatorsCAST, Int)] = {
    val res = new mutable.ArrayBuffer[(BoolOperatorsCAST, Int)]
    def portSize(str: String): IntVar = IntVar("S_"+str)

    // create size constraints
    var sizeConstraints: BoolOperatorsCAST = BoolConst("true")
    funcComps.foreach(comp => {
      val outSize = portSize(pn2lnMap.getOrElse(comp.outputPort.name, null).name)
      var totalInputSize: ArithOperatorsCAST = IntConst("1")
      comp.inputPorts.foreach(ip => totalInputSize = Plus(totalInputSize, portSize(pn2lnMap.getOrElse(ip.name, null).name)))
      val totalSizeConstraint = Equal(outSize, totalInputSize)
      sizeConstraints = And(sizeConstraints, totalSizeConstraint)
    })

    val circuitOutLoc = IntConst((numInputPorts + components.length - 1).toString)
    val originalSize = originalExp.size()
    val lbound = if(originalSize - sizeBound >= 1) originalSize - sizeBound else 1
    val ubound = originalSize + sizeBound

    // Legal range input size
    funcComps.foreach(comp => {
      comp.inputPorts.foreach(p => {
        val pName = pn2lnMap.getOrElse(p.name, null).name
        val pSize = portSize(pName)
        val upperBoundConstraint = LTE(pSize, IntConst((ubound-(comp.inputPorts.length)).toString))
        val lowerBoundConstraint = GTE(pSize, IntConst(1.toString))
        sizeConstraints = And(sizeConstraints, And(lowerBoundConstraint, upperBoundConstraint))
      })
    })

    // constants have size 1
    consComps.foreach(c => {
      sizeConstraints = And(sizeConstraints, Equal(portSize(pn2lnMap.getOrElse(c.outputPort.name, null).name), IntConst("1")))
    })

    // lits have size 1
    litComps.foreach(c => {
      val inputSize = Equal(portSize(pn2lnMap.getOrElse(c.inputPorts(0).name, null).name), IntConst("1"))
      val outputSize = Equal(portSize(pn2lnMap.getOrElse(c.outputPort.name, null).name), IntConst("1"))
      sizeConstraints = And(sizeConstraints, And(inputSize, outputSize))
    })

    // connections between label and size
    var i = 0
    while (i < numInputPorts) {
      // Originally components instead of funcsComps
      sizeConstraints = components.foldLeft(sizeConstraints) {
        (res, comp) => {
          comp.inputPorts.foldLeft(res)(
            (r, inputPort) => {
              val label = pn2lnMap.getOrElse(inputPort.name, null)
              val iFormula = IntConst(i.toString)
              if (inputPort.breed.compareTo(inputPorts(i).breed) == 0 /*&& inputPort.sort.equals(inputPorts(i).sort)*/) {
                val antecedent = Equal(label, iFormula)
                val consequent = Equal(portSize(label.name), IntConst("1"))
                //logger.info("****** "+Implies(antecedent, consequent))
                And(r, Implies(antecedent, consequent))
              }
              else r
            })
        }
      }
      i += 1
    }

    val numLabels = labels.size

    i = 0
    while (i < numLabels) {
      var j = i + 1
      while (j < numLabels) {
        val labelX = labels.getOrElse(i, null)
        val labelY = labels.getOrElse(j, null)

        val portX = ln2pMap.getOrElse(labelX, null)
        val portY = ln2pMap.getOrElse(labelY, null)

        if (portX.breed.compareTo(portY.breed) == 0 && !(isOutputLabel(labelX) && isOutputLabel(labelY)) && portX.sort.equals(portY.sort)) {
          val antecedent = Equal(labelX, labelY)
          val consequent = Equal(portSize(labelX.name), portSize(labelY.name))
          sizeConstraints = And(sizeConstraints, Implies(antecedent, consequent))
          //temp.append(Implies(antecedent, consequent))
        }
        j += 1
      }
      i += 1
    }

    // Now we bound

    funcComps.foreach(comp => {
      val eq = Equal(pn2lnMap.getOrElse(comp.outputPort.name, null), circuitOutLoc)
      if (comp.outputPort.breed.compareTo(outputPort.breed) == 0) {
        val antecedent = eq
        val upperBoundConstraint = LTE(portSize(comp.outputLabel), IntConst(ubound.toString))
        val lowerBoundConstraint = GTE(portSize(comp.outputLabel), IntConst(lbound.toString))
        sizeConstraints = And(sizeConstraints, Implies(antecedent, And(lowerBoundConstraint, upperBoundConstraint)))
        //sizeConstraints = And(sizeConstraints, And(lowerBoundConstraint, upperBoundConstraint))
      }
    })

    //val x  = CASTUtils.convert2Z3AST(sizeConstraints, new Z3Context("MODEL" -> true))
    //logger.info(x)
    res.append((sizeConstraints, 1))
    res
  }

  // This seems not appropriate, considering bounding size using constraints like labels
  def generateSoftSizeDiff(sizeBound: Int): mutable.ArrayBuffer[(BoolOperatorsCAST, Int)] = {
    logger.info("Original Size Loc: "+outputLocOrigExp)
    val res = new mutable.ArrayBuffer[(BoolOperatorsCAST, Int)]()
    val origLoc = IntConst(outputLocOrigExp.toString)
    val bound = IntConst(sizeBound.toString)
    // Upper bound
    components.foreach(comp => {
      val out = IntVar(comp.outputLabel)
      val antecedent = GTE(out, origLoc)
      val consequent = LTE(Minus(out, origLoc), bound)
      res.append((Implies(antecedent, consequent), 1))
    })
    // Lower bound
    var lowerBConstraints: BoolOperatorsCAST = BoolConst("false")
    funcComps.foreach(comp => {
      lowerBConstraints = Or(lowerBConstraints, LTE(Minus(origLoc, IntVar(comp.outputLabel)), bound))
    })
    res.append((lowerBConstraints, 1))
    res
  }

  def generateWellFormednessConstraints(): BoolOperatorsCAST = {
    val psiRange = generateLabelRangeConstraints()
    val psiDiffLoc = generateDistinctOutputLabelConstraints()
    val psiAcyc = generateAcycConstraints()
    val distinctInputs = generateDistinctInputs()// Bachle: added
    val noBothConstants = generateNoBothConstantInputs()// Bachle: added
    return And(And(distinctInputs, noBothConstants), And(psiRange, And(psiDiffLoc, psiAcyc)))
  }

  def generateConnectionConstraints(): BoolOperatorsCAST = {
    var constraints: BoolOperatorsCAST = BoolConst("true")
    val numInputPorts = inputPorts.length
    val numComponents = components.length

    var i = 0
    while (i < numInputPorts) {
      constraints = components.foldLeft(constraints) {
        (res, comp) => {
          comp.inputPorts.foldLeft(res)(
            (r, inputPort) => {
              val label = pn2lnMap.getOrElse(inputPort.name, null)
              val iFormula = IntConst(i.toString)
              if (inputPort.breed.compareTo(inputPorts(i).breed) == 0) {
                val antecedent = Equal(label, iFormula)
                val consequent = inputPort.equalPortName(inputPorts(i))
                And(r, Implies(antecedent, consequent))
              }
              else {
                And(r, Not(Equal(label, iFormula)))
              }
          })
        }
      }

      i += 1
    }

    val numLabels = labels.size

    i = 0
    while (i < numLabels) {
      var j = i + 1
      while (j < numLabels) {
        val labelX = labels.getOrElse(i, null)
        val labelY = labels.getOrElse(j, null)

        val portX = ln2pMap.getOrElse(labelX, null)
        val portY = ln2pMap.getOrElse(labelY, null)

        if (portX.breed.compareTo(portY.breed) == 0 && !(isOutputLabel(labelX) && isOutputLabel(labelY))) {
          val antecedent = Equal(labelX, labelY)
          val consequent = portX.equalPortName(portY)
          constraints = And(constraints, Implies(antecedent, consequent))
        } else {
          constraints = And(constraints, Not(Equal(labelX, labelY)))
        }
        j += 1
      }
      i += 1
    }

    components.foreach(comp => {
      val eq = Equal(pn2lnMap.getOrElse(comp.outputPort.name, null),
                    IntConst((numInputPorts + numComponents - 1).toString))
      if (comp.outputPort.breed.compareTo(outputPort.breed) == 0) {
        val antecedent = eq
        val consequent = outputPort.equalPortName(comp.outputPort)
        constraints = And(constraints, Implies(antecedent, consequent))
      } else {
        constraints = And(constraints, Not(eq))
      }
    })

    constraints = components.foldLeft(constraints) {
      (res, comp) => {
        And(res, comp.spec)
      }
    }

    return constraints
  }

  def getLModel(model: SolverUtils.LabelModel) = {
    labels.values.foldLeft(new mutable.HashMap[String, Int]()){
      (res, v) => {
        val i  = model.getOrElse(v, throw new RuntimeException("No value in model!!!"))
        res += v.name -> i.value.toInt
        res
      }
    }
  }

  def constructProgramListing(lModel: mutable.HashMap[String, Int]) = {
    val initLength = inputPorts.length
    components.foldLeft(new Array[Component](components.length)){
      (res, comp) => {
        val pos = lModel.getOrElse(comp.outputLabel,-1) - initLength
        res(pos) = comp
        comp.setPosition(pos)
        res
      }
    }
  }

  def refinedProgList(progList: Array[Component], lModel: mutable.HashMap[String, Int]) = {
    val initLength = inputPorts.length
    var workList = immutable.List[Component]() // behave like a stack
    workList = progList(components.length - 1) :: workList
    var refinedProg = Array[(Component, ArrayBuffer[String])]()
    val visitedSet = new mutable.HashSet[Component]()
    visitedSet.add(workList.head)

    def Desc[T : Ordering] = implicitly[Ordering[T]].reverse
    while (workList.nonEmpty){
      workList = workList.sortBy(_.pos)(Desc)
      val curComp = workList.head
      workList = workList.tail // popped head

      val compArgs = new ArrayBuffer[String]()
      curComp.inputLabels.foreach(ipLabel => {
        val l = lModel.getOrElse(ipLabel,-1)
        if(l < initLength){
          compArgs.append(inputPorts(l).nameWithoutID)
        } else {
          val sourceComp = progList(l - initLength)
          if(!visitedSet.contains(sourceComp)){
            workList = sourceComp :: workList
            visitedSet.add(sourceComp)
          }

          // Some unclear from reference implementation
          compArgs.append(sourceComp.outputPort.name)
        }
      })
      refinedProg = ((curComp, compArgs)) +: refinedProg
    }
    refinedProg
  }

  def lValToProg(model: SolverUtils.LabelModel): String = {
    val lModel = getLModel(model)
    val progList = constructProgramListing(lModel)
    val refinedProg = refinedProgList(progList, lModel)
    val refinedProg1 = refinedProg.map{case(comp, args) => (comp, args.filter(_.contains("_o_")))}
    val renameMap = new mutable.HashMap[String, String]()
    var nextID = 1
    refinedProg1.foreach{case (comp, args) => {
      renameMap += comp.outputPort.name -> String.format("o_%02d", new Integer(nextID))
      nextID += 1
    }}

    val refinedProg2 = refinedProg1.map{case (comp, args) => (comp, args.foldLeft(new ArrayBuffer[String]()){
      (res, arg) => {
        res.append(renameMap.getOrElse(arg, arg))
        res
      }
    })}

    var prog = ""
    nextID = 0
    refinedProg2.foreach{case (comp, args) => {
      val outputName = String.format("o_%02d", new Integer(nextID + 1))
      prog += comp.print(args, outputName)
      prog += "\n"
      nextID += 1
    }}
    prog
  }

  def generateCircuitExpression(model: SolverUtils.LabelModel): String = {
    val lModel = getLModel(model)
    val progList = constructProgramListing(lModel)
    val refinedProg = refinedProgList(progList, lModel).reverse

    val tmpMap = new mutable.HashMap[String, Int]()
    refinedProg.zipWithIndex.foreach{case (comp, i) => tmpMap += comp._1.outputPort.name -> i}

    def generateSubexpr(i: Int): String = {
      val (comp, compArgs) = refinedProg(i)
      val production = comp.production
      production match {
        case e: SymbolGTerm => e.symbol
        case e: LiteralGTerm => e.litGTerm.litStr
        case e: FunGTerm => {
          val opSym = e.funName
          var retVal = String.format("(%s", opSym)
          compArgs.foreach(compArg => {
            retVal += " "
            retVal += generateSubexpr(tmpMap.getOrElse(compArg, -1))
          })
          retVal += ")"
          retVal
        }
      }
    }

    val funcRetType = synthFun.sort.toMangleString()
    val argList = synthFun.argList.args.foldLeft(""){(res, arg) => res+"("+arg.name+" "+arg.sort.toMangleString()+")"+" "}
    return String.format("(define-fun %s (%s) %s %s)", funcName, argList, funcRetType, generateSubexpr(0))
  }
}

object Circuit{
  var numCircuits = 0
}