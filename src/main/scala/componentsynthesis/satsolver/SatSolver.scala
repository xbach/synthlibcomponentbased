package componentsynthesis.satsolver

import com.microsoft.z3._
import common.astutils.{CASTUtils, SubstitutionMaker}
import common.coreast._
import componentsynthesis.ComponentSynthesisDriver.{ExampleMultiFuncs, InputVariationMap}
import componentsynthesis.core.{Circuit, Port}
import componentsynthesis.satsolver.SolverUtils.{GeneralModel, LabelModel}
import config.ConfigOptions
import org.apache.log4j.Logger

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 4/3/17.
  */
object MaxSatAlgorithm extends Enumeration{
  type Algorithm = Value
  val FUMALIK = Value("FuMalik")
  val STANDARD_WEIGHTED = Value("StardardWeighted")
}
abstract class SatSolver[T](algorithm: MaxSatAlgorithm.Algorithm) {
  protected val logger = Logger.getLogger(this.getClass)

  protected lazy val hardClauses = new mutable.ArrayBuffer[WCAST[T]]()
  protected lazy val softClauses = new mutable.ArrayBuffer[WCAST[T]]()

  type ConstraintWWeight = ArrayBuffer[ArrayBuffer[(BoolOperatorsCAST, Int)]]

  def clear(): Unit = {
    softClauses.clear()
    hardClauses.clear()
  }

  def maxSat(variableNames: mutable.HashSet[String]): Option[LabelModel] = {
    algorithm match {
      case _: MaxSatAlgorithm.FUMALIK.type => fumalik(variableNames)
      case _: MaxSatAlgorithm.STANDARD_WEIGHTED.type => standardMaxSat(variableNames)
    }
  }

  def fumalik(variableNames: mutable.HashSet[String]): Option[LabelModel] = {
    null
  }

  def standardMaxSat(variableNames: mutable.HashSet[String]): Option[LabelModel]

  def delete(): Unit = {
    hardClauses.clear()
    softClauses.clear()
  }
  def push(): Unit
  def pop(): Unit
  def substitute(exp: CAST, from: mutable.ArrayBuffer[CAST], to: mutable.ArrayBuffer[CAST], reduceImplies: Boolean): T

  def mkWCAST(ast: CAST, gr: String): WCAST[T]
  def mkWCASTWeight(ast: CAST, gr: String, w: Int): WCAST[T]

  def mkWCAST(ast: T, gr: String): WCAST[T] = WCAST(ast, gr, None)
  def mkWCASTWeight(ast: T, gr: String, w: Int): WCAST[T] = WCAST(ast, gr, Some(w))

  def mkHard(clause: T): WCAST[T] = mkWCAST(clause, "hard")
  def mkHard(clause: CAST): WCAST[T] = mkWCAST(clause, "hard")

  def addHard(clause: WCAST[T]): Unit = hardClauses.append(clause)
  def addSoft(clause: WCAST[T]): Unit = softClauses.append(clause)
  def consumeHardSoftThenClear(): Unit
  def mkSolver(): SatSolver[T]
  def model(variables: mutable.HashSet[VarTerm]): GeneralModel
  def check(): Boolean // true => sat, false => unsat

  //def mkT(exp: CAST): T
  //def CASTConversion(cast: CAST): T

  protected def generateVerificationConstraint(spec: CAST, specConn: CAST,
                                             circuits: ArrayBuffer[Circuit], con: CAST,
                                             model: LabelModel): T

  private def generateConstraintForExamples(example: ExampleMultiFuncs,
                                            circuits: ArrayBuffer[Circuit], i: Int,
                                            spec: CAST, specConn: CAST, con: CAST): (T, ArrayBuffer[VarTerm]) = {
    val from = new ArrayBuffer[CAST]()
    val to = new ArrayBuffer[CAST]()
    val outputVars = new ArrayBuffer[VarTerm]()
    circuits.foreach(circuit => {
      // substitute output port
      from.append(circuit.outputPort.portNameCAST)
      val output = circuit.outputPort.genVarCopyForIteration(i)
      to.append(output)
      outputVars.append(output)

      // input ports
      circuit.inputPorts.foreach(p => {
        from.append(p.portNameCAST)
        to.append(p.genVarCopyForIteration(i))
      })

      circuit.components.foreach(comp => {
        // substitute output port
        from.append(comp.outputPort.portNameCAST)
        to.append(comp.outputPort.genVarCopyForIteration(i))

        // input ports
        comp.inputPorts.foreach(p => {
          from.append(p.portNameCAST)
          to.append(p.genVarCopyForIteration(i))
        })
      })

      if(example.size > 0) {
        val exampleForCircuit = example.getOrElse(circuit.funcName, null)
        // Make sure the map holds expected value of the circuit, if it was intended to do so.

        assert(exampleForCircuit.exists(_._1.getName().compareTo(circuit.funcName) == 0), "Map does not hold expected output of the circuit!")
        exampleForCircuit.foreach(e => {
          // Make sure the variable name is not the circuit name (function name)
          // which holds the expected value.
          if (e._1.getName().compareTo(circuit.funcName) != 0) {
            from.append(e._1)
            to.append(e._2)
          }
        })
      }
    })

    val conSub = substitute(con, from, to, false)
    val specConSub = substitute(specConn, from, to, false)
    val specSub = if(example.size ==0) substitute(spec, from, to, false) else {
      //logger.info("Ex: "+example)
      //logger.info("Bef: "+spec)
      val x = substitute(spec, from, to, true)
      logger.info("Reduced Implies at CAST during substitution ...")
      //logger.info("Aft: "+x)
      x
    }
    (mkAnd(specSub, specConSub, conSub), outputVars)
  }

  def mkAnd(args: T*): T

  private def outputCoverage(outs: ArrayBuffer[ArrayBuffer[VarTerm]], toPrint: Boolean = true) = {
    // --- begin generating soft constraints for output coverage ----
    var i = 0
    val norm = new ArrayBuffer[ArrayBuffer[InternalTerm]]()
    while (i<outs(0).length){
      val temp = new ArrayBuffer[InternalTerm]()
      outs.foreach(o => temp.append(o(i)))
      norm.append(temp)
      i += 1
    }

    norm.zipWithIndex.foreach{case (outs, index) => {
      var i = 0
      while (i<outs.length){
        var j = i+1
        while (j<outs.length){
          val temp = Not(Equal(outs(i), outs(j)))
          if(toPrint){
            logger.info("Output coverage")
            logger.info(temp)
          }
          addSoft(mkWCASTWeight(temp, "OUT"+index, 1))
          j += 1
        }
        i += 1
      }
    }}

    logger.info("Added Output Coverage")
    // ---- End output coverage soft constraints ---------
  }

  protected def addSoftConstraints(constraints: ConstraintWWeight, label: String, circuits: mutable.Buffer[Circuit], check: Circuit => Boolean, printFormula: Boolean = false) = {
    constraints.zipWithIndex.foreach{case (circuitConstraints, circuitNumber) => {
      if(check(circuits(circuitNumber))) {
        logger.info("Added "+label+circuitNumber.toString)
        circuitConstraints.foreach { case (constraint, weight) => {
          if(printFormula)
            logger.info(label+": "+constraint+" w: "+weight)
          addSoft(mkWCASTWeight(constraint, label + circuitNumber.toString, weight))
        }}
      }
    }}
  }

  protected def selectRepresentativeExamples(examples: mutable.HashSet[ExampleMultiFuncs],
                                             circuits: ArrayBuffer[Circuit]):
  (mutable.HashSet[ExampleMultiFuncs], mutable.HashSet[ExampleMultiFuncs]) = {
    val eval = new CASTVisitorBase {
      override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = ???

      override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = ???

      override def visitIntVar(intVar: IntVar): VisitorReturnType = ???
    }

    if(examples.size > ConfigOptions.maxExamples){
      examples.partition{example => {
        circuits.exists(circuit => {
          val valueMap = example.getOrElse(circuit.funcName, null)
          val (variableValues, expectedOutput) = valueMap.partition{case(term, _) => term.getName().compareTo(circuit.funcName) != 0}
          val (from, to) = variableValues.foldLeft((new ArrayBuffer[CAST](), new ArrayBuffer[CAST])){
            (res, item) => {
              res._1.append(CASTUtils.decVar2OrigName(item._1, circuit.lineNumer))
              res._2.append(item._2)
              res
            }
          }
          val substitutionMaker = new SubstitutionMaker(from, to, false)
          val executed = circuit.originalExp.accept(substitutionMaker).asInstanceOf[CAST].accept(eval)
          val expected = expectedOutput.head._2.accept(eval)
          !executed.eq(expected)
        })
      }}
    } else (examples, new mutable.HashSet[ExampleMultiFuncs]())
  }

  /*private def sortExamples(examples: mutable.HashSet[ExampleMultiFuncs],
                           circuits: ArrayBuffer[Circuit]): Array[ExampleMultiFuncs] = {
    val groupedExamples = examples.toArray.groupBy(ex => {
      ex.filter(p => circuits.exists(circuit => circuit.funcName.compareTo(p._1) == 0))
    })

    val sortedExamples = groupedExamples.toArray.sortBy(f => f._2.length)
    sortedExamples.foldLeft(Array[ExampleMultiFuncs]()){_ ++ _._2}
  }*/

  // TODO: implement GenerateCircuitSimilarityConstraints(circuits)
  def synthesize(spec: CAST, specInputPorts: ArrayBuffer[Port], specConn: CAST,
                 circuits: ArrayBuffer[Circuit],
                 examples: mutable.HashSet[ExampleMultiFuncs],
                 inputVariations: InputVariationMap): Boolean = {
    var wellFormed: BoolOperatorsCAST = BoolConst("true")
    var connections: BoolOperatorsCAST = BoolConst("true")
    circuits.foreach(circuit => {
      wellFormed = And(wellFormed, circuit.generateWellFormednessConstraints())
      connections = And(connections, circuit.generateConnectionConstraints())
    })

    //circuits.foreach(logger.info(_))
    logger.debug("Well formed:"+wellFormed)
    logger.debug("Connections:"+connections)
    logger.debug("Spec:"+spec)
    logger.debug("SpecConn:"+specConn)

    // ---- Begin adding hard constraints ------
    logger.info("==============Begin Hard Constraints===================")
    var iter = 0
    addHard(mkHard(wellFormed))
    logger.info("Added Well Formed Hard Constraints")
    // Note we only need this if the spec is not in the example form, meaning the spec is logical constraints
    //val (first,_) = generateConstraintForExamples(new ExampleMultiFuncs, circuits, iter, spec, specConn, connections)
    //addHard(mkHard(first))

    //TODO: verify solution generated by selected examples with remaining examples
    // Examples selections if the number of examples are too large.
    val (selectedExamples, remainingExamples) = selectRepresentativeExamples(examples, circuits)

    if(remainingExamples.nonEmpty) {
      selectedExamples.add(remainingExamples.head)
      remainingExamples.remove(remainingExamples.head)
    }
    logger.info("Selected Examples: "+selectedExamples)
    // Newly added for t9
    //val sortedExamples = selectedExamples //sortExamples(selectedExamples, circuits)
    //logger.info("Sorted Examples: "+sortedExamples)
    // End for t9
    iter += 1
    val outs = new ArrayBuffer[ArrayBuffer[VarTerm]]()
    selectedExamples.foreach(ex => {
      val (exConstraint, outVars) = generateConstraintForExamples(ex, circuits, iter, spec, specConn, connections)
      addHard(mkHard(exConstraint))
      logger.info("Added Spec, SpecCon, and Connections Hard Constraints for example: " + iter)
      outs.append(outVars)
      iter += 1
    })

    val softSizeBound = circuits.foldLeft(new ConstraintWWeight){(res, circuit) => {
      res.append(circuit.generateSizeBound(sizeBound = ConfigOptions.sizeBound))
      //circuit.temp.foreach(c => addHard(mkHard(c)))
      res
    }}

    softSizeBound.zipWithIndex.foreach{case (constraints, index) => {
      if(circuits(index).origExpHasLitVarTerm) {
        logger.info("Added SizeBound Hard Constraints")
        constraints.foreach { case (constr, weight) => {
          //logger.info("Size Constraints: "+constr)
          //addSoft(mkWCASTWeight(constr, "SizeDiff"+index.toString, weight))
          addHard(mkHard(constr))
        }}
      }
    }}

    logger.info("==============End Hard Constraints===================")

    // ----- End adding hard constraints ------

    def checkHasLiteral(circuit: Circuit): Boolean = {
      circuit.origExpHasLitVarTerm
    }

    def noCheck(circuit: Circuit): Boolean = {
      true
    }

    val fewerIdenSubTree = circuits.foldLeft(new ConstraintWWeight){(res, circuit) => {
      res.append(circuit.generateSoftFewerIdenticalSubTree())
      res
    }}

    val (softStructural, fewerConsts) = circuits.foldLeft((new ConstraintWWeight, new ConstraintWWeight)){(res, circuit) => {
      val res1 = res._1
      val res2 = res._2
      val (structuralConstraints, cache) = circuit.generateSoftStructuralConstraints()
      //structuralConstraints.appendAll(circuit.generateSoftLocationConstraints())
      res1.append(structuralConstraints)
      res2.append(circuit.generateFewerConstants(cache))
      (res1, res2)
    }}

    val softLocs = circuits.foldLeft(new ConstraintWWeight){(res, circuit) => {
      res.append(circuit.generateSoftLocationConstraints())
      res
    }}

    val softInputVariation = circuits.foldLeft(new ConstraintWWeight){(res, circuit) => {
      val variationMap = inputVariations.getOrElse(circuit.funcName, null)
      res.append(circuit.generateInputVariationConstraints(variationMap))
      res
    }}

    val variableScore = circuits.foldLeft(new ConstraintWWeight){(res, circuit) => {
      val scoreMap = ConfigOptions.variableScore.getOrElse(circuit.funcName, null)
      if(scoreMap != null){
        res.append(circuit.generateVariableWScore(scoreMap))
      }
      res
    }}

    // ---- Begin adding soft constraints -----
    logger.info("==============Begin Soft Constraints===================")
    addSoftConstraints(softLocs, "Structural", circuits, checkHasLiteral, true)
    //addSoftConstraints(softStructural, "Structural", circuits, checkHasLiteral, true)
    //addSoftConstraints(fewerConsts, "Structural", circuits, checkHasLiteral, true)
    addSoftConstraints(fewerConsts, "Structural2", circuits, checkHasLiteral, true)
    outputCoverage(outs)
    addSoftConstraints(fewerIdenSubTree, "IdenSubTree", circuits, checkHasLiteral, false)
    //addSoftConstraints(softInputVariation, "InputVariation", circuits, checkHasLiteral, false)
    addSoftConstraints(variableScore, "VarScore", circuits, noCheck, true)
    logger.info("==============End Soft Constraints===================")
    // ---- End adding soft constraints -------

    //logger.info("Constraints:"+hardConstraints)
    logger.info("Solving model ...")
    val variables = circuits.foldLeft(new mutable.HashSet[String]()){
      (res, circuit) => {
        circuit.labels.values.foreach(v => res.add(v.name))
        res
      }
    }

    circuits.foreach(c => logger.info("Circuit: "+ c.funcName + " -- Num Components: "+c.components.length))
    val stopSize = remainingExamples.size
    var loopSize = 0
    var foundSolution = false
    val verifier = mkSolver()
    while (loopSize == 0 || (loopSize<stopSize && foundSolution == false)) {
      maxSat(variables) match {
        case None => {
          logger.info("Synthesis Failed")
          //clear()
        }
        case Some(model) => {
          logger.info("Model: " + model)
          logger.info("Model solved!")
          logger.info("Now verifying...")
          val verificationConstraint = generateVerificationConstraint(spec, specConn, circuits, connections, model)
          verifier.push()
          verifier.addHard(mkHard(verificationConstraint))
          verifier.consumeHardSoftThenClear()
          if(verifier.check() == false) {
            logger.info("Found solution!")
            foundSolution = true
            logger.debug("Synthesized program:")
            logger.debug(circuits.foldLeft("")((res, circuit) => {
              res + circuit.funcName + "\n" + circuit.lValToProg(model) + "\n"
            }))

            val printedCircuits = mutable.HashSet[String]()
            circuits.foreach(circuit => {
              if (!printedCircuits.contains(circuit.funcName)) {
                logger.info(circuit.generateCircuitExpression(model))
                printedCircuits.add(circuit.funcName)
              }
            })
            //clear()
          } else {
            // TODO: consider output coverage constraints once new examples arrive.
            // However, that may not be needed since we already considered representative examples
            //verifier.model()
            logger.info("Need to extract counterexamples here...")
          }
          verifier.pop()
        }
      }
      loopSize += 1
    }
    clear()
    return foundSolution
  }
}
abstract class WeightedSatSolverCAST(algorithm: MaxSatAlgorithm.Algorithm) extends SatSolver[CAST](algorithm) {

  lazy val solverContext = {
    val cfg = new java.util.HashMap[String, String]()
    cfg.put("model", "true")
    new Context(cfg)
  }

  lazy val solver = solverContext.mkOptimize()

  override def delete(): Unit = {
    super.delete()
    solverContext.dispose()
  }
  override def push(): Unit = solver.Push()
  override def pop(): Unit = solver.Pop()

  override def mkWCAST(ast: CAST, gr: String): WCAST[CAST] = {
    WCAST(ast, gr, None)
  }

  override def mkWCASTWeight(ast: CAST, gr: String, w: Int): WCAST[CAST] = {
    WCAST(ast, gr, Some(w))
  }

  override def mkAnd(args: CAST*) = {
    var and: BoolOperatorsCAST = BoolConst("true")
    args.foreach(arg => {
      and = And(and, arg.asInstanceOf[BoolOperatorsCAST])
    })
    and
  }

  override def substitute(exp: CAST, from: mutable.ArrayBuffer[CAST], to: mutable.ArrayBuffer[CAST], reduceImplies: Boolean): CAST = {
    val sub = new SubstitutionMaker(from, to, reduceImplies)
    exp.accept(sub).asInstanceOf[CAST]
  }

}
case class Z3WeightedSatSolver(algorithm: MaxSatAlgorithm.Algorithm) extends WeightedSatSolverCAST(algorithm){

  override def consumeHardSoftThenClear(): Unit = {
    hardClauses.foreach(hc => solver.Add(CASTUtils.convert2Z3ASTJava(hc.cast, solverContext)))
    softClauses.foreach(sc => solver.AssertSoft(CASTUtils.convert2Z3ASTJava(sc.cast, solverContext), sc.weight.getOrElse(1), sc.group))
    hardClauses.clear()
    softClauses.clear()
  }

  override def generateVerificationConstraint(spec: CAST, specConn: CAST, circuits: ArrayBuffer[Circuit], con: CAST, model: LabelModel): CAST = {
    val constraints = new ArrayBuffer[CAST]()
    val from = new ArrayBuffer[CAST]()
    val to = new ArrayBuffer[CAST]()
    circuits.foreach{circuit => {
      circuit.labels.values.foreach(l => {
        from.append(l)
        to.append(model.getOrElse(l, null))
      })
    }}
    constraints.append(substitute(con, from, to, false))
    constraints.append(specConn)
    constraints.append(Not(spec.asInstanceOf[BoolOperatorsCAST]))
    return CASTUtils.joinConstraint(constraints)
  }

  override def standardMaxSat(variableNames: mutable.HashSet[String]): Option[LabelModel] = {
    val params = solverContext.mkParams()
    params.add("opt.priority", ConfigOptions.optStrategy)
    //params.add("opt.priority", "box")
    //params.add("timeout", 120000)
    solver.setParameters(params)

    val goal = solverContext.mkGoal(true, true, false) //TODO clarify these parameters
    hardClauses.foreach({ case h => goal.add(CASTUtils.convert2Z3ASTJava(h.cast, solverContext)) })
    val solveEqs: Tactic = solverContext.mkTactic("solve-eqs")
    val simp: Tactic = solverContext.mkTactic("simplify")
    val prop: Tactic = solverContext.mkTactic("propagate-values")
    val ctx_simp: Tactic = solverContext.mkTactic("ctx-simplify")
    val simplification = solverContext.andThen(simp, prop, solveEqs, ctx_simp)
    val res = simplification.apply(goal)
    res.getSubgoals().foreach(goal => goal.getFormulas.foreach(hc => solver.Add(hc)))

    //hardClauses.foreach(hc => solver.Add(CASTUtils.convert2Z3ASTJava(hc.cast, solverContext)))
    softClauses.foreach(sc => solver.AssertSoft(CASTUtils.convert2Z3ASTJava(sc.cast, solverContext), sc.weight.getOrElse(1), sc.group))
    //logger.info(solver.toString)
    val status = solver.Check()
    if (status.equals(Status.SATISFIABLE)) {
      val model = solver.getModel()
      logger.info("Full Model: "+model)
      val resModel = new LabelModel
      variableNames.foreach(v => {
        val res = model.eval(solverContext.mkIntConst(v), true)
        resModel += IntVar(v) -> IntConst(res.asInstanceOf[IntNum].getInt.toString)
      })
      Some(resModel)
    } else if (status.equals(Status.UNSATISFIABLE)) {
      logger.info("UNSAT")
      /*val pr = solverContext.mkParams()
      pr.add("unsat-core", true)
      val normSolver = solverContext.mkSolver()
      normSolver.setParameters(pr)
      var i = 0
      hardClauses.foreach(c => {
        normSolver.assertAndTrack(CASTUtils.convert2Z3ASTJava(c.cast, solverContext), solverContext.mkBoolConst(CASTUtils.convert2Z3ASTJava(c.cast, solverContext).toString))
        i += 1
      })
      normSolver.check()
      normSolver.getUnsatCore*/

      return None
    } else {
      //throw new UnsupportedOperationException()
      logger.info("TIMED OUT!")
      return None
    }
  }

  override def check(): Boolean = {
    val status = solver.Check()
    if(status.equals(Status.SATISFIABLE))
      return true
    else return false
  }

  override def mkSolver(): SatSolver[CAST] = {
    return Z3WeightedSatSolver(algorithm)
  }

  private def cast2Expr(cast: CAST): Expr = {
    cast match {
      case i: IntVar => solverContext.mkIntConst(i.name)
      case b: BoolVar => solverContext.mkBoolConst(b.name)
    }
  }

  private def expr2CAST(expr: Expr): CAST = {
    if(expr.isInt)
      IntConst(expr.asInstanceOf[IntNum].getInt.toString)
    else if(expr.isBool)
      BoolConst(expr.asInstanceOf[BoolExpr].isTrue.toString)
    else throw new RuntimeException("Not yet handled type: "+expr)
  }

  override def model(variables: mutable.HashSet[VarTerm]): GeneralModel = {
    val sat = check()
    val resModel = new GeneralModel
    if(sat){
      val model = solver.getModel
      variables.foreach(v => {
        val value = expr2CAST(model.eval(cast2Expr(v), true)).asInstanceOf[ConstTerm]
        resModel += v -> value
      })
    }
    resModel
  }
}
/*case class OptiMathSatSolver(algorithm: MaxSatAlgorithm.Algorithm) extends WeightedSatSolverCAST(algorithm){
  override def generateVerificationConstraint(spec: CAST, specConn: CAST, circuits: ArrayBuffer[Circuit], con: CAST, model: Model): CAST = null
  val optimathSat = "/home/dxble/workspace/satsolver/optimathsat-1.4.1-linux-64-bit/bin/optimathsat"

  override def standardMaxSat(variableNames: mutable.HashSet[String]): Option[Model] = {
    //val params = solverContext.mkParams()
    //params.add("opt.priority", "pareto")
    //params.add("produce-models", true)
    //params.add("timeout", 120000)
    //solver.setParameters(params)

    val goal = solverContext.mkGoal(true, true, false) //TODO clarify these parameters
    hardClauses.foreach({ case h => goal.add(CASTUtils.convert2Z3ASTJava(h.cast, solverContext)) })
    val solveEqs: Tactic = solverContext.mkTactic("solve-eqs")
    val simp: Tactic = solverContext.mkTactic("simplify")
    val prop: Tactic = solverContext.mkTactic("propagate-values")
    val ctx_simp: Tactic = solverContext.mkTactic("ctx-simplify")
    val simplification = solverContext.andThen(simp, prop, solveEqs, ctx_simp)
    val res = simplification.apply(goal)
    res.getSubgoals().foreach(goal => goal.getFormulas.foreach(hc => solver.Add(hc)))

    //hardClauses.foreach(hc => solver.Add(CASTUtils.convert2Z3ASTJava(hc.cast, solverContext)))
    softClauses.foreach(sc => solver.AssertSoft(CASTUtils.convert2Z3ASTJava(sc.cast, solverContext), sc.weight.getOrElse(1), sc.group))
    //logger.info(solver.toString)
    val smtLib = "(set-option :produce-models true)\n" + solver.toString + "\n (get-model)"
    import java.io._
    val pw = new PrintWriter(new File("constraint.smt"))
    pw.write(smtLib)
    pw.close

    import sys.process._

    val cmd = optimathSat + " " + "constraint.smt" + " -optimization.priority="+ConfigOptions.optStrategy
    val result =  cmd.!!

    val lines = scala.io.Source.fromString(result).getLines()
    var sat = false
    val resModel = new Model
    while (lines.hasNext){
      val l = lines.next()
      if(l.compareTo("sat") == 0){
        sat = true
      }
      if(!sat)
        return None
      else {
        println(l)
        if(l.compareTo("sat") != 0) {
          val variable2Value = l.replace("(", "").replace(")", "").trim.split(" ")
          if (variableNames.contains(variable2Value(0)))
            resModel += IntVar(variable2Value(0)) -> IntConst(variable2Value(1))
        }
      }
    }
    if(resModel.isEmpty) return None
    else return Some(resModel)
  }
}*/