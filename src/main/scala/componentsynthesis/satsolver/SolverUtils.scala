package componentsynthesis.satsolver

import common.astutils.ToZ3ASTConverter
import common.coreast._
import z3.scala.Z3Context

import scala.collection.mutable

/**
  * Created by dxble on 3/28/17.
  */
object SolverUtils {
  type LabelModel = mutable.HashMap[IntVar, IntConst]
  type GeneralModel = mutable.HashMap[VarTerm, ConstTerm]

  lazy val z3 = new Z3Context("MODEL" -> true)

  def testZ32() = {
    val bvar1 = BoolVar("x1")
    val ivar1 = IntVar("i1")
    val ivar2 = IntVar("i2")
    val cs1 = GTE(ivar1, ivar2)
    val cs2 = Greater(ivar1, ivar2)
    val z3 = new Z3Context("MODEL" -> true)
    val converter = new ToZ3ASTConverter(z3)
    val ast1 = cs1.accept(converter).asInstanceOf[Z3ASTReturnType].getAST()
    val ast2 = cs2.accept(converter).asInstanceOf[Z3ASTReturnType].getAST()
    println(ast1)
    println(ast2)
    val solver = z3.mkSolver()
    val cstr1 = z3.mkAnd(ast1, z3.mkNot(ast2))
    solver.assertCnstr(cstr1)
    println("Constraint: "+cstr1)
    println(solver.check())
    //println("Model available? "+solver.isModelAvailable)
    //if(solver.isModelAvailable)
    //  println(solver.getModel())
    // Need reset
    solver.reset()
    val cstr2 = z3.mkAnd(ast2, z3.mkNot(ast1))
    solver.assertCnstr(cstr2)
    println("Constraint: "+cstr2)
    println(solver.check())
    //println("Model available? "+solver.isModelAvailable)
    //if(solver.isModelAvailable)
    //  println(solver.getModel())
    //else {
    //  println(solver.getUnsatCore().foreach(f => println(f)))
    //}
    val cs3 = Or(And(cs1, Not(cs2)), And(cs2, Not(cs1)))
    val cs3AST = cs3.accept(converter).asInstanceOf[Z3ASTReturnType].getAST()
    // Need reset
    solver.reset()
    solver.assertCnstr(cs3AST)
    println("Constraint: "+cs3AST)
    println(solver.check())
    //println("Model available? "+solver.isModelAvailable)
    //if(solver.isModelAvailable)
      println(solver.getModel())
    z3.delete()
  }
  def testZ3() = {
    val solver = z3.mkSolver()
    val sort = z3.mkIntSort()
    val c  = z3.mkImplies(z3.mkEq(z3.mkInt(10, sort), z3.mkInt(10, sort)), z3.mkEq(z3.mkConst(z3.mkStringSymbol("VX"), sort), z3.mkInt(10, sort)))
    solver.assertCnstr(c)
    solver.check() match {
      case Some(x) => {
        //println(solver.getModel())
      }
    }
    println(solver.getModel())
  }
  def main(args: Array[String]): Unit = {
    testZ3()
    //testZ32()
  }
}
