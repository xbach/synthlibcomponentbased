package componentsynthesis.satsolver

import com.microsoft.z3.{IntNum, _}
import common.astutils.{CAST2Z3Java, CASTUtils, ToZ3ASTConverter}
import common.coreast.{Z3ASTReturnType, _}
import componentsynthesis.core.Circuit
import z3.scala.Z3AST
import componentsynthesis.satsolver.SolverUtils.LabelModel

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 4/4/17.
  */
/*case class Z3JavaWeightedSatSolver(algorithm: MaxSatAlgorithm.Algorithm) extends SatSolver[BoolExpr](algorithm) {

  override def generateVerificationConstraint(spec: CAST, specConn: CAST, circuits: ArrayBuffer[Circuit], con: CAST, model: Model): BoolExpr = ???
  lazy val solverContext = {
    val cfg = new java.util.HashMap[String, String]()
    cfg.put("model", "true")
    new Context(cfg)
  }

  lazy val solver = solverContext.mkOptimize()

  override def delete(): Unit = {
    super.delete()
    solverContext.dispose()
  }
  override def push(): Unit = solver.Push()
  override def pop(): Unit = solver.Pop()

  def mkWCAST(ast: CAST, gr: String): WCAST[BoolExpr] = {
    val converter = new CAST2Z3Java(solverContext)
    WCAST(ast.accept(converter).asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[BoolExpr], gr, None)
  }

  def mkWCASTWeight(ast: CAST, gr: String, w: Int): WCAST[BoolExpr] = {
    val converter = new CAST2Z3Java(solverContext)
    WCAST(ast.accept(converter).asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[BoolExpr], gr, Some(w))
  }

  override def mkAnd(args: BoolExpr*) = {
    var and = solverContext.mkTrue()
    args.foreach(arg => {
      and = solverContext.mkAnd(and, arg)
    })
    and
  }

  override def substitute(exp: CAST, from: mutable.ArrayBuffer[CAST], to: mutable.ArrayBuffer[CAST], reduceImplies: Boolean): BoolExpr = {
    val converter = new CAST2Z3Java(solverContext)
    val z3Exp = exp.accept(converter).asInstanceOf[Z3ASTJavaRetType].ast
    val fromExps = from.foldLeft(Array[Expr]()){(res, e) => {
      res :+ e.accept(converter).asInstanceOf[Z3ASTJavaRetType].ast
    }}
    val toExps = to.foldLeft(Array[Expr]()){(res, e) => {
      res :+ e.accept(converter).asInstanceOf[Z3ASTJavaRetType].ast
    }}
    z3Exp.substitute(fromExps, toExps).asInstanceOf[BoolExpr]
  }

  override def standardMaxSat(variableNames: mutable.HashSet[String]): Option[Model] = {
    val goal = solverContext.mkGoal(true, true, false) //TODO clarify these parameters
    hardClauses.foreach({ case h => goal.add(h.cast) })
    val solveEqs: Tactic = solverContext.mkTactic("solve-eqs")
    val simp: Tactic = solverContext.mkTactic("simplify")
    val prop: Tactic = solverContext.mkTactic("propagate-values")
    val ctx_simp: Tactic = solverContext.mkTactic("ctx-simplify")
    //val css: Tactic = solverContext.mkTactic("ctx-solver-simplify")
    val simplification = solverContext.andThen(solveEqs, prop, simp, ctx_simp)
    val res = simplification.apply(goal)
    val simHard = res.getSubgoals()(0).getFormulas
    simHard.foreach(hc => solver.Add(hc))
    val params = solverContext.mkParams()
    params.add("opt.priority", "pareto")
    //params.add("elim_01", false)
    solver.setParameters(params)
    //hardClauses.foreach(hc => solver.Add(hc.cast))
    softClauses.foreach(sc => solver.AssertSoft(sc.cast, 1, sc.group))
    val status = solver.Check()
    if (status.equals(Status.SATISFIABLE)) {
      val model = solver.getModel()
      val resModel = new Model
      variableNames.foreach(v => {
        val res = model.eval(solverContext.mkIntConst(v), true)
        resModel += IntVar(v) -> IntConst(res.asInstanceOf[IntNum].getInt.toString)
      })
      Some(resModel)
    } else if (status.equals(Status.UNSATISFIABLE)) {
      return None
    } else {
      throw new UnsupportedOperationException()
    }
  }

  override def fumalik(variableNames: mutable.HashSet[String]) = {
    null
  }
}
case class Z3ScalaWeightedSatSolver(algorithm: MaxSatAlgorithm.Algorithm) extends SatSolver[Z3AST](algorithm) {

  lazy val solverContext = new z3.scala.Z3Context("MODEL" -> true)

  lazy val solver = solverContext.mkOptimizer()

  override def generateVerificationConstraint(spec: CAST, specConn: CAST, circuits: ArrayBuffer[Circuit], con: CAST, model: Model): Z3AST = ???
  override def delete(): Unit = {
    super.delete()
    solverContext.delete()
  }
  override def push(): Unit = solver.push()
  override def pop(): Unit = solver.pop()

  def mkWCAST(ast: CAST, gr: String): WCAST[Z3AST] = {
    WCAST(CASTUtils.convert2Z3AST(ast, solverContext), gr, None)
  }

  def mkWCASTWeight(ast: CAST, gr: String, w: Int): WCAST[Z3AST] = {
    WCAST(CASTUtils.convert2Z3AST(ast, solverContext), gr, Some(w))
  }

  override def mkAnd(args: Z3AST*) = {
    var and = solverContext.mkTrue()
    args.foreach(arg => {
      and = solverContext.mkAnd(and, arg)
    })
    and
  }

  override def substitute(exp: CAST, from: mutable.ArrayBuffer[CAST], to: mutable.ArrayBuffer[CAST], reduceImplies: Boolean): Z3AST = {
    val converter = new ToZ3ASTConverter(solverContext)
    val z3Exp = exp.accept(converter).asInstanceOf[Z3ASTReturnType].ast
    val fromExps = from.foldLeft(Array[Z3AST]()){(res, e) => {
      res :+ e.accept(converter).asInstanceOf[Z3ASTReturnType].ast
    }}
    val toExps = to.foldLeft(Array[Z3AST]()){(res, e) => {
      res :+ e.accept(converter).asInstanceOf[Z3ASTReturnType].ast
    }}
    solverContext.substitute(z3Exp,fromExps, toExps)
  }

  override def standardMaxSat(variableNames: mutable.HashSet[String]): Option[Model] = {
    hardClauses.foreach(hc => solver.assertCnstr(hc.cast))
    softClauses.foreach(sc => solver.assertCnstr(sc.cast, 1, sc.group))
    solver.check() match {
      case None => {
        return None
      }
      case Some(sat) => {
        if(sat){
          val model = solver.getModel()
          val resModel = new Model
          variableNames.foreach(v => {
            val res = model.evalAs[Int](solverContext.mkIntConst(v))
            resModel += IntVar(v) -> IntConst(res.getOrElse(throw new RuntimeException("Cannot be None: "+res)).toString)
          })
          return Some(resModel)
        }else{
          return None
        }
      }
    }
  }
}
*/