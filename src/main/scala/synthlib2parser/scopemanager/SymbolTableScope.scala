package main.scala.synthlib2parser.scopemanager

import scala.collection.mutable
import main.scala.synthlib2parser._
import main.scala.synthlib2parser.scopemanager.SymtabEntryKind.SymtabEntryKind
import utils.MyStack

/**
  * Created by dxble on 7/10/16.
  */
object SymtabEntryKind extends Enumeration {
  type SymtabEntryKind = Value
  val STENTRY_QVARIABLE,
  STENTRY_BVARIABLE,
  STENTRY_ARG,
  STENTRY_USER_FUNCTION,
  STENTRY_THEORY_FUNCTION,
  STENTRY_SYNTH_FUNCTION,
  STENTRY_UNINTERP_FUNCTION,
  STENTRY_SORT = Value
}

class SymbolTableEntry (steSort: SortExp, fundefCmd: FunDefCmd, steKind: SymtabEntryKind.SymtabEntryKind, letBoundTerm: Term){
  def apply(steSort: SortExp, fundefCmd: FunDefCmd, steKind: SymtabEntryKind, letBoundTerm: Term): SymbolTableEntry = {
    if(fundefCmd == null && letBoundTerm == null) {
      if (steKind == SymtabEntryKind.STENTRY_USER_FUNCTION || steKind == SymtabEntryKind.STENTRY_BVARIABLE)
        throw new RuntimeException("INTERNAL: Wrong constructor called on SymbolTableEntry")
    }
    return this
  }

  def getSort() = steSort
  def getKind() = steKind

  def this(steKind: SymtabEntryKind.SymtabEntryKind, steSort: SortExp){
    this(steSort, null, steKind, null)
  }

  def this(letBoundTerm: Term, steSort: SortExp){
    this(steSort, null, SymtabEntryKind.STENTRY_BVARIABLE, letBoundTerm)
  }

  def this(fundef: FunDefCmd) {
    //this()
    this(new FunSortExp(fundef.args.args.foldLeft(List[SortExp]()){(res, asPair) => res:+asPair.sort.myClone().asInstanceOf[SortExp]}, fundef.sort.myClone().asInstanceOf[SortExp]), fundef, SymtabEntryKind.STENTRY_USER_FUNCTION, null)
  }

  def myClone(): SymbolTableEntry = {
    steKind match {
      case SymtabEntryKind.STENTRY_USER_FUNCTION => return new SymbolTableEntry(fundefCmd.myClone().asInstanceOf[FunDefCmd])
      case SymtabEntryKind.STENTRY_BVARIABLE => return new SymbolTableEntry(letBoundTerm.myClone().asInstanceOf[Term], steSort.myClone().asInstanceOf[SortExp])
      case _ => return new SymbolTableEntry(steKind, steSort.myClone().asInstanceOf[SortExp])
      //  case SymtabEntryKind.STENTRY_UNINTERP_FUNCTION => return new SymbolTableEntry(steKind, steSort.myClone())
    }
  }
}

class SymbolTableScope {
  private val bindings = new mutable.HashMap[String, SymbolTableEntry]()
  def lookup(identifier: String): SymbolTableEntry = {
    bindings.get(identifier).getOrElse(null)
  }

  def bind(identifier: String, symbolTableEntry: SymbolTableEntry) ={
    bindings += (identifier -> symbolTableEntry)
  }

  def checkedBind(identifier: String, symbolTableEntry: SymbolTableEntry) = {
    if(lookup(identifier) != null){
      throw new RuntimeException("Error: Redeclaration of identifier \"" +
        identifier + "\"")
    }

    bind(identifier, symbolTableEntry)
  }

  def getBindings() = bindings

  def myClone(): SymbolTableScope = {
    val ret = new SymbolTableScope()
    bindings.foldLeft(ret.getBindings()){
      (res, binding) =>{
        res += (binding._1 -> binding._2.myClone())
        res
      }
    }
    if(bindings.size > 0)
      assert(ret.getBindings().size > 0)
    return ret
  }
}

class SymbolTable{
  val scopes = new MyStack[SymbolTableScope]()

  private def init() = {
    scopes.push(new SymbolTableScope())
  }

  init()

  def push() = {
    scopes.push(new SymbolTableScope())
  }

  def push(wholeScope: SymbolTableScope) = {
    scopes.push(wholeScope)
  }

  def pop(): SymbolTableScope = {
    if (scopes.size == 1) {
      throw new RuntimeException("Error: SymbolTable::Pop(), tried to pop too many scopes")
    }
    scopes.pop()
  }

  def mangleName(name: String, argSorts: List[SortExp]): String ={
    return argSorts.foldLeft(name){
      (res, argSort) => {
        val actualSort = resolveSort(argSort)
        res + "_@_" + actualSort.toMangleString()
      }
    }
  }

  def resolveSort(sort: SortExp): SortExp = {
    if(sort == null) //return null
      throw new RuntimeException("Internal: SymbolTable::ResolveSort() was called with a NULL argument")
    if(!sort.isInstanceOf[NamedSortExp])
      return sort
    //TODO: this is a trick for now. Later we need to see why the lexer does not recognize the token
    //Bachle: to fix
    if(sort.asInstanceOf[NamedSortExp].name == "Bool")
      return BoolSortExp()
    if(sort.asInstanceOf[NamedSortExp].name == "Bool")
      return IntSortExp()
    val namedEntry = lookupSort(sort.asInstanceOf[NamedSortExp].name)
    //if(namedEntry == null)
    //  println("debug")
    resolveSort(namedEntry.getSort())
  }

  def lookup(identifier: String): SymbolTableEntry = {
    var index = scopes.size - 1
    while (index>=0){
      val ret = scopes(index).lookup(identifier)
      if(ret != null)
        return ret

      index -= 1
    }
    return null
  }

  def lookupSort(sortName: String): SymbolTableEntry = {
    val retVal = lookup(SymbolTableCommon.mangleSortName(sortName))
    if(retVal != null && retVal.getKind() == SymtabEntryKind.STENTRY_SORT) {
      return retVal
    }
    return null
  }

  def lookupSortRecursive(sortName: String): SymbolTableEntry = {
    val retVal = lookupSort(sortName)
    if (retVal != null && retVal.getKind() == SymtabEntryKind.STENTRY_SORT &&
      retVal.getSort().isInstanceOf[NamedSortExp]) {
          val namedSort = retVal.getSort().asInstanceOf[NamedSortExp]
      return lookupSortRecursive(namedSort.name)
    } else {
      return retVal
    }
  }

  def lookupVariable(varName: String): SymbolTableEntry = {
    val retVal = lookup(varName)
    if(retVal != null && (retVal.getKind() == SymtabEntryKind.STENTRY_QVARIABLE ||
      retVal.getKind() == SymtabEntryKind.STENTRY_BVARIABLE ||
      retVal.getKind() == SymtabEntryKind.STENTRY_ARG)) {
      return retVal
    }
    return null
  }

  def lookupFun(funName: String, argSorts: List[SortExp]): SymbolTableEntry = {
    val mangledName = mangleName(funName, argSorts)
    val retVal = lookup(mangledName)
    if(retVal != null && (retVal.getKind() == SymtabEntryKind.STENTRY_USER_FUNCTION ||
      retVal.getKind() == SymtabEntryKind.STENTRY_SYNTH_FUNCTION ||
      retVal.getKind() == SymtabEntryKind.STENTRY_THEORY_FUNCTION ||
      retVal.getKind() == SymtabEntryKind.STENTRY_UNINTERP_FUNCTION)) {
      return retVal
    }
    return null
  }

  def bindSort(name: String, sort: SortExp) = {
    scopes(0).checkedBind(SymbolTableCommon.mangleSortName(name),
      new SymbolTableEntry(SymtabEntryKind.STENTRY_SORT, sort))
  }

  def bindVariable(name: String, sort: SortExp) = {
    scopes.top.checkedBind(name, new SymbolTableEntry(SymtabEntryKind.STENTRY_QVARIABLE, sort))
  }

  def bindUserFun(fundef: FunDefCmd) = {
    // get the arg sorts
    val argSorts = fundef.args.args.foldLeft(List[SortExp]()) {
      (res, asPair) =>{
        res :+ asPair.sort
      }
    }
    val mangledName = mangleName(fundef.symbol, argSorts)
    scopes.top.checkedBind(mangledName, new SymbolTableEntry(fundef))
  }

  private def cloneVector(argList: List[SortExp]) ={
    argList.foldLeft(List[SortExp]()){(res, arg) => res :+ arg.myClone().asInstanceOf[SortExp]}
  }

  def bindSynthFun(name: String, argSorts: List[SortExp], retSort: SortExp) = {
    val funSort = new FunSortExp(cloneVector(argSorts), retSort.myClone().asInstanceOf[SortExp])
    val mangledName = mangleName(name, argSorts)
    scopes.top.checkedBind(mangledName,
      new SymbolTableEntry(SymtabEntryKind.STENTRY_SYNTH_FUNCTION, funSort))
  }

  def bindFormal(name: String, sort: SortExp) = {
    scopes.top.checkedBind(name, new SymbolTableEntry(SymtabEntryKind.STENTRY_ARG, sort))
  }

  def bindTheoryFun(name: String, argSorts: List[SortExp], retSort: SortExp) = {
    val funSort = new FunSortExp(cloneVector(argSorts),retSort.myClone().asInstanceOf[SortExp])
    val mangledName = mangleName(name, argSorts)

    scopes(0).checkedBind(mangledName, new SymbolTableEntry(SymtabEntryKind.STENTRY_THEORY_FUNCTION, funSort))
  }

  def checkDefinedTheoryFun(name: String, argSorts: List[SortExp], retSort: SortExp) ={
    val funSort = new FunSortExp(cloneVector(argSorts),retSort.myClone().asInstanceOf[SortExp])
    val mangledName = mangleName(name, argSorts)
    if(scopes(0).lookup(mangledName) == null)
      throw new RuntimeException("Error: Not defined theory funtion: "+funSort)
  }

  //TODO: implement these functions later
  def bindLetVariable(name: String, sort: SortExp) = {null}
  def bindUninterpretedFun(name: String, argSorts: List[SortExp], retSort: SortExp) = {null}
}

object SymbolTableCommon{
  def mangleSortName(name: String): String = return name + "_@S"
}