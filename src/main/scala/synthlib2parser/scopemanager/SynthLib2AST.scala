package main.scala.synthlib2parser.scopemanager

import main.scala.synthlib2parser.scopemanager.GTermKind.GTermKind

/**
  * Created by dxble on 7/8/16.
  */

trait Positional {

  /** The source position of this object, initially set to undefined. */
  var pos: Position = NoPosition

  /** If current source position is undefined, update it with given position `newpos`
    *  @return  the object itself
    */
  def setPos(newpos: Position): this.type = {
    if (pos eq NoPosition) pos = newpos
    this
  }
}
trait Position {

  /** The line number referred to by the position; line numbers start at 1. */
  def line: Int

  /** The column number referred to by the position; column numbers start at 1. */
  def column: Int

  /** The contents of the line at this position. (must not contain a new-line character).
    */
  protected def lineContents: String

  /** Returns a string representation of the `Position`, of the form `line.column`. */
  override def toString = ""+line+"."+column

  /** Returns a more ``visual'' representation of this position.
    *  More precisely, the resulting string consists of two lines:
    *   1. the line in the document referred to by this position
    *   2. a caret indicating the column
    *
    *  Example:
    *  {{{
    *    List(this, is, a, line, from, the, document)
    *                 ^
    *  }}}
    */
  def longString = lineContents+"\n"+lineContents.take(column-1).map{x => if (x == '\t') x else ' ' } + "^"

  /** Compare this position to another, by first comparing their line numbers,
    * and then -- if necessary -- using the columns to break a tie.
    *
    * @param `that` a `Position` to compare to this `Position`
    * @return true if this position's line number or (in case of equal line numbers)
    *         column is smaller than the corresponding components of `that`
    */
  def <(that: Position) = {
    this.line < that.line ||
      this.line == that.line && this.column < that.column
  }
}
object NoPosition extends Position {
  def line = 0
  def column = 0
  override def toString = "<undefined position>"
  override def longString = toString
  def lineContents = ""
}

case class LineColPosition(l: Int, c: Int, s: String) extends Position {
  def line = l
  def column = c

  override def toString = s
  override def longString = toString
  def lineContents = s
}

object ASTCmdKind extends Enumeration {
  type ASTCmdKind = Value
  val CMD_FUNDEF,
  CMD_SYNTHFUN,
  CMD_FUNDECL,
  CMD_SORTDEF,
  CMD_SETOPTS,
  CMD_VARDECL,
  CMD_CONSTRAINT,
  CMD_ORIGEXPR,
  CMD_SETLOGIC,
  CMD_CHECKSYNTH = Value
}

object SortKind extends Enumeration {
  type SortKind = Value
  val SORTKIND_INT,
    SORTKIND_BV,
    SORTKIND_ARRAY,
    SORTKIND_REAL,
    SORTKIND_BOOL,
    SORTKIND_FUN,
    SORTKIND_ENUM,
    SORTKIND_NAMED = Value
}

object GTermKind extends Enumeration {
  type GTermKind = Value
  val FUNGTERM_KIND,
  LITERALGTERM_KIND,
  SYMBOLGTERM_KIND = Value
}

sealed trait ASTBase extends Positional{
  def accept(visitor: ASTVisitorBase)
  def myClone(): ASTBase
}

case class Program(allCmds: List[ASTCmd]) extends ASTBase{
  def accept(visitor: ASTVisitorBase) = {
    visitor.visitProgram(this)
  }

  override def myClone(): ASTBase = {
    new Program(allCmds.foldLeft(List[ASTCmd]()){(res, cmd) => res :+ cmd.myClone().asInstanceOf[ASTCmd]})
  }
}
case class ArgSortPair(name: String, sort: SortExp) extends ASTBase{
  override def myClone() = new ArgSortPair(name, sort.myClone().asInstanceOf[SortExp])

  def accept(visitor: ASTVisitorBase) = {
    visitor.visitArgSortPair(this)
  }
}

case class ArgList(args: List[ArgSortPair]) extends ASTBase{
  override def myClone(): ArgList = new ArgList(args.foldLeft(List[ArgSortPair]()){(res, asPair) => res :+ asPair.myClone()})

  def accept(visitor: ASTVisitorBase) = {
    args.foreach(arg => arg.accept(visitor))
  }
}

case class Literal(litStr: String, sort: SortExp) extends ASTBase{
  def accept(visitor: ASTVisitorBase) = {
    visitor.visitLiteral(this)
  }

  override def myClone(): ASTBase = new Literal(litStr, sort.myClone().asInstanceOf[SortExp])
}
case class NTDef(symbol: String, sort: SortExp, expansions: List[GTerm]) extends ASTBase{
  def accept(visitor: ASTVisitorBase) = {
    visitor.visitNTDef(this)
  }

  override def myClone(): ASTBase = new NTDef(symbol, sort.myClone().asInstanceOf[SortExp], expansions.foldLeft(List[GTerm]()){(res, exp) => res :+ exp.myClone().asInstanceOf[GTerm]})
}

abstract class Term() extends ASTBase{
  def getTermSort(symTab: SymbolTable): SortExp
}
case class FunTerm(funName: String, args: List[Term]) extends Term{
  override def accept(visitor: ASTVisitorBase) = {
    visitor.visitFunTerm(this)
  }

  override def myClone(): Term = new FunTerm(funName, args.foldLeft(List[Term]()){(res, arg) => res :+ arg.myClone().asInstanceOf[Term]})

  override def getTermSort(symTab: SymbolTable): SortExp = {
    // determine this function's sort from the symbol table
    val argSorts = args.foldLeft(List[SortExp]()){
      (res, arg) => {
        res :+ arg.getTermSort(symTab)
      }
    }
    val entry = symTab.lookupFun(funName, argSorts);
    if(entry == null) {
      throw new RuntimeException("Could not determine type of term: " +this + "This could be due to an undeclared function or mismatched arguments to function")
    }

    val funSort = entry.getSort().asInstanceOf[FunSortExp]
    if(funSort == null) {
      throw new RuntimeException("Identifier \"" + funName + "\" does " +
        "not refer to an function, but used as one")
    }
    return funSort.retSort
  }
}

case class LiteralTerm(litTerm: Literal) extends Term{
  override def accept(visitor: ASTVisitorBase) = {
    visitor.visitLiteralTerm(this)
  }
  override def myClone(): Term = new LiteralTerm(litTerm)

  override def getTermSort(symTab: SymbolTable): SortExp = litTerm.sort
}
case class SymbolTerm(symbolStr: String) extends Term{
  override def accept(visitor: ASTVisitorBase) = {
    visitor.visitSymbolTerm(this)
  }
  override def myClone(): Term = new SymbolTerm(symbolStr)

  override def getTermSort(symTab: SymbolTable): SortExp = {
    val entry = symTab.lookup(symbolStr)
    if(entry == null) {
      throw new RuntimeException("Could not resolve identifier \"" + symbolStr + "\"");
    }

    val symSort = entry.getSort()
    if(entry.getKind() == SymtabEntryKind.STENTRY_USER_FUNCTION ||
      entry.getKind() == SymtabEntryKind.STENTRY_SYNTH_FUNCTION ||
      entry.getKind() == SymtabEntryKind.STENTRY_THEORY_FUNCTION ||
      entry.getKind() == SymtabEntryKind.STENTRY_UNINTERP_FUNCTION) {
      return symSort.asInstanceOf[FunSortExp]
    } else {
      return symSort
    }
  }
}

abstract class GTerm(kindStr: String) extends ASTBase{
  def getTermSort(symTab: SymbolTable): SortExp
  def getGTermKind(): GTermKind
  def getGTermName(): String
}
case class SymbolGTerm(symbol: String) extends GTerm("SymbolGTerm"){
  override def accept(visitor: ASTVisitorBase) = {
    visitor.visitSymbolGTerm(this)
  }

  override def getGTermName() = symbol
  override def myClone(): GTerm = new SymbolGTerm(symbol)
  override def getTermSort(symTab: SymbolTable): SortExp = {
    var entry = symTab.lookup(symbol)
    if(entry == null) {
      entry = symTab.lookup(SymbolTableCommon.mangleSortName(symbol))
      if(entry == null)
        throw new RuntimeException("Could not resolve identifier \"" + symbol + "\"");
    }

    val symSort = entry.getSort()
    if(entry.getKind() == SymtabEntryKind.STENTRY_USER_FUNCTION ||
      entry.getKind() == SymtabEntryKind.STENTRY_SYNTH_FUNCTION ||
      entry.getKind() == SymtabEntryKind.STENTRY_THEORY_FUNCTION ||
      entry.getKind() == SymtabEntryKind.STENTRY_UNINTERP_FUNCTION) {
      return symSort.asInstanceOf[FunSortExp]
    } else {
      return symSort
    }
  }

  override def getGTermKind(): GTermKind = GTermKind.SYMBOLGTERM_KIND
}
case class LiteralGTerm(litGTerm: Literal) extends GTerm("LiteralGTerm"){
  override def accept(visitor: ASTVisitorBase) = {
    visitor.visitLiteralGTerm(this)
  }
  override def getGTermName() = litGTerm.litStr
  override def myClone(): GTerm = new LiteralGTerm(litGTerm)
  override def getTermSort(symTab: SymbolTable): SortExp = litGTerm.sort

  override def getGTermKind(): GTermKind = GTermKind.LITERALGTERM_KIND
}
case class FunGTerm(funName: String, args: List[GTerm]) extends GTerm("FunGTerm"){
  override def accept(visitor: ASTVisitorBase) = {
    visitor.visitFunGTerm(this)
  }
  override def getGTermName() = funName
  override def myClone(): GTerm = new FunGTerm(funName, args.foldLeft(List[GTerm]()){(res, arg) => res :+ arg.myClone().asInstanceOf[GTerm]})

  override def getTermSort(symTab: SymbolTable): SortExp = {
    // determine this function's sort from the symbol table
    val argSorts = args.foldLeft(List[SortExp]()){
      (res, arg) => {
        res :+ arg.getTermSort(symTab)
      }
    }
    val entry = symTab.lookupFun(funName, argSorts)
    if(entry == null) {
      throw new RuntimeException("Could not determine type of term: " +this + "This could be due to an undeclared function or mismatched arguments to function")
    }

    val funSort = entry.getSort().asInstanceOf[FunSortExp]
    if(funSort == null) {
      throw new RuntimeException("Identifier \"" + funName + "\" does " +
        "not refer to an function, but used as one")
    }
    return funSort.retSort
  }

  override def getGTermKind(): GTermKind = GTermKind.FUNGTERM_KIND
}

abstract class ASTCmd extends ASTBase{
}

case class SetLogicCmd(logicName: String) extends ASTCmd(){
  def accept(visitor: ASTVisitorBase) = {
    visitor.visitSetLogicCmd(this)
  }

  override def myClone(): ASTBase = new SetLogicCmd(logicName)
}

case class FunDefCmd(symbol: String, args: ArgList, sort: SortExp, funDef: Term, var scope: SymbolTableScope) extends ASTCmd{
  def setScope(scopeTemp: SymbolTableScope) = scope = scopeTemp

  override def myClone(): ASTBase = new FunDefCmd(symbol, args.myClone(), sort.myClone().asInstanceOf[SortExp], funDef.myClone().asInstanceOf[Term], scope.myClone())

  def accept(visitor: ASTVisitorBase) = {
    visitor.visitFunDefCmd(this)
  }
}
case class SynthFunCmd(name: String, argList: ArgList, sort: SortExp, grammarRules: List[NTDef], var scope: SymbolTableScope) extends ASTCmd(){
  def setScope(scopeTemp: SymbolTableScope) = scope = scopeTemp

  def accept(visitor: ASTVisitorBase) = {
    visitor.visitSynthFunCmd(this)
  }

  override def myClone(): ASTBase = new SynthFunCmd(name, argList.myClone(), sort.myClone().asInstanceOf[SortExp], grammarRules.foldLeft(List[NTDef]()){ (res, grule) => res :+ grule.myClone().asInstanceOf[NTDef]}, scope.myClone())
}
case class CheckSynthCmd() extends ASTCmd(){
  def accept(visitor: ASTVisitorBase) = {
    visitor.visitCheckSynthCmd(this)
  }

  override def myClone(): ASTBase = new CheckSynthCmd()
}
case class ConstraintCmd(term: Term) extends ASTCmd{
  def accept(visitor: ASTVisitorBase) = {
    visitor.visitConstraintCmd(this)
  }

  override def myClone(): ASTBase = new ConstraintCmd(term.myClone().asInstanceOf[Term])
}
case class AssertCmd(term: Term) extends ASTCmd{
  def accept(visitor: ASTVisitorBase) = {
    visitor.visitAssertCmd(this)
  }

  override def myClone(): ASTBase = new AssertCmd(term.myClone().asInstanceOf[Term])
}
case class VarDeclCmd(name: String, sort: SortExp) extends ASTCmd{
  def accept(visitor: ASTVisitorBase) = {
    visitor.visitVarDeclCmd(this)
  }

  override def myClone(): ASTBase = new VarDeclCmd(name, sort.myClone().asInstanceOf[SortExp])
}
case class OriginalExpCmd(name: String, args: ArgList, sort: SortExp, constraint: Term,var scope: SymbolTableScope) extends ASTCmd{
  def setScope(scopeTemp: SymbolTableScope) = scope = scopeTemp

  def accept(visitor: ASTVisitorBase) = {
    visitor.visitOriginalExpCmd(this)
  }

  override def myClone(): ASTBase = new OriginalExpCmd(name, args.myClone(), sort.myClone().asInstanceOf[SortExp], constraint.myClone().asInstanceOf[Term], scope.myClone())
}

abstract class SortExp(typeStr: String) extends ASTCmd{
  def toMangleString(): String //= typeStr

}

case class IntSortExp() extends SortExp("Int"){
  override def accept(visitor: ASTVisitorBase) = {
    visitor.visitIntSortExp(this)
  }
  override def myClone(): SortExp = new IntSortExp()

  override def toMangleString(): String = "Int"
  override def equals(other: Any): Boolean = {
    if(other.isInstanceOf[IntSortExp])
      return true
    else
      return false
  }
  override def hashCode(): Int = {1}
}
case class BoolSortExp() extends SortExp("Bool"){
  override def accept(visitor: ASTVisitorBase) = {
    visitor.visitBoolSortExp(this)
  }
  override def myClone(): SortExp = new BoolSortExp()
  override def toMangleString(): String = "Bool"
  override def equals(other: Any): Boolean ={
    if(other.isInstanceOf[BoolSortExp])
      return true
    else
      return false
  }
  override def hashCode(): Int = {2}
}
case class NamedSortExp(name: String) extends SortExp("NamedSortExp"){
  override def accept(visitor: ASTVisitorBase) = {
    visitor.visitNamedSortExp(this)
  }
  override def myClone(): SortExp = new NamedSortExp(name)
  override def toMangleString() = throw new RuntimeException("Internal: Tried to call NamedSortExpr::ToMangleString()")
  override def equals(other: Any): Boolean ={
    if(!other.isInstanceOf[NamedSortExp])
      return false
    else
      return other.asInstanceOf[NamedSortExp].name == name
  }
  override def hashCode(): Int = {3}
}
case class FunSortExp(argSorts: List[SortExp], retSort: SortExp) extends SortExp("FunSortExp"){
  override def accept(visitor: ASTVisitorBase) = {
    visitor.visitFunSortExpr(this)
  }
  override def myClone(): SortExp = {
    val args = argSorts.foldLeft(List[SortExp]()){(res, arg) => res:+arg.myClone().asInstanceOf[SortExp]}
    new FunSortExp(args, retSort.myClone().asInstanceOf[SortExp])
  }
  override def toMangleString(): String ={
    val retVal = argSorts.foldLeft("") {
      (res, argSort) => {
        res + argSort.toMangleString() + "->"
      }
    }
    return retVal + retSort.toMangleString()
  }

  override def equals(other: Any): Boolean ={
    if(!other.isInstanceOf[FunSortExp])
      return false
    else{
      val castOther = other.asInstanceOf[FunSortExp]
      val retSortEqual = castOther.retSort.equals(castOther.retSort)
      val argSortsSizeEqual = castOther.argSorts.size == argSorts.size
      if(retSortEqual && argSortsSizeEqual){
        var index = 0
        while(index < argSorts.size){
          if(!castOther.argSorts(index).equals(argSorts(index)))
            return false
          index+=1
        }
        return true
      }else{
        return false
      }
    }
  }
  override def hashCode(): Int = {4}
}

abstract class ASTVisitorBase(name: String){
  def visitAssertCmd(cmd: AssertCmd): Unit = cmd.term.accept(this)

  def getName(): String = name
  def visitProgram(prog: Program) = {
    prog.allCmds.foreach(cmd => cmd.accept(this))
  }

  def visitFunDefCmd(cmd: FunDefCmd) = {
    cmd.args.accept(this)
    cmd.sort.accept(this)
    cmd.funDef.accept(this)
  }

  def visitSynthFunCmd(cmd: SynthFunCmd) = {
    cmd.argList.accept(this)
    cmd.sort.accept(this)
    cmd.grammarRules.foreach(r => r.accept(this))
  }
  def visitVarDeclCmd(cmd: VarDeclCmd) = {
    cmd.sort.accept(this)
  }

  def visitConstraintCmd(cmd: ConstraintCmd) = {
    cmd.term.accept(this)
  }

  def visitOriginalExpCmd(cmd: OriginalExpCmd) = {
    cmd.args.accept(this)
    cmd.sort.accept(this)
    cmd.constraint.accept(this)
  }

  def visitSetLogicCmd(cmd: SetLogicCmd) = {
    if(cmd.logicName != "LIA")
      throw new RuntimeException("Not Suported Logic")
  }

  def visitCheckSynthCmd(cmd: CheckSynthCmd) = {

  }

  def visitArgSortPair(cmd: ArgSortPair) = {
    cmd.sort.accept(this)
  }

  def visitIntSortExp(cmd: IntSortExp) ={}
  def visitBoolSortExp(cmd: BoolSortExp)={}
  def visitNamedSortExp(cmd: NamedSortExp) ={}

  def visitFunTerm(term: FunTerm) ={
    term.args.foreach(arg => arg.accept(this))
  }

  def visitLiteralTerm(term: LiteralTerm) = {term.litTerm.accept(this)}
  def visitSymbolTerm(term: SymbolTerm) = {}

  def visitFunGTerm(term: FunGTerm) = {
    term.args.foreach(arg => arg.accept(this))
  }

  def visitLiteralGTerm(term: LiteralGTerm) = {
    term.litGTerm.accept(this)
  }
  def visitSymbolGTerm(term: SymbolGTerm) = {}
  def visitNTDef(ntdef: NTDef) = {
    ntdef.sort.accept(this)
    ntdef.expansions.foreach(exp => exp.accept(this))
  }

  def visitFunSortExpr(funSort: FunSortExp) = {
    funSort.argSorts.foreach(arg => arg.accept(this))
    funSort.retSort.accept(this)
  }

  def visitLiteral(lit: Literal) = {}
}

