package main.scala.synthlib2parser.scopemanager

/**
  * Created by dxble on 7/11/16.
  */
class LogicSymbolLoader {
  private val registeredTypes: scala.collection.mutable.HashSet[SortExp] = new scala.collection.mutable.HashSet[SortExp]
  private val bvLoaded: Boolean = false

  def loadCore(symTab: SymbolTable): Unit ={
    val unOpVec = List[SortExp](new BoolSortExp())
    val binOpVec = List[SortExp](new BoolSortExp(), new BoolSortExp())
    val boolSort = new BoolSortExp()
    symTab.bindTheoryFun("and", binOpVec, boolSort)
    symTab.bindTheoryFun("or", binOpVec, boolSort)
    symTab.bindTheoryFun("not", unOpVec, boolSort)
    symTab.bindTheoryFun("=>", binOpVec, boolSort)
    symTab.bindTheoryFun("=", binOpVec, boolSort)
    //TODO: bind other theory funs here: not, xor, nxor, nand, nor, iff, ite
  }

  def loadLIA(symTab: SymbolTable): Unit ={
    val unOpVec = List[SortExp](new IntSortExp())
    val binOpVec = List[SortExp](new IntSortExp(), new IntSortExp())
    val intSort = new IntSortExp()
    val boolSort = new BoolSortExp()


    symTab.bindTheoryFun("+", binOpVec, intSort)
    symTab.bindTheoryFun("-", binOpVec, intSort)
    symTab.bindTheoryFun("*", binOpVec, intSort)
    symTab.bindTheoryFun("/", binOpVec, intSort)
    symTab.bindTheoryFun("div", binOpVec, intSort)
    symTab.bindTheoryFun("mod", binOpVec, intSort)
    symTab.bindTheoryFun("abs", unOpVec, intSort)
    symTab.bindTheoryFun("-", unOpVec, intSort)

    symTab.bindTheoryFun("<", binOpVec, boolSort)
    symTab.bindTheoryFun("<=", binOpVec, boolSort)
    symTab.bindTheoryFun(">", binOpVec, boolSort)
    symTab.bindTheoryFun(">=", binOpVec, boolSort)
    symTab.bindTheoryFun("=", binOpVec, boolSort)
  }

  def loadAll(symTab: SymbolTable)
  {
    val bs = new BoolSortExp()
    val is = new IntSortExp()
    //val rs = new RealSortExp()
    registeredTypes.add(bs)
    //registeredTypes.add(rs)
    registeredTypes.add(is)
    loadCore(symTab)
    loadLIA(symTab)
    //TODO: load others: e.g., BV, Real
  }

  def reset(): Unit ={
    registeredTypes.clear()
  }
}
