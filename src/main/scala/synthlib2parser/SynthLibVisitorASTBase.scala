package synthlib2parser

import generatedparser.SynthLibParser._
import generatedparser._
import main.scala.synthlib2parser.scopemanager.{SortExp, _}
import org.antlr.v4.runtime.ParserRuleContext
import scala.collection.JavaConverters.asScalaBuffer

/**
  * Created by dxble on 3/8/17.
  */
class SynthLibVisitorASTBase extends SynthLibBaseVisitor[ASTBase]{

  override def visitProgram(ctx: ProgramContext): ASTBase = {
    val cmd0 = visit(ctx.setLogicCmd()).asInstanceOf[ASTCmd]

    val cmds = scala.collection.JavaConverters.asScalaBuffer(ctx.cmd()).map(cmd => visit(cmd).asInstanceOf[ASTCmd])
    val node =  Program(cmd0 :: cmds.toList)
    node.setPos(getPos(ctx))
    node
  }

  private def getPos(ctx: ParserRuleContext): LineColPosition = {
    val line = ctx.getStart.getLine
    val col = ctx.getStart.getCharPositionInLine
    val text = ctx.getText

    return new LineColPosition(line, col, text)
  }

  override def visitSetLogicCmd(ctx: SetLogicCmdContext): ASTBase = {
    val node = new SetLogicCmd(ctx.Symbol().getSymbol.getText)
    node.setPos(getPos(ctx))
    node
  }

  override def visitIntSortExp(ctx: IntSortExpContext): ASTBase = {
    val node = IntSortExp()
    node.setPos(getPos(ctx))
    node
  }

  override def visitBoolSortExp(ctx: BoolSortExpContext): ASTBase = {
    val node = BoolSortExp()
    node.setPos(getPos(ctx))
    node
  }

  override def visitLiteralTerm(ctx: LiteralTermContext): ASTBase = {
    val lit = visit(ctx.literal()).asInstanceOf[Literal]
    val node = LiteralTerm(lit)
    node.setPos(getPos(ctx))
    node
  }

  override def visitLiteralGTerm(ctx: LiteralGTermContext): ASTBase = {
    val lit = visit(ctx.literal()).asInstanceOf[Literal]
    val node = LiteralGTerm(lit)
    node.setPos(getPos(ctx))
    node
  }

  override def visitIntConstExp(ctx: IntConstExpContext): ASTBase = {
    val text = ctx.IntConst().getSymbol.getText
    val node = Literal(text, IntSortExp())
    node.setPos(getPos(ctx))
    node
  }

  override def visitBoolConstExp(ctx: BoolConstExpContext): ASTBase = {
    val text = ctx.BoolConst().getSymbol.getText
    val node = Literal(text, BoolSortExp())
    node.setPos(getPos(ctx))
    node
  }

  override def visitSymbolTerm(ctx: SymbolTermContext): ASTBase = {
    val node = SymbolTerm(ctx.Symbol().getSymbol.getText)
    node.setPos(getPos(ctx))
    node
  }

  override def visitFterm(ctx: FtermContext): ASTBase = {
    val name = ctx.Symbol().getSymbol.getText
    val terms = asScalaBuffer(ctx.term()).foldLeft(List[Term]()){
      (res, t) => {
        res :+ visit(t).asInstanceOf[Term]
      }
    }
    val fterm = FunTerm(name, terms)
    fterm.setPos(getPos(ctx))
    fterm
  }

  override def visitSymbolGTerm(ctx: SymbolGTermContext): ASTBase = {
    val node = SymbolGTerm(ctx.Symbol().getSymbol.getText)
    node.setPos(getPos(ctx))
    node
  }

  override def visitFGTerm(ctx: FGTermContext): ASTBase = {
    val name = ctx.Symbol().getSymbol.getText
    val gterms = asScalaBuffer(ctx.gTerm()).foldLeft(List[GTerm]()){
      (res, t) => {
        res :+ visit(t).asInstanceOf[GTerm]
      }
    }
    val fgterm = FunGTerm(name, gterms)
    fgterm.setPos(getPos(ctx))
    fgterm
  }

  override def visitNTDef(ctx: NTDefContext): ASTBase = {
    val sym = ctx.Symbol().getSymbol.getText
    val sort = visit(ctx.sortExpr()).asInstanceOf[SortExp]
    val expans = asScalaBuffer(ctx.gTerm()).foldLeft(List[GTerm]()){
      (res, t) =>
        try {
           res :+ visit(t).asInstanceOf[GTerm]
        }catch {
          case e: Exception => {
            println("debug")
            res
          }
        }
    }
    val node = NTDef(sym, sort, expans)
    node.setPos(getPos(ctx))
    node
  }

  override def visitPairs(ctx: PairsContext): ASTBase = {
    var i = -1
    val pairs = asScalaBuffer(ctx.Symbol()).foldLeft(List[ArgSortPair]()){
      (res, symbol) => {
        i += 1
        res :+ ArgSortPair(symbol.getSymbol.getText, visit(ctx.sortExpr(i)).asInstanceOf[SortExp])
      }
    }
    val node = ArgList(pairs)
    node.setPos(getPos(ctx))
    node
  }

  override def visitSynthFunCmd(ctx: SynthFunCmdContext): ASTBase = {
    val fname = ctx.Symbol().getSymbol.getText
    val pairs = visit(ctx.pairs()).asInstanceOf[ArgList]
    val sort = visit(ctx.sortExpr()).asInstanceOf[SortExp]
    val rules = asScalaBuffer(ctx.nTDef()).foldLeft(List[NTDef]()){
      (res, n) => res :+ visit(n).asInstanceOf[NTDef]
    }
    val node = SynthFunCmd(fname, pairs, sort, rules, null)
    node.setPos(getPos(ctx))
    node
  }

  override def visitOrigCmd(ctx: OrigCmdContext): ASTBase = {
    val fname = ctx.Symbol().getSymbol.getText
    val pairs = visit(ctx.pairs()).asInstanceOf[ArgList]
    val sort = visit(ctx.sortExpr()).asInstanceOf[SortExp]
    val term = visit(ctx.term()).asInstanceOf[Term]
    val node = OriginalExpCmd(fname, pairs, sort, term, null)
    node.setPos(getPos(ctx))
    node
  }

  override def visitCheckSynthCmd(ctx: CheckSynthCmdContext): ASTBase = {
    val node = CheckSynthCmd()
    node.setPos(getPos(ctx))
    node
  }

  override def visitConstraintCmd(ctx: ConstraintCmdContext): ASTBase = {
    val term = visit(ctx.term()).asInstanceOf[Term]
    val node = ConstraintCmd(term)
    node.setPos(getPos(ctx))
    node
  }

  override def visitVarDeclCmd(ctx: VarDeclCmdContext): ASTBase = {
    val name = ctx.Symbol().getSymbol.getText
    val sort = visit(ctx.sortExpr()).asInstanceOf[SortExp]
    val node = VarDeclCmd(name, sort)
    node.setPos(getPos(ctx))
    node
  }
}
