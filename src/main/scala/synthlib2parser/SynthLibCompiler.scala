/**
  * Created by dxble on 3/8/17.
  */
package synthlib2parser

import java.io.IOException

import generatedparser.{SynthLibLexer, SynthLibParser}
import main.scala.synthlib2parser.scopemanager._
import org.antlr.v4.runtime.{ANTLRInputStream, CommonTokenStream}
import org.apache.log4j.{BasicConfigurator, Logger}

/**
  * Created by dxble on 3/7/17.
  */
class SynthLibCompiler {
  val logger = Logger.getLogger(this.getClass)
  val theSymbolTable = new SymbolTable

  def parse(file: String): ASTBase ={
    // create a CharStream that reads from standard input
    val inputString = myLib.Lib.readFile2String(file)
    val input = new ANTLRInputStream(inputString)
    //ANTLRInputStream input = new ANTLRInputStream("(set-logic  LIA)\n(check-synth)");
    // create a lexer that feeds off of input CharStream
    val lexer = new SynthLibLexer(input)
    // create a buffer of tokens pulled from the lexer
    val tokens = new CommonTokenStream(lexer)
    // create a parser that feeds off the tokens buffer
    val parser = new SynthLibParser(tokens)
    val tree = parser.program()
    // begin parsing at init rule
    val visitor = new SynthLibVisitorASTBase()
    val ast = visitor.visit(tree)
    logger.debug(tree.toStringTree(parser)) // print LISP-style tree
    val logicLoader = new LogicSymbolLoader
    logicLoader.loadAll(theSymbolTable)
    SymtabBuilder.doBuild(ast.asInstanceOf[Program], theSymbolTable)
    ast
    //System.out.println(tree.getText()); // print LISP-style tree
  }
}

object SynthLibCompiler{
  val logger = Logger.getLogger(this.getClass)
  @throws[IOException]
  def main(args: Array[String]): Unit = {
    val test = new SynthLibCompiler()
    //test.parse("/home/dxble/workspace/parsers2/src/myparser/test4.sygus")
    //BasicConfigurator.configure()
    logger.info("Parsing...")
    val start = System.currentTimeMillis()
    val ast = test.parse("/home/dxble/workspace/repairtools/angelix/src/af2sygus/patch.sygus")
    val end = System.currentTimeMillis()
    logger.info("Running time: "+ (end - start)/1000.0)
    logger.info("Parsed...")
    logger.debug(ast)
    logger.debug(test.theSymbolTable)
  }
}