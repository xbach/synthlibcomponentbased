package metadriver

import componentsynthesis.ComponentSynthesisDriver
import config.ConfigOptions.SolverName
import config.{ConfigOptions, ConfigurationPropertiesX}

/**
  * Created by dxble on 4/8/17.
  */
object MainDriver {
  def main(args: Array[String]): Unit = {
    //val angelicForestFile = args(0)
    //val extractedDir=args(1)
    //val outputFile=args(2)
    //val configFile=args(3)
    //val solver=args(3)
    //val solverPath=args(4)
    //val beautifierPath=args(5)
    //val additionalConfig = args(4)

    val configFile = args(0)

    ConfigurationPropertiesX.load(configFile)
    ConfigurationPropertiesX.print()

    assert(ConfigurationPropertiesX.properties != null)
    if(ConfigOptions.solverName.compare(SolverName.CompRank) == 0){
      ComponentSynthesisDriver.solve()
    } else {
      throw new RuntimeException("Not yet supported solver: "+ConfigOptions.solverName)
    }
  }
}
