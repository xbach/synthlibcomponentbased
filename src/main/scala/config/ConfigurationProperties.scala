package config

/**
  * Created by dxble on 9/11/16.
  */

import org.apache.log4j.Logger
import java.io.{File, FileInputStream, InputStream}
import java.util.Properties
case class ConfigurationException(message:String)  extends Exception(message)

object ConfigurationPropertiesX {
  var properties: Properties = null
  protected var log: Logger = Logger.getLogger(this.getClass)

  def load(configFile: String) = {
    var propFile: InputStream = null
    try {
      if(new File(configFile).exists()) {
        properties = new Properties
        propFile = new FileInputStream(configFile)
        properties.load(propFile)
      }
    }
    catch {
      case e: Exception => {
        e.printStackTrace
      }
    }
  }

  def clearConfig() = {
    properties.clear()
  }

  def hasProperty(key: String): Boolean = {
    if (properties.getProperty(key) == null) {
      return false
    }
    return true
  }

  def getProperty(key: String): String = {
    return properties.getProperty(key)
  }

  def getPropertyInt(key: String): Integer = {
    return Integer.valueOf(properties.getProperty(key))
  }

  def getPropertyBool(key: String): Boolean = {
    return properties.getProperty(key).toBoolean
  }

  def getPropertyDouble(key: String): Double = {
    return properties.getProperty(key).toDouble
  }

  def print() {
    log.info("----------------------------")
    log.info("---Configuration properties:---Execution values")
    import scala.collection.JavaConverters._
    properties.stringPropertyNames.asScala.foreach{ key =>
      log.info("p:" + key + "= " + properties.getProperty(key))
    }
    log.info("----------------------------")
  }
}
