package config

import componentsynthesis.satsolver.MaxSatAlgorithm
import org.apache.log4j.Logger

import scala.collection.mutable

/**
  * Created by dxble on 2/23/17.
  */
object ConfigOptions {
  val logger = Logger.getLogger(this.getClass)

  object ActualVariableType extends Enumeration{
    type ActualVarType = Value
    val BoolActualType = Value("bool")
    val IntActualType = Value("int")
    val CharActualType = Value("char")
    val DoubleActualType = Value("double")
    val FloatActualType = Value("float")
  }

  // TODO: add other solvers here
  object SolverName extends Enumeration{
    type Solver = Value
    val Enum = Value("Enum")
    val CompRank = Value("CompRank")
  }

  object SynthesisLevel extends Enumeration{
    type SynthLevel = Value
    val ALTERNATIVES = Value("alternatives")
    val VARIABLES = Value("variables")
    val BASIC_ARITHMETIC = Value("basic-arithmetic") //
    val BASIC_INEQUALITIES= Value("basic-inequalities")
    val BASIC_EQUALITIES= Value("basic-equalities")
    val BASIC_LOGIC = Value("basic-logic")
    val INTEGER_CONSTANTS = Value("integer-constants")
    val BOOLEAN_CONSTANTS = Value("boolean-constants")
  }

  lazy val actualVarTypeFile: String = {
    if(ConfigurationPropertiesX.properties == null)
      ""
    else {
      val prop = ConfigurationPropertiesX.properties.getProperty("actualVarTypeFile")
      if (prop == null)
        ""
      else prop
    }
  }

  lazy val synthesisLevels: Array[SynthesisLevel.SynthLevel] = {
    if(ConfigurationPropertiesX.properties == null)
      Array(SynthesisLevel.ALTERNATIVES, SynthesisLevel.BASIC_EQUALITIES, SynthesisLevel.BASIC_INEQUALITIES, SynthesisLevel.BASIC_ARITHMETIC, SynthesisLevel.BASIC_LOGIC)
    else {
      val prop = ConfigurationPropertiesX.properties.getProperty("synthesisLevels")
      if (prop == null)
        Array(SynthesisLevel.ALTERNATIVES, SynthesisLevel.BASIC_EQUALITIES, SynthesisLevel.BASIC_INEQUALITIES, SynthesisLevel.BASIC_ARITHMETIC, SynthesisLevel.BASIC_LOGIC)
      else prop.split(";").map(s => SynthesisLevel.withName(s.trim))
    }
  }

  lazy val syntaxFeature: Boolean = {
    if(ConfigurationPropertiesX.properties == null)
      true
    else {
      val prop = ConfigurationPropertiesX.properties.getProperty("feature.syntax")
      if (prop == null)
        true
      else prop.toBoolean
    }
  }

  lazy val semanticFeature: Boolean = {
    if(ConfigurationPropertiesX.properties == null)
      true
    else {
      val prop = ConfigurationPropertiesX.properties.getProperty("feature.semantic")
      if (prop == null)
        true
      else prop.toBoolean
    }
  }

  lazy val solverName: SolverName.Solver = {
    val prop = ConfigurationPropertiesX.properties.getProperty("solver.name")
    if (prop == null) {
      val default = SolverName.CompRank
      logger.info("Not specified solver name! Using default solver: "+default)
      default
    }
    else SolverName.withName(prop)
  }

  lazy val sygusFile: String = {
    val prop = ConfigurationPropertiesX.properties.getProperty("solver.sygusFile")
    if (prop == null){
      val default = "synthesized.patch.sygus"
      logger.info("Not specified sygus file name to write out! Using default name: "+default)
      default
    }
    else prop
  }

  lazy val sizeBound: Int = {
    val prop = ConfigurationPropertiesX.properties.getProperty("solver.sizeBound")
    if (prop == null){
      logger.info("Not specified size bound! Using default size bound: 3")
      3
    }
    else prop.toInt
  }

  lazy val variableScore: mutable.HashMap[String, mutable.HashMap[String, Int]] = {
    val prop = ConfigurationPropertiesX.properties.getProperty("solver.variableScore")
    if (prop == null){
      new mutable.HashMap[String, mutable.HashMap[String, Int]]()
    }
    else {
      val res = new mutable.HashMap[String, mutable.HashMap[String, Int]]()
      scala.io.Source.fromFile(prop).getLines().foreach(line => {
        if(!line.startsWith("#")) {
          val sp = line.split(" ")
          val funcName = sp(0)
          val variable = sp(1)
          val score = sp(2)
          val variableWScore = res.getOrElse(funcName, new mutable.HashMap[String, Int]())
          variableWScore += variable -> score.toInt
          res.update(funcName, variableWScore)
        }
      })
      res
    }
  }

  lazy val optStrategy: String = {
    val prop = ConfigurationPropertiesX.properties.getProperty("solver.opt.strategy")
    if (prop == null) {
      val default = "pareto"
      logger.info("Not specified optimization strategy! Using default strategy: "+default)
      default
    }
    else prop
  }

  lazy val maxSatAlgorithm: MaxSatAlgorithm.Algorithm = {
    val prop = ConfigurationPropertiesX.properties.getProperty("solver.satSolver")
    if (prop == null) {
      val default = MaxSatAlgorithm.STANDARD_WEIGHTED
      logger.info("Not specified optimization strategy! Using default strategy: "+default)
      default
    }
    else MaxSatAlgorithm.withName(prop)
  }

  lazy val maxExamples: Int = {
    val prop = ConfigurationPropertiesX.properties.getProperty("solver.maxExamples")
    if (prop == null) {
      15
    } else prop.toInt
  }
}
