package common.astutils

import com.microsoft.z3.{ArithExpr, BoolExpr, Context, Expr}
import common.coreast.{IntVar, _}

/**
  * Created by dxble on 4/3/17.
  */
class CAST2Z3Java(z3: Context) extends CASTVisitorBase{
  override def visitFunctionSignature(functionDesign: FunctionSignature): Z3ASTJavaRetType ={
    null
  }
  override def visitGreater(greater: Greater): Z3ASTJavaRetType = {
    val left = greater.opr1.accept(this)
    val right = greater.opr2.accept(this)
    val ast = z3.mkGt(left.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr], right.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr])
    new Z3ASTJavaRetType(ast)
  }

  override def visitLessThan(lessThan: LessThan): Z3ASTJavaRetType ={
    val left = lessThan.opr1.accept(this)
    val right = lessThan.opr2.accept(this)
    val ast = z3.mkLt(left.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr], right.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr])
    new Z3ASTJavaRetType(ast)
  }

  override def visitLTE(lTE: LTE): Z3ASTJavaRetType ={
    val left = lTE.opr1.accept(this)
    val right = lTE.opr2.accept(this)
    val ast = z3.mkLe(left.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr], right.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr])
    new Z3ASTJavaRetType(ast)
  }

  override def visitGTE(gTE: GTE): Z3ASTJavaRetType ={
    val left = gTE.opr1.accept(this)
    val right = gTE.opr2.accept(this)
    val ast = z3.mkGe(left.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr], right.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr])
    new Z3ASTJavaRetType(ast)
  }

  override def visitEqual(equal: Equal): Z3ASTJavaRetType={
    val left = equal.opr1.accept(this)
    val right = equal.opr2.accept(this)
    val ast = z3.mkEq(left.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[Expr], right.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[Expr])
    new Z3ASTJavaRetType(ast)
  }

  override def visitAnd(and: And): Z3ASTJavaRetType={
    val left = and.opr1.accept(this)
    val right = and.opr2.accept(this)
    val ast = z3.mkAnd(left.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[BoolExpr], right.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[BoolExpr])
    new Z3ASTJavaRetType(ast)
  }

  override def visitOr(or: Or): Z3ASTJavaRetType={
    val left = or.opr1.accept(this)
    val right = or.opr2.accept(this)
    val ast = z3.mkOr(left.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[BoolExpr], right.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[BoolExpr])
    new Z3ASTJavaRetType(ast)
  }

  override def visitNot(not: Not): Z3ASTJavaRetType={
    val op = not.opr.accept(this)
    val ast = z3.mkNot(op.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[BoolExpr])
    new Z3ASTJavaRetType(ast)
  }

  override def visitImplies(implies: Implies): Z3ASTJavaRetType={
    val left = implies.opr1.accept(this)
    val right = implies.opr2.accept(this)
    val ast = z3.mkImplies(left.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[BoolExpr], right.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[BoolExpr])
    new Z3ASTJavaRetType(ast)
  }

  override def visitBoolVar(boolVar: BoolVar): Z3ASTJavaRetType ={
    val bvar = z3.mkBoolConst(boolVar.name)
    new Z3ASTJavaRetType(bvar)
  }

  override def visitBoolConst(boolConst: BoolConst): Z3ASTJavaRetType={
    val bconst = if(boolConst.value.toBoolean) z3.mkTrue() else z3.mkFalse()
    new Z3ASTJavaRetType(bconst)
  }

  override def visitPlus(plus: Plus): Z3ASTJavaRetType={
    val left = plus.opr1.accept(this)
    val right = plus.opr2.accept(this)
    val ast = z3.mkAdd(left.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr], right.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr])
    new Z3ASTJavaRetType(ast)
  }

  override def visitMinus(minus: Minus): Z3ASTJavaRetType={
    val left = minus.opr1.accept(this)
    val right = minus.opr2.accept(this)
    val ast = z3.mkSub(left.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr], right.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr])
    new Z3ASTJavaRetType(ast)
  }

  override def visitDiv(div: Div): Z3ASTJavaRetType={
    val left = div.opr1.accept(this)
    val right = div.opr2.accept(this)
    val ast = z3.mkDiv(left.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr], right.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr])
    new Z3ASTJavaRetType(ast)
  }

  override def visitMult(mult: Mult): Z3ASTJavaRetType={
    val left = mult.opr1.accept(this)
    val right = mult.opr2.accept(this)
    val ast = z3.mkMul(left.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr], right.asInstanceOf[Z3ASTJavaRetType].getAST().asInstanceOf[ArithExpr])
    new Z3ASTJavaRetType(ast)
  }

  override def visitIntConst(intConst: IntConst): Z3ASTJavaRetType={
    val iconst = z3.mkInt(intConst.value.toInt)
    new Z3ASTJavaRetType(iconst)
  }

  override def visitIntVar(intVar: IntVar): Z3ASTJavaRetType = {
    val ivar = z3.mkIntConst(intVar.name)
    new Z3ASTJavaRetType(ivar)
  }
}
