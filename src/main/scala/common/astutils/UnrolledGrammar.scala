package common.astutils

import common.coreast._
import main.scala.synthlib2parser.scopemanager._
import org.apache.log4j.Logger

import scala.collection.mutable

/**
  * Created by dxble on 7/15/16.
  */
/**
  * Example scenario: given X, e.g., variable name, find appropriate alternatives that type and scope checked
  * Solution: X -> its type -> (variable, type) -> scope -> available alternatives
  * This should be called on each synth-fun
  */
class UnrolledGrammar extends ASTVisitorBase("UnrolledGrammar"){
  val logger = Logger.getLogger(this.getClass)

  //Scope Name -> expansion rules. e.g., scope Name is Start, followed by its rules
  val scopeRules = new mutable.HashMap[String, List[GTerm]]()
  //Type -> [Scope Name]
  val type2Scopes = new mutable.HashMap[SortExp, List[String]]()

  val scope2Type = new mutable.HashMap[String, SortExp]()

  val foundScopesOfExp = new mutable.HashMap[CAST, ScopeList]()

  override def visitNTDef(nTDef: NTDef) ={
    scopeRules += (nTDef.symbol -> nTDef.expansions)
    scope2Type += (nTDef.symbol -> nTDef.sort)
    val scopes = type2Scopes.get(nTDef.sort).getOrElse(null)
    if(scopes == null){
      type2Scopes += (nTDef.sort -> List[String](nTDef.symbol))
    }else{
      val addedScopes = scopes :+ nTDef.symbol
      type2Scopes.update(nTDef.sort, addedScopes)
    }
  }

  def typeOfExp(exp: CAST): SortExp ={
    if(exp.isInstanceOf[BoolOperatorsCAST])
      BoolSortExp()
    else if (exp.isInstanceOf[ArithOperatorsCAST])
      IntSortExp()
    else {
      throw new RuntimeException("Not yet handled type other than Bool and Int!")
    }
  }

  def findScopesOfExp(exp: CAST): ScopeList ={
    //val expSort = typeOfExp(exp)
    //val scopes = type2Scopes.get(expSort).getOrElse(throw new RuntimeException("Not found scope of type: "+expSort))
    //exp.accept(this)
    val scopes = foundScopesOfExp.getOrElse(exp, null)
    if(scopes != null)
      return scopes
    else {
      val scopeFinder = new ExpScopesCollector(this)
      val expScopes = exp.accept(scopeFinder).asInstanceOf[ScopeList]
      logger.debug(exp + " " + expScopes)
      if (expScopes.scopesList.size == 0)
        throw new RuntimeException("Scope of: " + exp + " cannot be found! This could be because of unsupported theory function \"" + exp.topLevelString() + "\" in this exp!")
      expScopes
    }
  }
}

class ExpScopesCollector(unrolledGrammar: UnrolledGrammar) extends CASTVisitorBase{
  val scopesOfExp = new mutable.HashSet[String]()
  override def visitFunctionSignature(functionDesign: FunctionSignature): VisitorReturnType = EmptyType()

  private def scopeOfBinFun(scopesLeft: Scope, scopesRight: Scope, funName: String, funSort: SortExp) = {
    val scopeFun = scopeLookupFun(funName, funSort)
    scopeFun.foldLeft(List[ScopeName]()){
      (res, eachScope) =>{
        val (funScope, argScopes) = eachScope// binary has two args
        val scopePair = (argScopes(0),(argScopes(1)))
        val xCross = scopesLeft.asInstanceOf[Scope].cross(scopesRight.asInstanceOf[Scope])
        if(xCross.asInstanceOf[ScopeCrossProduct].scopes.contains(scopePair)){
          res :+ ScopeName(funScope)
        }else{
          res
        }
      }
    }
  }

  private def scopeOfUnFun(scope: Scope, funName: String, funSort: SortExp) ={
    val scopeFun = scopeLookupFun(funName, funSort)
    scopeFun.foldLeft(List[ScopeName]()){
      (res, eachScope) =>{
        val (funScope, argScopes) = eachScope// unary has one arg
        if(scope.asInstanceOf[ScopeList].scopesList.contains(argScopes(0))){
          res :+ ScopeName(funScope)
        }else{
          res
        }
      }
    }
  }

  override def visitGreater(greater: Greater): VisitorReturnType = {
    val scopesLeft = greater.opr1.accept(this)
    val scopesRight = greater.opr2.accept(this)
    val scopeList = scopeOfBinFun(scopesLeft.asInstanceOf[Scope], scopesRight.asInstanceOf[Scope], ">", BoolSortExp())
    ScopeList(scopeList)
  }

  override def visitLessThan(lessThan: LessThan): VisitorReturnType ={
    val scopesLeft = lessThan.opr1.accept(this)
    val scopesRight = lessThan.opr2.accept(this)
    val scopeList = scopeOfBinFun(scopesLeft.asInstanceOf[Scope], scopesRight.asInstanceOf[Scope], "<", BoolSortExp())
    ScopeList(scopeList)
  }

  override def visitLTE(lTE: LTE): VisitorReturnType ={
    val scopesLeft = lTE.opr1.accept(this)
    val scopesRight = lTE.opr2.accept(this)
    val scopeList = scopeOfBinFun(scopesLeft.asInstanceOf[Scope], scopesRight.asInstanceOf[Scope], "<=", BoolSortExp())
    ScopeList(scopeList)
  }

  override def visitGTE(gTE: GTE): VisitorReturnType ={
    val scopesLeft = gTE.opr1.accept(this)
    val scopesRight = gTE.opr2.accept(this)
    val scopeList = scopeOfBinFun(scopesLeft.asInstanceOf[Scope], scopesRight.asInstanceOf[Scope], ">=", BoolSortExp())
    ScopeList(scopeList)
  }

  override def visitEqual(equal: Equal): VisitorReturnType={
    val scopesLeft =  equal.opr1.accept(this)
    val scopesRight = equal.opr2.accept(this)
    val scopeList = scopeOfBinFun(scopesLeft.asInstanceOf[Scope], scopesRight.asInstanceOf[Scope], "=", BoolSortExp())
    ScopeList(scopeList)
  }

  override def visitAnd(and: And): VisitorReturnType={
    val scopesLeft = and.opr1.accept(this)
    val scopesRight = and.opr2.accept(this)
    val scopeList = scopeOfBinFun(scopesLeft.asInstanceOf[Scope], scopesRight.asInstanceOf[Scope], "and", BoolSortExp())
    ScopeList(scopeList)
  }

  override def visitOr(or: Or): VisitorReturnType={
    val scopesLeft = or.opr1.accept(this)
    val scopesRight = or.opr2.accept(this)
    val scopeList = scopeOfBinFun(scopesLeft.asInstanceOf[Scope], scopesRight.asInstanceOf[Scope], "or", BoolSortExp())
    ScopeList(scopeList)
  }

  override def visitNot(not: Not): VisitorReturnType={
    val scope = not.opr.accept(this)
    val scopeList = scopeOfUnFun(scope.asInstanceOf[Scope], "not", BoolSortExp())
    ScopeList(scopeList)
  }

  override def visitImplies(implies: Implies): VisitorReturnType={
    val scopesLeft = implies.opr1.accept(this)
    val scopesRight = implies.opr2.accept(this)
    val scopeList = scopeOfBinFun(scopesLeft.asInstanceOf[Scope], scopesRight.asInstanceOf[Scope], "=>", BoolSortExp())
    ScopeList(scopeList)
  }

  override def visitPlus(plus: Plus): VisitorReturnType={
    val scopesLeft = plus.opr1.accept(this)
    val scopesRight = plus.opr2.accept(this)
    val scopeList = scopeOfBinFun(scopesLeft.asInstanceOf[Scope], scopesRight.asInstanceOf[Scope], "+", IntSortExp())
    ScopeList(scopeList)
  }

  override def visitMinus(minus: Minus): VisitorReturnType={
    val scopesLeft = minus.opr1.accept(this)
    val scopesRight = minus.opr2.accept(this)
    val scopeList = scopeOfBinFun(scopesLeft.asInstanceOf[Scope], scopesRight.asInstanceOf[Scope], "-", IntSortExp())
    ScopeList(scopeList)
  }

  override def visitDiv(div: Div): VisitorReturnType={
    val scopesLeft = div.opr1.accept(this)
    val scopesRight = div.opr2.accept(this)
    val scopeList = scopeOfBinFun(scopesLeft.asInstanceOf[Scope], scopesRight.asInstanceOf[Scope], "/", IntSortExp())
    ScopeList(scopeList)
  }

  override def visitMult(mult: Mult): VisitorReturnType={
    val scopesLeft = mult.opr1.accept(this)
    val scopesRight = mult.opr2.accept(this)
    val scopeList = scopeOfBinFun(scopesLeft.asInstanceOf[Scope], scopesRight.asInstanceOf[Scope], "*", IntSortExp())
    ScopeList(scopeList)
  }

  private def scopeLookupVarOrConst(toLook: String, sort: SortExp): Scope ={
    val scopes = unrolledGrammar.type2Scopes.get(sort).getOrElse(null)
    val scopesOfThisVar = scopes.filter(eachScope => {
      val rules = unrolledGrammar.scopeRules.get(eachScope).getOrElse(null)
      rules.find(p => (p.isInstanceOf[SymbolGTerm] && p.asInstanceOf[SymbolGTerm].symbol.equals(toLook)) ||
        (p.isInstanceOf[LiteralGTerm] && p.asInstanceOf[LiteralGTerm].litGTerm.litStr.equals(toLook))) match {
        case None => false
        case Some(_) => true
      }
    })
    val scopesList = scopesOfThisVar.foldLeft(List[ScopeName]())((res,sc) => res :+ ScopeName(sc))
    ScopeList(scopesList)
  }

  private def scopeLookupFun(toLook: String, sort: SortExp): mutable.HashMap[String, List[ScopeName]] ={
    val scopes = unrolledGrammar.type2Scopes.get(sort).getOrElse(null)
    scopes.foldLeft(new mutable.HashMap[String, List[ScopeName]]())((resMap, eachScope) => {
      val rules = unrolledGrammar.scopeRules.get(eachScope).getOrElse(null)
      rules.find(p => p.isInstanceOf[FunGTerm] && p.asInstanceOf[FunGTerm].funName.equals(toLook)) match {
        case None => resMap
        case Some(func) => {
          val argScopes = func.asInstanceOf[FunGTerm].args.foldLeft(List[ScopeName]()){
            (res, arg) =>{
              val scopeOfArg = arg match {
                case SymbolGTerm(variableOrScopeName) => {
                  if(unrolledGrammar.scopeRules.contains(variableOrScopeName)){//ScopeName
                    ScopeName(variableOrScopeName)
                  }else{//Variable, then it should be in the same scope of this function
                    ScopeName(eachScope)
                  }
                }
                case LiteralGTerm(lit) => {
                  ScopeName(eachScope)
                }
              }
              res :+ scopeOfArg
          }}
          resMap += eachScope -> argScopes
          resMap
        }
      }
    })
  }

  override def visitBoolVar(boolVar: BoolVar): VisitorReturnType = {
    scopeLookupVarOrConst(boolVar.name, BoolSortExp())
  }

  override def visitBoolConst(boolConst: BoolConst): VisitorReturnType = {
    scopeLookupVarOrConst(boolConst.value, BoolSortExp())
  }

  override def visitIntVar(intVar: IntVar): VisitorReturnType = {
    scopeLookupVarOrConst(intVar.name, IntSortExp())
  }

  override def visitIntConst(intConst: IntConst): VisitorReturnType = {
    scopeLookupVarOrConst(intConst.value, IntSortExp())
  }
}