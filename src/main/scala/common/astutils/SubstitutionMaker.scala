package common.astutils

import scala.collection.mutable
import common.coreast.{IntVar, _}

/**
  * Created by dxble on 4/4/17.
  */
class SubstitutionMaker(from: mutable.ArrayBuffer[CAST], to: mutable.ArrayBuffer[CAST], reduceImplies: Boolean) extends CASTVisitorBase{

  private def substitution(exp: CAST): Option[CAST] = {
    val found = from.indexOf(exp)
    if(found == -1){
      None
    } else {
      Some(to(found))
    }
  }

  override def visitFunctionSignature(functionDesign: FunctionSignature): CAST ={
    substitution(functionDesign) match {
      case None => functionDesign
      case Some(v) => v
    }
  }

  override def visitGreater(greater: Greater): CAST = {
    substitution(greater) match {
      case None => {
        val left = greater.opr1.accept(this).asInstanceOf[ArithOperatorsCAST]
        val right = greater.opr2.accept(this).asInstanceOf[ArithOperatorsCAST]
        new Greater(left, right, greater.position)
      }
      case Some(v) => v
    }
  }

  override def visitLessThan(lessThan: LessThan): CAST ={
    substitution(lessThan) match {
      case None => {
        val left = lessThan.opr1.accept(this).asInstanceOf[ArithOperatorsCAST]
        val right = lessThan.opr2.accept(this).asInstanceOf[ArithOperatorsCAST]
        new LessThan(left, right, lessThan.position)
      }
      case Some(v) => v
    }
  }

  override def visitLTE(lTE: LTE): CAST ={
    substitution(lTE) match {
      case None => {
        val left = lTE.opr1.accept(this).asInstanceOf[ArithOperatorsCAST]
        val right = lTE.opr2.accept(this).asInstanceOf[ArithOperatorsCAST]
        new LTE(left, right, lTE.position)
      }
      case Some(v) => v
    }
  }

  override def visitGTE(gTE: GTE): CAST ={
    substitution(gTE) match {
      case None => {
        val left = gTE.opr1.accept(this).asInstanceOf[ArithOperatorsCAST]
        val right = gTE.opr2.accept(this).asInstanceOf[ArithOperatorsCAST]
        new GTE(left, right, gTE.position)
      }
      case Some(v) => v
    }
  }

  override def visitEqual(equal: Equal): CAST = {
    substitution(equal) match {
      case None => {
        val left = equal.opr1.accept(this).asInstanceOf[CAST]
        val right = equal.opr2.accept(this).asInstanceOf[CAST]
        new Equal(left, right, equal.position)
      }
      case Some(v) => v
    }
  }

  override def visitAnd(and: And): CAST = {
    substitution(and) match {
      case None => {
        val left = and.opr1.accept(this).asInstanceOf[BoolOperatorsCAST]
        val right = and.opr2.accept(this).asInstanceOf[BoolOperatorsCAST]
        new And(left, right, and.position)
      }
      case Some(v) => v
    }
  }

  override def visitOr(or: Or): CAST = {
    substitution(or) match {
      case None => {
        val left = or.opr1.accept(this).asInstanceOf[BoolOperatorsCAST]
        val right = or.opr2.accept(this).asInstanceOf[BoolOperatorsCAST]
        new Or(left, right, or.position)
      }
      case Some(v) => v
    }
  }

  override def visitNot(not: Not): CAST = {
    substitution(not) match {
      case None => {
        val op = not.opr.accept(this).asInstanceOf[BoolOperatorsCAST]
        new Not(op, not.position)
      }
      case Some(v) => v
    }
  }

  override def visitImplies(implies: Implies): CAST = {
    substitution(implies) match {
      case None => {
        val left = implies.opr1.accept(this).asInstanceOf[BoolOperatorsCAST]
        val right = implies.opr2.accept(this).asInstanceOf[BoolOperatorsCAST]
        if(reduceImplies){
          val evalLeft = new EvalAntecedent
          val evaluated = left.accept(evalLeft)
          if(evaluated.isInstanceOf[BoolConst]) {
            val res = evaluated.asInstanceOf[BoolConst]
            if (res.value.compareTo("true") == 0)
              return right
            else return BoolConst("true")
          } else return new Implies(left, right, implies.position)
        }
        else return new Implies(left, right, implies.position)
      }
      case Some(v) => return v
    }
  }

  override def visitBoolVar(boolVar: BoolVar): CAST = {
    substitution(boolVar) match {
      case None => {
        boolVar
      }
      case Some(v) => v
    }
  }

  override def visitBoolConst(boolConst: BoolConst): CAST = {
    substitution(boolConst) match {
      case None => {
        boolConst
      }
      case Some(v) => v
    }
  }

  override def visitPlus(plus: Plus): CAST = {
    substitution(plus) match {
      case None => {
        val left = plus.opr1.accept(this).asInstanceOf[ArithOperatorsCAST]
        val right = plus.opr2.accept(this).asInstanceOf[ArithOperatorsCAST]
        new Plus(left, right, plus.position)
      }
      case Some(v) => v
    }
  }

  override def visitMinus(minus: Minus): CAST = {
    substitution(minus) match {
      case None => {
        val left = minus.opr1.accept(this).asInstanceOf[ArithOperatorsCAST]
        val right = minus.opr2.accept(this).asInstanceOf[ArithOperatorsCAST]
        new Minus(left, right, minus.position)
      }
      case Some(v) => v
    }
  }

  override def visitDiv(div: Div): CAST = {
    substitution(div) match {
      case None => {
        val left = div.opr1.accept(this).asInstanceOf[ArithOperatorsCAST]
        val right = div.opr2.accept(this).asInstanceOf[ArithOperatorsCAST]
        new Div(left, right, div.position)
      }
      case Some(v) => v
    }
  }

  override def visitMult(mult: Mult): CAST = {
    substitution(mult) match {
      case None => {
        val left = mult.opr1.accept(this).asInstanceOf[ArithOperatorsCAST]
        val right = mult.opr2.accept(this).asInstanceOf[ArithOperatorsCAST]
        new Mult(left, right, mult.position)
      }
      case Some(v) => v
    }
  }

  override def visitIntConst(intConst: IntConst): CAST = {
    substitution(intConst) match {
      case None => {
        intConst
      }
      case Some(v) => v
    }
  }

  override def visitIntVar(intVar: IntVar): CAST = {
    substitution(intVar) match {
      case None => {
        intVar
      }
      case Some(v) => v
    }
  }
}
