package common.astutils

import common.coreast._

/**
  * Created by dxble on 4/6/17.
  */

class EvalAntecedent() extends CASTVisitorBase{
  def visitFunctionSignature(functionDesign: FunctionSignature) = functionDesign
  def visitIntVar(intVar: IntVar) = intVar
  def visitBoolVar(boolVar: BoolVar) = boolVar

  override def visitAnd(and: And): CAST = {
    val evalLeft = new EvalAntecedent
    val left = and.getLeft().accept(evalLeft)
    if(!left.isInstanceOf[BoolConst])
      return and

    if(left.asInstanceOf[BoolConst].value.compareTo("false") == 0)
      BoolConst("false")
    else {
      val evalRight = new EvalAntecedent
      val right = and.getRight().accept(evalRight)
      if(!right.isInstanceOf[BoolConst])
        return right.asInstanceOf[CAST]

      if(right.asInstanceOf[BoolConst].value.compareTo("false") == 0)
        return BoolConst("false")
      else return BoolConst("true")
    }
  }

  override def visitEqual(equal: Equal): CAST = {
    if(!equal.getLeft().isInstanceOf[ConstTerm] || !equal.getRight().isInstanceOf[ConstTerm])
      equal
    else {
      equal.getLeft() match {
        case left: IntConst => {
          equal.getRight() match {
            case right: IntConst => {
              if (left.value.compareTo(right.value) == 0)
                return BoolConst("true")
              else return BoolConst("false")
            }
          }
        }
        case left: BoolConst => {
          equal.getRight() match {
            case right: BoolConst => {
              if (left.value.compareTo(right.value) == 0)
                return BoolConst("true")
              else return BoolConst("false")
            }
          }
        }
      }
    }
  }
}
