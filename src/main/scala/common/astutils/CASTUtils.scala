package common.astutils

import com.microsoft.z3.{BoolExpr, Context, Expr}
import common.coreast._
import componentsynthesis.core.Port
import componentsynthesis.satsolver.SolverUtils
import main.scala.synthlib2parser.scopemanager.{BoolSortExp, IntSortExp, SortExp}
import z3.scala.{Z3AST, Z3Context}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 3/26/17.
  */
object CASTUtils {
  def getValFromType(value: String, sort: SortExp): CAST ={
    sort match {
      case _: IntSortExp => IntConst(value)
      case _: BoolSortExp => BoolConst(value)
    }
  }

  def makeVariable(name: String, sort: SortExp) = {
    sort match {
      case _: IntSortExp => IntVar(name)
      case _: BoolSortExp => BoolVar(name)
    }
  }

  def theory2CAST(theoryFunc: String, argConstraints: ArrayBuffer[CAST]): CAST = {
    try {
      theoryFunc match {
        case "and" => And(argConstraints(0).asInstanceOf[BoolOperatorsCAST], argConstraints(1).asInstanceOf[BoolOperatorsCAST])
        case "or" => Or(argConstraints(0).asInstanceOf[BoolOperatorsCAST], argConstraints(1).asInstanceOf[BoolOperatorsCAST])
        case "not" => Not(argConstraints(0).asInstanceOf[BoolOperatorsCAST])
        case "=>" => Implies(argConstraints(0).asInstanceOf[BoolOperatorsCAST], argConstraints(1).asInstanceOf[BoolOperatorsCAST])
        case "=" => Equal(argConstraints(0), argConstraints(1))
        case ">" => Greater(argConstraints(0).asInstanceOf[ArithOperatorsCAST], argConstraints(1).asInstanceOf[ArithOperatorsCAST])
        case ">=" => GTE(argConstraints(0).asInstanceOf[ArithOperatorsCAST], argConstraints(1).asInstanceOf[ArithOperatorsCAST])
        case "<" => LessThan(argConstraints(0).asInstanceOf[ArithOperatorsCAST], argConstraints(1).asInstanceOf[ArithOperatorsCAST])
        case "<=" => LTE(argConstraints(0).asInstanceOf[ArithOperatorsCAST], argConstraints(1).asInstanceOf[ArithOperatorsCAST])
        case "+" => Plus(argConstraints(0).asInstanceOf[ArithOperatorsCAST], argConstraints(1).asInstanceOf[ArithOperatorsCAST])
        case "-" => Minus(argConstraints(0).asInstanceOf[ArithOperatorsCAST], argConstraints(1).asInstanceOf[ArithOperatorsCAST])
        case "*" => Mult(argConstraints(0).asInstanceOf[ArithOperatorsCAST], argConstraints(1).asInstanceOf[ArithOperatorsCAST])
        case "/" => Div(argConstraints(0).asInstanceOf[ArithOperatorsCAST], argConstraints(1).asInstanceOf[ArithOperatorsCAST])
      }
    }catch {
      case e: Exception => {
        println("debug: theory "+theoryFunc)
        null
      }
    }
  }

  def joinConstraint(constraints: mutable.Buffer[CAST]): BoolOperatorsCAST = {
    var joined: BoolOperatorsCAST = null
    constraints.foreach(c => {
      if(joined == null) joined = c.asInstanceOf[BoolOperatorsCAST]
      else joined = And(c.asInstanceOf[BoolOperatorsCAST], joined)
    })
    if(joined == null) BoolConst("true") else joined
    //joined
  }

  // This code is a bit dirty, but works
  def decVar2OrigName(v: VarTerm, lineNumber: String): VarTerm = {
    val normedLineNumber = if(lineNumber != null && lineNumber != "") "_"+lineNumber else ""
    def normalize(name: String): String = {
      val temp = name.replace("__DECVAR_","").replace(normedLineNumber, "")
      val sp = temp.split("_")
      val origName = sp.take(sp.length-1).mkString("_")
      origName
    }

    v match {
      case i: IntVar => {
        val name = normalize(i.name)
        IntVar(name)
      }
      case b: BoolVar => {
        val name = normalize(b.name)
        BoolVar(name)
      }
    }
  }

  def transformName(e: VarTerm, specInputPorts: ArrayBuffer[Port]): VarTerm = {
    e match {
      case i: IntVar => {
        val name = String.format("__DECVAR_%s", i.name)
        val port = specInputPorts.find(p => p.nameWithoutID.compareTo(name) == 0).getOrElse(throw new RuntimeException("Unexpected!"))
        IntVar(port.name)
      }
      case b: BoolVar => {
        val name = String.format("__DECVAR_%s", b.name)
        val port = specInputPorts.find(p => p.nameWithoutID.compareTo(name) == 0).getOrElse(throw new RuntimeException("Unexpected!"))
        BoolVar(port.name)
      }
    }
  }

  def funcComponentCount(exp: CAST, countCache: mutable.HashMap[String, Int], unrolledGrammar: UnrolledGrammar): Unit = {
    exp match {
      case bin: BinOp => {
        val scopeList = unrolledGrammar.findScopesOfExp(bin)
        scopeList.scopesList.foreach(scopeName => {
          val key = scopeName.scope+"_"+bin.topLevelString()
          val count = countCache.getOrElse(key, 0)
          countCache += key -> (count+1)
        })
        funcComponentCount(bin.getLeft(), countCache, unrolledGrammar)
        funcComponentCount(bin.getRight(), countCache, unrolledGrammar)
      }
      case unary: UnOp => {
        val scopeList = unrolledGrammar.findScopesOfExp(unary)
        scopeList.scopesList.foreach(scopeName => {
          val key = scopeName.scope+"_"+unary.topLevelString()
          val count = countCache.getOrElse(key, 0)
          countCache += key -> (count+1)
        })
        funcComponentCount(unary.getOperand(), countCache, unrolledGrammar)
      }
      case dummy: Dummy => {
        funcComponentCount(dummy.getOp(), countCache, unrolledGrammar)
      }
      case _ => {}
    }
  }

  def convert2Z3AST(e: CAST, z3: Z3Context): Z3AST = {
    val converter = new ToZ3ASTConverter(z3)
    e.accept(converter).asInstanceOf[Z3ASTReturnType].ast
  }

  def convert2Z3ASTJava(e: CAST, z3: Context): BoolExpr = {
    val converter = new CAST2Z3Java(z3)
    e.accept(converter).asInstanceOf[Z3ASTJavaRetType].ast.asInstanceOf[BoolExpr]
  }

}
