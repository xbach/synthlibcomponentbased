// Generated from /home/dxble/workspace/synthlibparser/src/main/resources/SynthLib.g4 by ANTLR 4.6
package generatedparser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SynthLibParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SynthLibVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(SynthLibParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#setLogicCmd}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetLogicCmd(SynthLibParser.SetLogicCmdContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#cmd}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCmd(SynthLibParser.CmdContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#varDeclCmd}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDeclCmd(SynthLibParser.VarDeclCmdContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#synthFunCmd}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSynthFunCmd(SynthLibParser.SynthFunCmdContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#origCmd}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrigCmd(SynthLibParser.OrigCmdContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#nTDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNTDef(SynthLibParser.NTDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#pairs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPairs(SynthLibParser.PairsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#constraintCmd}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstraintCmd(SynthLibParser.ConstraintCmdContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#checkSynthCmd}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCheckSynthCmd(SynthLibParser.CheckSynthCmdContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#gTerm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGTerm(SynthLibParser.GTermContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#fGTerm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFGTerm(SynthLibParser.FGTermContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#literalGTerm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteralGTerm(SynthLibParser.LiteralGTermContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#symbolGTerm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSymbolGTerm(SynthLibParser.SymbolGTermContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTerm(SynthLibParser.TermContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#fterm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFterm(SynthLibParser.FtermContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#literalTerm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteralTerm(SynthLibParser.LiteralTermContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#symbolTerm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSymbolTerm(SynthLibParser.SymbolTermContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#sortExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSortExpr(SynthLibParser.SortExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#intSortExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntSortExp(SynthLibParser.IntSortExpContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#boolSortExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolSortExp(SynthLibParser.BoolSortExpContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(SynthLibParser.LiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#intConstExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntConstExp(SynthLibParser.IntConstExpContext ctx);
	/**
	 * Visit a parse tree produced by {@link SynthLibParser#boolConstExp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolConstExp(SynthLibParser.BoolConstExpContext ctx);
}