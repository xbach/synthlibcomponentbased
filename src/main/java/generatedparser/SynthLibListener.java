// Generated from /home/dxble/workspace/synthlibparser/src/main/resources/SynthLib.g4 by ANTLR 4.6
package generatedparser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SynthLibParser}.
 */
public interface SynthLibListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(SynthLibParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(SynthLibParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#setLogicCmd}.
	 * @param ctx the parse tree
	 */
	void enterSetLogicCmd(SynthLibParser.SetLogicCmdContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#setLogicCmd}.
	 * @param ctx the parse tree
	 */
	void exitSetLogicCmd(SynthLibParser.SetLogicCmdContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#cmd}.
	 * @param ctx the parse tree
	 */
	void enterCmd(SynthLibParser.CmdContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#cmd}.
	 * @param ctx the parse tree
	 */
	void exitCmd(SynthLibParser.CmdContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#varDeclCmd}.
	 * @param ctx the parse tree
	 */
	void enterVarDeclCmd(SynthLibParser.VarDeclCmdContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#varDeclCmd}.
	 * @param ctx the parse tree
	 */
	void exitVarDeclCmd(SynthLibParser.VarDeclCmdContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#synthFunCmd}.
	 * @param ctx the parse tree
	 */
	void enterSynthFunCmd(SynthLibParser.SynthFunCmdContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#synthFunCmd}.
	 * @param ctx the parse tree
	 */
	void exitSynthFunCmd(SynthLibParser.SynthFunCmdContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#origCmd}.
	 * @param ctx the parse tree
	 */
	void enterOrigCmd(SynthLibParser.OrigCmdContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#origCmd}.
	 * @param ctx the parse tree
	 */
	void exitOrigCmd(SynthLibParser.OrigCmdContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#nTDef}.
	 * @param ctx the parse tree
	 */
	void enterNTDef(SynthLibParser.NTDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#nTDef}.
	 * @param ctx the parse tree
	 */
	void exitNTDef(SynthLibParser.NTDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#pairs}.
	 * @param ctx the parse tree
	 */
	void enterPairs(SynthLibParser.PairsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#pairs}.
	 * @param ctx the parse tree
	 */
	void exitPairs(SynthLibParser.PairsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#constraintCmd}.
	 * @param ctx the parse tree
	 */
	void enterConstraintCmd(SynthLibParser.ConstraintCmdContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#constraintCmd}.
	 * @param ctx the parse tree
	 */
	void exitConstraintCmd(SynthLibParser.ConstraintCmdContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#checkSynthCmd}.
	 * @param ctx the parse tree
	 */
	void enterCheckSynthCmd(SynthLibParser.CheckSynthCmdContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#checkSynthCmd}.
	 * @param ctx the parse tree
	 */
	void exitCheckSynthCmd(SynthLibParser.CheckSynthCmdContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#gTerm}.
	 * @param ctx the parse tree
	 */
	void enterGTerm(SynthLibParser.GTermContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#gTerm}.
	 * @param ctx the parse tree
	 */
	void exitGTerm(SynthLibParser.GTermContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#fGTerm}.
	 * @param ctx the parse tree
	 */
	void enterFGTerm(SynthLibParser.FGTermContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#fGTerm}.
	 * @param ctx the parse tree
	 */
	void exitFGTerm(SynthLibParser.FGTermContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#literalGTerm}.
	 * @param ctx the parse tree
	 */
	void enterLiteralGTerm(SynthLibParser.LiteralGTermContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#literalGTerm}.
	 * @param ctx the parse tree
	 */
	void exitLiteralGTerm(SynthLibParser.LiteralGTermContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#symbolGTerm}.
	 * @param ctx the parse tree
	 */
	void enterSymbolGTerm(SynthLibParser.SymbolGTermContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#symbolGTerm}.
	 * @param ctx the parse tree
	 */
	void exitSymbolGTerm(SynthLibParser.SymbolGTermContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#term}.
	 * @param ctx the parse tree
	 */
	void enterTerm(SynthLibParser.TermContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#term}.
	 * @param ctx the parse tree
	 */
	void exitTerm(SynthLibParser.TermContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#fterm}.
	 * @param ctx the parse tree
	 */
	void enterFterm(SynthLibParser.FtermContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#fterm}.
	 * @param ctx the parse tree
	 */
	void exitFterm(SynthLibParser.FtermContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#literalTerm}.
	 * @param ctx the parse tree
	 */
	void enterLiteralTerm(SynthLibParser.LiteralTermContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#literalTerm}.
	 * @param ctx the parse tree
	 */
	void exitLiteralTerm(SynthLibParser.LiteralTermContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#symbolTerm}.
	 * @param ctx the parse tree
	 */
	void enterSymbolTerm(SynthLibParser.SymbolTermContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#symbolTerm}.
	 * @param ctx the parse tree
	 */
	void exitSymbolTerm(SynthLibParser.SymbolTermContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#sortExpr}.
	 * @param ctx the parse tree
	 */
	void enterSortExpr(SynthLibParser.SortExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#sortExpr}.
	 * @param ctx the parse tree
	 */
	void exitSortExpr(SynthLibParser.SortExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#intSortExp}.
	 * @param ctx the parse tree
	 */
	void enterIntSortExp(SynthLibParser.IntSortExpContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#intSortExp}.
	 * @param ctx the parse tree
	 */
	void exitIntSortExp(SynthLibParser.IntSortExpContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#boolSortExp}.
	 * @param ctx the parse tree
	 */
	void enterBoolSortExp(SynthLibParser.BoolSortExpContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#boolSortExp}.
	 * @param ctx the parse tree
	 */
	void exitBoolSortExp(SynthLibParser.BoolSortExpContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(SynthLibParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(SynthLibParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#intConstExp}.
	 * @param ctx the parse tree
	 */
	void enterIntConstExp(SynthLibParser.IntConstExpContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#intConstExp}.
	 * @param ctx the parse tree
	 */
	void exitIntConstExp(SynthLibParser.IntConstExpContext ctx);
	/**
	 * Enter a parse tree produced by {@link SynthLibParser#boolConstExp}.
	 * @param ctx the parse tree
	 */
	void enterBoolConstExp(SynthLibParser.BoolConstExpContext ctx);
	/**
	 * Exit a parse tree produced by {@link SynthLibParser#boolConstExp}.
	 * @param ctx the parse tree
	 */
	void exitBoolConstExp(SynthLibParser.BoolConstExpContext ctx);
}