// Generated from /home/dxble/workspace/synthlibparser/src/main/resources/SynthLib.g4 by ANTLR 4.6
package generatedparser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SynthLibParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.6", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		IntSort=1, BoolSort=2, IntConst=3, BoolConst=4, CONSTRAINTKW=5, ORIGKW=6, 
		DECVARKW=7, DECFUNKW=8, CHECKSYNTHKW=9, SETLOGICKW=10, OPENBRK=11, CLOSEBRK=12, 
		SYNTHFUN=13, Symbol=14, SpecialChar=15, WS=16, LINE_COMMENT=17;
	public static final int
		RULE_program = 0, RULE_setLogicCmd = 1, RULE_cmd = 2, RULE_varDeclCmd = 3, 
		RULE_synthFunCmd = 4, RULE_origCmd = 5, RULE_nTDef = 6, RULE_pairs = 7, 
		RULE_constraintCmd = 8, RULE_checkSynthCmd = 9, RULE_gTerm = 10, RULE_fGTerm = 11, 
		RULE_literalGTerm = 12, RULE_symbolGTerm = 13, RULE_term = 14, RULE_fterm = 15, 
		RULE_literalTerm = 16, RULE_symbolTerm = 17, RULE_sortExpr = 18, RULE_intSortExp = 19, 
		RULE_boolSortExp = 20, RULE_literal = 21, RULE_intConstExp = 22, RULE_boolConstExp = 23;
	public static final String[] ruleNames = {
		"program", "setLogicCmd", "cmd", "varDeclCmd", "synthFunCmd", "origCmd", 
		"nTDef", "pairs", "constraintCmd", "checkSynthCmd", "gTerm", "fGTerm", 
		"literalGTerm", "symbolGTerm", "term", "fterm", "literalTerm", "symbolTerm", 
		"sortExpr", "intSortExp", "boolSortExp", "literal", "intConstExp", "boolConstExp"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'Int'", "'Bool'", null, null, "'constraint'", "'original-expr'", 
		"'declare-var'", "'declare-fun'", "'check-synth'", "'set-logic'", "'('", 
		"')'", "'synth-fun'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "IntSort", "BoolSort", "IntConst", "BoolConst", "CONSTRAINTKW", 
		"ORIGKW", "DECVARKW", "DECFUNKW", "CHECKSYNTHKW", "SETLOGICKW", "OPENBRK", 
		"CLOSEBRK", "SYNTHFUN", "Symbol", "SpecialChar", "WS", "LINE_COMMENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SynthLib.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SynthLibParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public SetLogicCmdContext setLogicCmd() {
			return getRuleContext(SetLogicCmdContext.class,0);
		}
		public TerminalNode EOF() { return getToken(SynthLibParser.EOF, 0); }
		public List<CmdContext> cmd() {
			return getRuleContexts(CmdContext.class);
		}
		public CmdContext cmd(int i) {
			return getRuleContext(CmdContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			setState(63);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(48);
				setLogicCmd();
				setState(50); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(49);
					cmd();
					}
					}
					setState(52); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==OPENBRK );
				setState(54);
				match(EOF);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(57); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(56);
					cmd();
					}
					}
					setState(59); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==OPENBRK );
				setState(61);
				match(EOF);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetLogicCmdContext extends ParserRuleContext {
		public TerminalNode OPENBRK() { return getToken(SynthLibParser.OPENBRK, 0); }
		public TerminalNode SETLOGICKW() { return getToken(SynthLibParser.SETLOGICKW, 0); }
		public TerminalNode Symbol() { return getToken(SynthLibParser.Symbol, 0); }
		public TerminalNode CLOSEBRK() { return getToken(SynthLibParser.CLOSEBRK, 0); }
		public SetLogicCmdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setLogicCmd; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterSetLogicCmd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitSetLogicCmd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitSetLogicCmd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetLogicCmdContext setLogicCmd() throws RecognitionException {
		SetLogicCmdContext _localctx = new SetLogicCmdContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_setLogicCmd);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(65);
			match(OPENBRK);
			setState(66);
			match(SETLOGICKW);
			setState(67);
			match(Symbol);
			setState(68);
			match(CLOSEBRK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CmdContext extends ParserRuleContext {
		public VarDeclCmdContext varDeclCmd() {
			return getRuleContext(VarDeclCmdContext.class,0);
		}
		public SynthFunCmdContext synthFunCmd() {
			return getRuleContext(SynthFunCmdContext.class,0);
		}
		public ConstraintCmdContext constraintCmd() {
			return getRuleContext(ConstraintCmdContext.class,0);
		}
		public CheckSynthCmdContext checkSynthCmd() {
			return getRuleContext(CheckSynthCmdContext.class,0);
		}
		public OrigCmdContext origCmd() {
			return getRuleContext(OrigCmdContext.class,0);
		}
		public CmdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cmd; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterCmd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitCmd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitCmd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CmdContext cmd() throws RecognitionException {
		CmdContext _localctx = new CmdContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_cmd);
		try {
			setState(75);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(70);
				varDeclCmd();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(71);
				synthFunCmd();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(72);
				constraintCmd();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(73);
				checkSynthCmd();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(74);
				origCmd();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarDeclCmdContext extends ParserRuleContext {
		public TerminalNode OPENBRK() { return getToken(SynthLibParser.OPENBRK, 0); }
		public TerminalNode DECVARKW() { return getToken(SynthLibParser.DECVARKW, 0); }
		public TerminalNode Symbol() { return getToken(SynthLibParser.Symbol, 0); }
		public SortExprContext sortExpr() {
			return getRuleContext(SortExprContext.class,0);
		}
		public TerminalNode CLOSEBRK() { return getToken(SynthLibParser.CLOSEBRK, 0); }
		public VarDeclCmdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varDeclCmd; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterVarDeclCmd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitVarDeclCmd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitVarDeclCmd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VarDeclCmdContext varDeclCmd() throws RecognitionException {
		VarDeclCmdContext _localctx = new VarDeclCmdContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_varDeclCmd);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(77);
			match(OPENBRK);
			setState(78);
			match(DECVARKW);
			setState(79);
			match(Symbol);
			setState(80);
			sortExpr();
			setState(81);
			match(CLOSEBRK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SynthFunCmdContext extends ParserRuleContext {
		public List<TerminalNode> OPENBRK() { return getTokens(SynthLibParser.OPENBRK); }
		public TerminalNode OPENBRK(int i) {
			return getToken(SynthLibParser.OPENBRK, i);
		}
		public TerminalNode SYNTHFUN() { return getToken(SynthLibParser.SYNTHFUN, 0); }
		public TerminalNode Symbol() { return getToken(SynthLibParser.Symbol, 0); }
		public PairsContext pairs() {
			return getRuleContext(PairsContext.class,0);
		}
		public List<TerminalNode> CLOSEBRK() { return getTokens(SynthLibParser.CLOSEBRK); }
		public TerminalNode CLOSEBRK(int i) {
			return getToken(SynthLibParser.CLOSEBRK, i);
		}
		public SortExprContext sortExpr() {
			return getRuleContext(SortExprContext.class,0);
		}
		public List<NTDefContext> nTDef() {
			return getRuleContexts(NTDefContext.class);
		}
		public NTDefContext nTDef(int i) {
			return getRuleContext(NTDefContext.class,i);
		}
		public SynthFunCmdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_synthFunCmd; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterSynthFunCmd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitSynthFunCmd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitSynthFunCmd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SynthFunCmdContext synthFunCmd() throws RecognitionException {
		SynthFunCmdContext _localctx = new SynthFunCmdContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_synthFunCmd);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(83);
			match(OPENBRK);
			setState(84);
			match(SYNTHFUN);
			setState(85);
			match(Symbol);
			setState(86);
			match(OPENBRK);
			setState(87);
			pairs();
			setState(88);
			match(CLOSEBRK);
			setState(89);
			sortExpr();
			setState(90);
			match(OPENBRK);
			setState(92); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(91);
				nTDef();
				}
				}
				setState(94); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==OPENBRK );
			setState(96);
			match(CLOSEBRK);
			setState(97);
			match(CLOSEBRK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrigCmdContext extends ParserRuleContext {
		public List<TerminalNode> OPENBRK() { return getTokens(SynthLibParser.OPENBRK); }
		public TerminalNode OPENBRK(int i) {
			return getToken(SynthLibParser.OPENBRK, i);
		}
		public TerminalNode ORIGKW() { return getToken(SynthLibParser.ORIGKW, 0); }
		public TerminalNode Symbol() { return getToken(SynthLibParser.Symbol, 0); }
		public PairsContext pairs() {
			return getRuleContext(PairsContext.class,0);
		}
		public List<TerminalNode> CLOSEBRK() { return getTokens(SynthLibParser.CLOSEBRK); }
		public TerminalNode CLOSEBRK(int i) {
			return getToken(SynthLibParser.CLOSEBRK, i);
		}
		public SortExprContext sortExpr() {
			return getRuleContext(SortExprContext.class,0);
		}
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public OrigCmdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_origCmd; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterOrigCmd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitOrigCmd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitOrigCmd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrigCmdContext origCmd() throws RecognitionException {
		OrigCmdContext _localctx = new OrigCmdContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_origCmd);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(99);
			match(OPENBRK);
			setState(100);
			match(ORIGKW);
			setState(101);
			match(Symbol);
			setState(102);
			match(OPENBRK);
			setState(103);
			pairs();
			setState(104);
			match(CLOSEBRK);
			setState(105);
			sortExpr();
			setState(106);
			term();
			setState(107);
			match(CLOSEBRK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NTDefContext extends ParserRuleContext {
		public List<TerminalNode> OPENBRK() { return getTokens(SynthLibParser.OPENBRK); }
		public TerminalNode OPENBRK(int i) {
			return getToken(SynthLibParser.OPENBRK, i);
		}
		public TerminalNode Symbol() { return getToken(SynthLibParser.Symbol, 0); }
		public SortExprContext sortExpr() {
			return getRuleContext(SortExprContext.class,0);
		}
		public List<TerminalNode> CLOSEBRK() { return getTokens(SynthLibParser.CLOSEBRK); }
		public TerminalNode CLOSEBRK(int i) {
			return getToken(SynthLibParser.CLOSEBRK, i);
		}
		public List<GTermContext> gTerm() {
			return getRuleContexts(GTermContext.class);
		}
		public GTermContext gTerm(int i) {
			return getRuleContext(GTermContext.class,i);
		}
		public NTDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nTDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterNTDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitNTDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitNTDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NTDefContext nTDef() throws RecognitionException {
		NTDefContext _localctx = new NTDefContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_nTDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(109);
			match(OPENBRK);
			setState(110);
			match(Symbol);
			setState(111);
			sortExpr();
			setState(112);
			match(OPENBRK);
			setState(114); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(113);
				gTerm();
				}
				}
				setState(116); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IntConst) | (1L << BoolConst) | (1L << OPENBRK) | (1L << Symbol))) != 0) );
			setState(118);
			match(CLOSEBRK);
			setState(119);
			match(CLOSEBRK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PairsContext extends ParserRuleContext {
		public List<TerminalNode> OPENBRK() { return getTokens(SynthLibParser.OPENBRK); }
		public TerminalNode OPENBRK(int i) {
			return getToken(SynthLibParser.OPENBRK, i);
		}
		public List<TerminalNode> Symbol() { return getTokens(SynthLibParser.Symbol); }
		public TerminalNode Symbol(int i) {
			return getToken(SynthLibParser.Symbol, i);
		}
		public List<SortExprContext> sortExpr() {
			return getRuleContexts(SortExprContext.class);
		}
		public SortExprContext sortExpr(int i) {
			return getRuleContext(SortExprContext.class,i);
		}
		public List<TerminalNode> CLOSEBRK() { return getTokens(SynthLibParser.CLOSEBRK); }
		public TerminalNode CLOSEBRK(int i) {
			return getToken(SynthLibParser.CLOSEBRK, i);
		}
		public PairsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pairs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterPairs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitPairs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitPairs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PairsContext pairs() throws RecognitionException {
		PairsContext _localctx = new PairsContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_pairs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(128);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==OPENBRK) {
				{
				{
				setState(121);
				match(OPENBRK);
				setState(122);
				match(Symbol);
				setState(123);
				sortExpr();
				setState(124);
				match(CLOSEBRK);
				}
				}
				setState(130);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstraintCmdContext extends ParserRuleContext {
		public TerminalNode OPENBRK() { return getToken(SynthLibParser.OPENBRK, 0); }
		public TerminalNode CONSTRAINTKW() { return getToken(SynthLibParser.CONSTRAINTKW, 0); }
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public TerminalNode CLOSEBRK() { return getToken(SynthLibParser.CLOSEBRK, 0); }
		public ConstraintCmdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constraintCmd; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterConstraintCmd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitConstraintCmd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitConstraintCmd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstraintCmdContext constraintCmd() throws RecognitionException {
		ConstraintCmdContext _localctx = new ConstraintCmdContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_constraintCmd);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(131);
			match(OPENBRK);
			setState(132);
			match(CONSTRAINTKW);
			setState(133);
			term();
			setState(134);
			match(CLOSEBRK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CheckSynthCmdContext extends ParserRuleContext {
		public TerminalNode OPENBRK() { return getToken(SynthLibParser.OPENBRK, 0); }
		public TerminalNode CHECKSYNTHKW() { return getToken(SynthLibParser.CHECKSYNTHKW, 0); }
		public TerminalNode CLOSEBRK() { return getToken(SynthLibParser.CLOSEBRK, 0); }
		public CheckSynthCmdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_checkSynthCmd; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterCheckSynthCmd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitCheckSynthCmd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitCheckSynthCmd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CheckSynthCmdContext checkSynthCmd() throws RecognitionException {
		CheckSynthCmdContext _localctx = new CheckSynthCmdContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_checkSynthCmd);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(136);
			match(OPENBRK);
			setState(137);
			match(CHECKSYNTHKW);
			setState(138);
			match(CLOSEBRK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GTermContext extends ParserRuleContext {
		public FGTermContext fGTerm() {
			return getRuleContext(FGTermContext.class,0);
		}
		public LiteralGTermContext literalGTerm() {
			return getRuleContext(LiteralGTermContext.class,0);
		}
		public SymbolGTermContext symbolGTerm() {
			return getRuleContext(SymbolGTermContext.class,0);
		}
		public GTermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_gTerm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterGTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitGTerm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitGTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GTermContext gTerm() throws RecognitionException {
		GTermContext _localctx = new GTermContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_gTerm);
		try {
			setState(143);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPENBRK:
				enterOuterAlt(_localctx, 1);
				{
				setState(140);
				fGTerm();
				}
				break;
			case IntConst:
			case BoolConst:
				enterOuterAlt(_localctx, 2);
				{
				setState(141);
				literalGTerm();
				}
				break;
			case Symbol:
				enterOuterAlt(_localctx, 3);
				{
				setState(142);
				symbolGTerm();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FGTermContext extends ParserRuleContext {
		public TerminalNode OPENBRK() { return getToken(SynthLibParser.OPENBRK, 0); }
		public TerminalNode Symbol() { return getToken(SynthLibParser.Symbol, 0); }
		public TerminalNode CLOSEBRK() { return getToken(SynthLibParser.CLOSEBRK, 0); }
		public List<GTermContext> gTerm() {
			return getRuleContexts(GTermContext.class);
		}
		public GTermContext gTerm(int i) {
			return getRuleContext(GTermContext.class,i);
		}
		public FGTermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fGTerm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterFGTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitFGTerm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitFGTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FGTermContext fGTerm() throws RecognitionException {
		FGTermContext _localctx = new FGTermContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_fGTerm);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(145);
			match(OPENBRK);
			setState(146);
			match(Symbol);
			setState(150);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IntConst) | (1L << BoolConst) | (1L << OPENBRK) | (1L << Symbol))) != 0)) {
				{
				{
				setState(147);
				gTerm();
				}
				}
				setState(152);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(153);
			match(CLOSEBRK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralGTermContext extends ParserRuleContext {
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public LiteralGTermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literalGTerm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterLiteralGTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitLiteralGTerm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitLiteralGTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralGTermContext literalGTerm() throws RecognitionException {
		LiteralGTermContext _localctx = new LiteralGTermContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_literalGTerm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(155);
			literal();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SymbolGTermContext extends ParserRuleContext {
		public TerminalNode Symbol() { return getToken(SynthLibParser.Symbol, 0); }
		public SymbolGTermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_symbolGTerm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterSymbolGTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitSymbolGTerm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitSymbolGTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SymbolGTermContext symbolGTerm() throws RecognitionException {
		SymbolGTermContext _localctx = new SymbolGTermContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_symbolGTerm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(157);
			match(Symbol);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public FtermContext fterm() {
			return getRuleContext(FtermContext.class,0);
		}
		public LiteralTermContext literalTerm() {
			return getRuleContext(LiteralTermContext.class,0);
		}
		public SymbolTermContext symbolTerm() {
			return getRuleContext(SymbolTermContext.class,0);
		}
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitTerm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_term);
		try {
			setState(162);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPENBRK:
				enterOuterAlt(_localctx, 1);
				{
				setState(159);
				fterm();
				}
				break;
			case IntConst:
			case BoolConst:
				enterOuterAlt(_localctx, 2);
				{
				setState(160);
				literalTerm();
				}
				break;
			case Symbol:
				enterOuterAlt(_localctx, 3);
				{
				setState(161);
				symbolTerm();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FtermContext extends ParserRuleContext {
		public TerminalNode OPENBRK() { return getToken(SynthLibParser.OPENBRK, 0); }
		public TerminalNode Symbol() { return getToken(SynthLibParser.Symbol, 0); }
		public TerminalNode CLOSEBRK() { return getToken(SynthLibParser.CLOSEBRK, 0); }
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public FtermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fterm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterFterm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitFterm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitFterm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FtermContext fterm() throws RecognitionException {
		FtermContext _localctx = new FtermContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_fterm);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(164);
			match(OPENBRK);
			setState(165);
			match(Symbol);
			setState(169);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IntConst) | (1L << BoolConst) | (1L << OPENBRK) | (1L << Symbol))) != 0)) {
				{
				{
				setState(166);
				term();
				}
				}
				setState(171);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(172);
			match(CLOSEBRK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralTermContext extends ParserRuleContext {
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public LiteralTermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literalTerm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterLiteralTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitLiteralTerm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitLiteralTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralTermContext literalTerm() throws RecognitionException {
		LiteralTermContext _localctx = new LiteralTermContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_literalTerm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(174);
			literal();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SymbolTermContext extends ParserRuleContext {
		public TerminalNode Symbol() { return getToken(SynthLibParser.Symbol, 0); }
		public SymbolTermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_symbolTerm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterSymbolTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitSymbolTerm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitSymbolTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SymbolTermContext symbolTerm() throws RecognitionException {
		SymbolTermContext _localctx = new SymbolTermContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_symbolTerm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(176);
			match(Symbol);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SortExprContext extends ParserRuleContext {
		public IntSortExpContext intSortExp() {
			return getRuleContext(IntSortExpContext.class,0);
		}
		public BoolSortExpContext boolSortExp() {
			return getRuleContext(BoolSortExpContext.class,0);
		}
		public SortExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sortExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterSortExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitSortExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitSortExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SortExprContext sortExpr() throws RecognitionException {
		SortExprContext _localctx = new SortExprContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_sortExpr);
		try {
			setState(180);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IntSort:
				enterOuterAlt(_localctx, 1);
				{
				setState(178);
				intSortExp();
				}
				break;
			case BoolSort:
				enterOuterAlt(_localctx, 2);
				{
				setState(179);
				boolSortExp();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntSortExpContext extends ParserRuleContext {
		public TerminalNode IntSort() { return getToken(SynthLibParser.IntSort, 0); }
		public IntSortExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intSortExp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterIntSortExp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitIntSortExp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitIntSortExp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntSortExpContext intSortExp() throws RecognitionException {
		IntSortExpContext _localctx = new IntSortExpContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_intSortExp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(182);
			match(IntSort);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolSortExpContext extends ParserRuleContext {
		public TerminalNode BoolSort() { return getToken(SynthLibParser.BoolSort, 0); }
		public BoolSortExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolSortExp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterBoolSortExp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitBoolSortExp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitBoolSortExp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolSortExpContext boolSortExp() throws RecognitionException {
		BoolSortExpContext _localctx = new BoolSortExpContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_boolSortExp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(184);
			match(BoolSort);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public IntConstExpContext intConstExp() {
			return getRuleContext(IntConstExpContext.class,0);
		}
		public BoolConstExpContext boolConstExp() {
			return getRuleContext(BoolConstExpContext.class,0);
		}
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_literal);
		try {
			setState(188);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IntConst:
				enterOuterAlt(_localctx, 1);
				{
				setState(186);
				intConstExp();
				}
				break;
			case BoolConst:
				enterOuterAlt(_localctx, 2);
				{
				setState(187);
				boolConstExp();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntConstExpContext extends ParserRuleContext {
		public TerminalNode IntConst() { return getToken(SynthLibParser.IntConst, 0); }
		public IntConstExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intConstExp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterIntConstExp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitIntConstExp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitIntConstExp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntConstExpContext intConstExp() throws RecognitionException {
		IntConstExpContext _localctx = new IntConstExpContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_intConstExp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(190);
			match(IntConst);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolConstExpContext extends ParserRuleContext {
		public TerminalNode BoolConst() { return getToken(SynthLibParser.BoolConst, 0); }
		public BoolConstExpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolConstExp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).enterBoolConstExp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SynthLibListener ) ((SynthLibListener)listener).exitBoolConstExp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SynthLibVisitor ) return ((SynthLibVisitor<? extends T>)visitor).visitBoolConstExp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolConstExpContext boolConstExp() throws RecognitionException {
		BoolConstExpContext _localctx = new BoolConstExpContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_boolConstExp);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			match(BoolConst);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\23\u00c5\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\3\2\3\2\6\2\65\n\2\r\2\16\2\66\3\2\3\2\3\2\6\2<\n\2\r\2\16\2=\3\2\3\2"+
		"\5\2B\n\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\5\4N\n\4\3\5\3\5\3\5"+
		"\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\6\6_\n\6\r\6\16\6`\3"+
		"\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b"+
		"\6\bu\n\b\r\b\16\bv\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\7\t\u0081\n\t\f\t"+
		"\16\t\u0084\13\t\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\f\3\f\3\f\5"+
		"\f\u0092\n\f\3\r\3\r\3\r\7\r\u0097\n\r\f\r\16\r\u009a\13\r\3\r\3\r\3\16"+
		"\3\16\3\17\3\17\3\20\3\20\3\20\5\20\u00a5\n\20\3\21\3\21\3\21\7\21\u00aa"+
		"\n\21\f\21\16\21\u00ad\13\21\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\5"+
		"\24\u00b7\n\24\3\25\3\25\3\26\3\26\3\27\3\27\5\27\u00bf\n\27\3\30\3\30"+
		"\3\31\3\31\3\31\2\2\32\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,"+
		".\60\2\2\u00be\2A\3\2\2\2\4C\3\2\2\2\6M\3\2\2\2\bO\3\2\2\2\nU\3\2\2\2"+
		"\fe\3\2\2\2\16o\3\2\2\2\20\u0082\3\2\2\2\22\u0085\3\2\2\2\24\u008a\3\2"+
		"\2\2\26\u0091\3\2\2\2\30\u0093\3\2\2\2\32\u009d\3\2\2\2\34\u009f\3\2\2"+
		"\2\36\u00a4\3\2\2\2 \u00a6\3\2\2\2\"\u00b0\3\2\2\2$\u00b2\3\2\2\2&\u00b6"+
		"\3\2\2\2(\u00b8\3\2\2\2*\u00ba\3\2\2\2,\u00be\3\2\2\2.\u00c0\3\2\2\2\60"+
		"\u00c2\3\2\2\2\62\64\5\4\3\2\63\65\5\6\4\2\64\63\3\2\2\2\65\66\3\2\2\2"+
		"\66\64\3\2\2\2\66\67\3\2\2\2\678\3\2\2\289\7\2\2\39B\3\2\2\2:<\5\6\4\2"+
		";:\3\2\2\2<=\3\2\2\2=;\3\2\2\2=>\3\2\2\2>?\3\2\2\2?@\7\2\2\3@B\3\2\2\2"+
		"A\62\3\2\2\2A;\3\2\2\2B\3\3\2\2\2CD\7\r\2\2DE\7\f\2\2EF\7\20\2\2FG\7\16"+
		"\2\2G\5\3\2\2\2HN\5\b\5\2IN\5\n\6\2JN\5\22\n\2KN\5\24\13\2LN\5\f\7\2M"+
		"H\3\2\2\2MI\3\2\2\2MJ\3\2\2\2MK\3\2\2\2ML\3\2\2\2N\7\3\2\2\2OP\7\r\2\2"+
		"PQ\7\t\2\2QR\7\20\2\2RS\5&\24\2ST\7\16\2\2T\t\3\2\2\2UV\7\r\2\2VW\7\17"+
		"\2\2WX\7\20\2\2XY\7\r\2\2YZ\5\20\t\2Z[\7\16\2\2[\\\5&\24\2\\^\7\r\2\2"+
		"]_\5\16\b\2^]\3\2\2\2_`\3\2\2\2`^\3\2\2\2`a\3\2\2\2ab\3\2\2\2bc\7\16\2"+
		"\2cd\7\16\2\2d\13\3\2\2\2ef\7\r\2\2fg\7\b\2\2gh\7\20\2\2hi\7\r\2\2ij\5"+
		"\20\t\2jk\7\16\2\2kl\5&\24\2lm\5\36\20\2mn\7\16\2\2n\r\3\2\2\2op\7\r\2"+
		"\2pq\7\20\2\2qr\5&\24\2rt\7\r\2\2su\5\26\f\2ts\3\2\2\2uv\3\2\2\2vt\3\2"+
		"\2\2vw\3\2\2\2wx\3\2\2\2xy\7\16\2\2yz\7\16\2\2z\17\3\2\2\2{|\7\r\2\2|"+
		"}\7\20\2\2}~\5&\24\2~\177\7\16\2\2\177\u0081\3\2\2\2\u0080{\3\2\2\2\u0081"+
		"\u0084\3\2\2\2\u0082\u0080\3\2\2\2\u0082\u0083\3\2\2\2\u0083\21\3\2\2"+
		"\2\u0084\u0082\3\2\2\2\u0085\u0086\7\r\2\2\u0086\u0087\7\7\2\2\u0087\u0088"+
		"\5\36\20\2\u0088\u0089\7\16\2\2\u0089\23\3\2\2\2\u008a\u008b\7\r\2\2\u008b"+
		"\u008c\7\13\2\2\u008c\u008d\7\16\2\2\u008d\25\3\2\2\2\u008e\u0092\5\30"+
		"\r\2\u008f\u0092\5\32\16\2\u0090\u0092\5\34\17\2\u0091\u008e\3\2\2\2\u0091"+
		"\u008f\3\2\2\2\u0091\u0090\3\2\2\2\u0092\27\3\2\2\2\u0093\u0094\7\r\2"+
		"\2\u0094\u0098\7\20\2\2\u0095\u0097\5\26\f\2\u0096\u0095\3\2\2\2\u0097"+
		"\u009a\3\2\2\2\u0098\u0096\3\2\2\2\u0098\u0099\3\2\2\2\u0099\u009b\3\2"+
		"\2\2\u009a\u0098\3\2\2\2\u009b\u009c\7\16\2\2\u009c\31\3\2\2\2\u009d\u009e"+
		"\5,\27\2\u009e\33\3\2\2\2\u009f\u00a0\7\20\2\2\u00a0\35\3\2\2\2\u00a1"+
		"\u00a5\5 \21\2\u00a2\u00a5\5\"\22\2\u00a3\u00a5\5$\23\2\u00a4\u00a1\3"+
		"\2\2\2\u00a4\u00a2\3\2\2\2\u00a4\u00a3\3\2\2\2\u00a5\37\3\2\2\2\u00a6"+
		"\u00a7\7\r\2\2\u00a7\u00ab\7\20\2\2\u00a8\u00aa\5\36\20\2\u00a9\u00a8"+
		"\3\2\2\2\u00aa\u00ad\3\2\2\2\u00ab\u00a9\3\2\2\2\u00ab\u00ac\3\2\2\2\u00ac"+
		"\u00ae\3\2\2\2\u00ad\u00ab\3\2\2\2\u00ae\u00af\7\16\2\2\u00af!\3\2\2\2"+
		"\u00b0\u00b1\5,\27\2\u00b1#\3\2\2\2\u00b2\u00b3\7\20\2\2\u00b3%\3\2\2"+
		"\2\u00b4\u00b7\5(\25\2\u00b5\u00b7\5*\26\2\u00b6\u00b4\3\2\2\2\u00b6\u00b5"+
		"\3\2\2\2\u00b7\'\3\2\2\2\u00b8\u00b9\7\3\2\2\u00b9)\3\2\2\2\u00ba\u00bb"+
		"\7\4\2\2\u00bb+\3\2\2\2\u00bc\u00bf\5.\30\2\u00bd\u00bf\5\60\31\2\u00be"+
		"\u00bc\3\2\2\2\u00be\u00bd\3\2\2\2\u00bf-\3\2\2\2\u00c0\u00c1\7\5\2\2"+
		"\u00c1/\3\2\2\2\u00c2\u00c3\7\6\2\2\u00c3\61\3\2\2\2\17\66=AM`v\u0082"+
		"\u0091\u0098\u00a4\u00ab\u00b6\u00be";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}