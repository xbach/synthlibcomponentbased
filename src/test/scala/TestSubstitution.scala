import common.astutils.SubstitutionMaker
import common.coreast._

import scala.collection.mutable
/**
  * Created by dxble on 4/4/17.
  */
object TestSubstitution {
  def test1(): Boolean = {
    val exp = Implies(And(Equal(IntVar("x"), IntConst("1")), Equal(IntVar("y"), IntConst("5"))), Equal(IntVar("x"), IntConst("1")))
    println("Original Exp: "+exp)
    val from = new mutable.ArrayBuffer[CAST]()
    val to = new mutable.ArrayBuffer[CAST]()
    from.append(IntVar("x"))
    to.append(IntConst("6"))
    val subs = new SubstitutionMaker(from, to, true)
    val res = exp.accept(subs).asInstanceOf[CAST]
    println("Substituted Exp: "+res)
    true
  }

  def test11(): Boolean = {
    val exp = Implies(And(Equal(IntVar("x"), IntConst("1")), Equal(IntVar("y"), IntConst("5"))), Equal(IntVar("z"), IntConst("3")))
    println("Original Exp: "+exp)
    val from = new mutable.ArrayBuffer[CAST]()
    val to = new mutable.ArrayBuffer[CAST]()
    from.append(IntVar("x"))
    to.append(IntConst("1"))
    from.append(IntVar("y"))
    to.append(IntConst("5"))
    val subs = new SubstitutionMaker(from, to, true)
    val res = exp.accept(subs).asInstanceOf[CAST]
    println("Substituted Exp: "+res)
    true
  }

  def test2(): Boolean = {
    val exp = And(Equal(IntVar("x"), IntConst("1")), Equal(IntVar("y"), IntConst("5")))
    println("Original Exp: "+exp)
    val from = new mutable.ArrayBuffer[CAST]()
    val to = new mutable.ArrayBuffer[CAST]()
    from.append(Equal(IntVar("x"), IntConst("1")))
    to.append(Equal(IntVar("z"), IntConst("8")))
    val subs = new SubstitutionMaker(from, to, true)
    val res = exp.accept(subs).asInstanceOf[CAST]
    println("Substituted Exp: "+res)
    true
  }

  def test3(): Boolean = {
    val exp = And(Equal(IntVar("x"), IntConst("1")), Equal(IntVar("y"), IntConst("5")))
    println("Original Exp: "+exp)
    val from = new mutable.ArrayBuffer[CAST]()
    val to = new mutable.ArrayBuffer[CAST]()
    from.append(Equal(IntVar("x"), IntConst("7")))
    to.append(Equal(IntVar("z"), IntConst("8")))
    val subs = new SubstitutionMaker(from, to, true)
    val res = exp.accept(subs).asInstanceOf[CAST]
    println("Substituted Exp: "+res)
    true
  }

  def test4(): Boolean = {
    val exp = And(Equal(IntVar("x"), IntConst("1")), Equal(IntVar("y"), IntConst("5")))
    println("Original Exp: "+exp)
    val from = new mutable.ArrayBuffer[CAST]()
    val to = new mutable.ArrayBuffer[CAST]()
    from.append(Equal(IntVar("y"), IntConst("5")))
    to.append(Equal(IntVar("y"), IntConst("7")))
    val subs = new SubstitutionMaker(from, to, true)
    val res = exp.accept(subs).asInstanceOf[CAST]
    println("Substituted Exp: "+res)
    true
  }

  def test5(): Boolean = {
    val exp = And(Equal(IntVar("x"), IntConst("1")), Equal(IntVar("y"), IntConst("5")))
    println("Original Exp: "+exp)
    val from = new mutable.ArrayBuffer[CAST]()
    val to = new mutable.ArrayBuffer[CAST]()
    from.append(IntVar("x"))
    to.append(IntVar("a"))
    from.append(IntVar("y"))
    to.append(IntVar("b"))

    val subs = new SubstitutionMaker(from, to, true)
    val res = exp.accept(subs).asInstanceOf[CAST]
    println("Substituted Exp: "+res)
    true
  }

  def main(args: Array[String]) {
    test1()
    test11()
    //test2()
    //test3()
    //test4()
    //test5()
  }

}
