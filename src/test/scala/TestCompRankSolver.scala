import metadriver.MainDriver

/**
  * Created by dxble on 4/8/17.
  */
object TestCompRankSolver {
  val prefix = "/home/dxble/workspace/synthlibparser/src/test/resources/"
  def main(args: Array[String]): Unit = {
    //test1() //Angelix[no], ours[yes]
    //test2() //Angelix[yes], ours[yes]
    //test3() //Angelix[no], ours[yes]
    //test4() //Angelix[no], ours[yes]
    //test6() //Angelix[yes], ours[yes]
    test7() //Angelix[no], ours[yes] // ours not yet correct // correct without softStructural. All 1 -> 7 works without softStructural
    //test9() //Angelix[no], ours[yes, conditional 12] // ours not yet correct // correct without softStructural and softInputVariation
    //test12() //Angelix[no], ours[yes, conditional 9], ours only works with coverage // clash with test9 structural label
    //test13() // Angelix[no], ours[yes]
    //test14() // Angelix[yes], ours[yes]
    //test(15) // size bound 3, Angelix[yes], ours[yes]
    //test(16) // ours not yet work, give same buggy exp because no error witness
    //test(17) // Angelix[yes], ours[yes] // ours only constant 1
    //test(18) // Angelix[yes], ours[yes]
    //test(19) // ours not correct, give same buggy exp because no error witness
    test(20) // Angelix[no], ours not correct
  }

  def test(testNum: Int): Unit = {
    val configFile= prefix+String.format("t%d/configurationFile.txt", new Integer(testNum))
    MainDriver.main(Array[String](configFile))
  }

  def test1() = {
    val configFile= prefix+"t1/configurationFile.txt"
    MainDriver.main(Array[String](configFile))
  }

  def test2() = {
    val configFile= prefix+"t2/configurationFile.txt"
    MainDriver.main(Array[String](configFile))
  }

  def test3() = {
    val configFile= prefix+"t3/configurationFile.txt"
    MainDriver.main(Array[String](configFile))
  }

  def test4() = {
    val configFile= prefix+"t4/configurationFile.txt"
    MainDriver.main(Array[String](configFile))
  }

  def test6() = {
    val configFile= prefix+"t6/configurationFile.txt"
    MainDriver.main(Array[String](configFile))
  }

  def test7() = {
    val configFile= prefix+"t7/configurationFile.txt"
    MainDriver.main(Array[String](configFile))
  }

  def test9() = {
    val configFile= prefix+"t9/configurationFile.txt"
    MainDriver.main(Array[String](configFile))
  }

  def test12() = {
    val configFile= prefix+"t12/configurationFile.txt"
    MainDriver.main(Array[String](configFile))
  }

  def test13() = {
    val configFile= prefix+"t13/configurationFile.txt"
    MainDriver.main(Array[String](configFile))
  }

  def test14() = {
    val configFile= prefix+"t14/configurationFile.txt"
    MainDriver.main(Array[String](configFile))
  }
}
